<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Report Email</h2>
<p>{{ $data['message'] }}</p>
<a href="{{ $data['url'] }}" target="_blank">{{ $data['url'] }}</a>
<p>Reported by user {{ $data['by_user'] }}</p>
</body>
</html>