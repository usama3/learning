-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 17, 2018 at 07:58 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lp`
--

-- --------------------------------------------------------

--
-- Table structure for table `badges`
--

CREATE TABLE `badges` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `earned_picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `points` int(11) DEFAULT NULL,
  `type` enum('bronze','silver','gold') COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `badges`
--

INSERT INTO `badges` (`id`, `name`, `description`, `picture`, `earned_picture`, `points`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Supporter', '<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:12.0pt\">First upvote you give.</span></span></span></p>', 'images/Badges/4f03da7c95a64656c20766cf795717a6.png', 'images/Badges/bede3f5e8077b6243968d90e91de361d.jpg', 500, 'bronze', '2018-10-02 14:12:51', '2018-10-16 23:00:37'),
(2, 'Teacher', '<p><span style=\"font-size:12.0pt\">Answer a question with a bounty of 1 or more.</span></p>', 'images/Badges/5a8975c6233882832b366466c7435357.png', 'images/Badges/49ed318d9b12d5ea888a8bc45ae3060a.jpg', 500, 'bronze', '2018-10-02 14:27:16', '2018-10-16 22:59:53'),
(3, 'Promoter', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Calibri&quot;,sans-serif\">First bounty you offer on your own question</span></span></p>', 'images/Badges/f0114767b700626c1fe2c12e3b9c4b6c.png', 'images/Badges/dfbb95529cb7a39975dad9604dec9f04.jpg', 500, 'bronze', '2018-10-02 14:33:55', '2018-10-02 14:33:55'),
(4, 'Benefactor', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Calibri&quot;,sans-serif\">First bounty you manually award on your own question.</span></span></p>', 'images/Badges/0472ec27871f6797a68752f831e89042.png', 'images/Badges/ac46d4d3590dc44ac9daf41fd373389e.jpg', 500, 'bronze', '2018-10-03 01:44:33', '2018-10-03 01:44:33'),
(5, 'Critic', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Calibri&quot;,sans-serif\">First downvote</span></span></p>', 'images/Badges/3f87414a428a2c3258b3e963b89c20dd.png', 'images/Badges/3464fd8b1879f5e0cd6aec317b8d59d4.jpg', 500, 'bronze', '2018-10-02 14:36:07', '2018-10-02 14:36:07'),
(6, 'Mortarboard', '<p><span style=\"font-size:12.0pt\">Earn at least 200 points in a single day.</span></p>', 'images/Badges/9e4ecf857d97c9b36190a8853c74f775.png', 'images/Badges/5269bcee263e5662c4e9a8cae595164b.jpg', 500, 'bronze', '2018-10-02 14:37:17', '2018-10-16 23:00:06'),
(7, 'Rare', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Calibri&quot;,sans-serif\">Ask a question and accept an answer.</span></span></p>', 'images/Badges/633c0899de2e68512d8617e1d49d97f7.png', 'images/Badges/3f73a5595bdea605410ddd3615ab46d6.jpg', 500, 'bronze', '2018-10-02 14:38:53', '2018-10-02 14:38:53'),
(8, 'Self-Learner', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Calibri&quot;,sans-serif\">Answer you own question with 3 or more up vote.</span></span></p>', 'images/Badges/bb37fe45be0eeaf28917927c9c92e9de.png', 'images/Badges/86d376c11ecd209e3734311a1f030c16.jpg', 500, 'bronze', '2018-10-02 14:39:36', '2018-10-02 14:39:36'),
(9, 'Common', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Calibri&quot;,sans-serif\">The first question you ask with 1 or more upvotes.</span></span></p>', 'images/Badges/dea1e457c8a6d019054198bed1c6a40d.png', 'images/Badges/0fc9b79e9cfb23d58198ff01f6065f4c.jpg', 500, 'bronze', '2018-10-02 14:40:39', '2018-10-02 14:40:39'),
(10, 'Enthusiast', '<p><span style=\"font-size:12.0pt\">Visit the site 30 times in 30 days.</span></p>', 'images/Badges/cf1b20d1ffd633fa19ed6d576b244781.png', 'images/Badges/4dc2866249f2487b24be3b70690a556a.jpg', 1500, 'silver', '2018-10-02 14:56:03', '2018-10-03 17:21:49'),
(12, 'Refiner', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Calibri&quot;,sans-serif\">Answer 50 question.</span></span></p>', 'images/Badges/654c4197fe77f9f455a3bcd1680877d0.png', 'images/Badges/be90f60884aa5b32c8f453e8d69c8792.jpg', 1500, 'silver', '2018-10-02 14:57:49', '2018-10-02 14:57:49'),
(13, 'Sportsmanship', '<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:12.0pt\">Upvote answer on a question where an answer of you has a positive score.</span></span></span></p>', 'images/Badges/44cb24a888402ef452fafdbc22d9afd9.png', 'images/Badges/8f1bc365bf87b280040edb884deac2fc.jpg', 1500, 'silver', '2018-10-02 14:58:40', '2018-10-03 13:46:12'),
(14, 'Amazing', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Calibri&quot;,sans-serif\">earn at least 500 points in a single day.</span></span></p>', 'images/Badges/b564383543d0b3af16eb2b1f4db6f2bc.png', 'images/Badges/86a5730f3eea28662481222f84298dcb.jpg', 1500, 'silver', '2018-10-02 14:59:57', '2018-10-02 14:59:57'),
(15, 'Fanatic', '<p><span style=\"font-size:12.0pt\">Visit the site 100 times in 100 days.</span></p>', 'images/Badges/6b5334460eaf478d69c054be4694b569.png', 'images/Badges/2fcedf3d579c689d744b6f2afeace9ca.jpg', 5000, 'gold', '2018-10-02 15:00:41', '2018-10-03 17:22:27'),
(16, 'Electorate', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Calibri&quot;,sans-serif\">Vote on 600 questions or answers.</span></span></p>', 'images/Badges/c8736144586cd4fa2b1459007db206f5.png', 'images/Badges/52fdf638742e1527afb737647b76be0e.jpg', 5000, 'gold', '2018-10-02 15:01:21', '2018-10-02 15:01:21'),
(17, 'Legendary', '<p><span style=\"font-size:12.0pt\">Earn 200 daily points 150 times.</span></p>', 'images/Badges/15d071d5844256c4d22998c0c817e739.png', 'images/Badges/ce098244e6a330a4770e81f3cfa76b02.jpg', 5000, 'gold', '2018-10-02 15:02:09', '2018-10-16 23:00:21');

-- --------------------------------------------------------

--
-- Table structure for table `badge_user`
--

CREATE TABLE `badge_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `badge_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `badge_user`
--

INSERT INTO `badge_user` (`id`, `user_id`, `badge_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-10-17 17:44:23', '2018-10-17 17:44:23'),
(2, 1, 2, '2018-10-17 17:44:23', '2018-10-17 17:44:23'),
(3, 1, 3, '2018-10-17 17:44:23', '2018-10-17 17:44:23'),
(4, 1, 4, '2018-10-17 17:44:23', '2018-10-17 17:44:23'),
(5, 1, 5, '2018-10-17 17:44:23', '2018-10-17 17:44:23'),
(6, 1, 6, '2018-10-17 17:44:23', '2018-10-17 17:44:23'),
(7, 1, 7, '2018-10-17 17:44:23', '2018-10-17 17:44:23'),
(8, 1, 8, '2018-10-17 17:44:23', '2018-10-17 17:44:23'),
(9, 1, 9, '2018-10-17 17:44:23', '2018-10-17 17:44:23'),
(10, 1, 10, '2018-10-17 17:44:23', '2018-10-17 17:44:23'),
(11, 1, 12, '2018-10-17 17:44:23', '2018-10-17 17:44:23'),
(12, 1, 13, '2018-10-17 17:44:23', '2018-10-17 17:44:23'),
(13, 1, 14, '2018-10-17 17:44:23', '2018-10-17 17:44:23'),
(14, 1, 15, '2018-10-17 17:44:23', '2018-10-17 17:44:23'),
(15, 1, 16, '2018-10-17 17:44:23', '2018-10-17 17:44:23'),
(16, 1, 17, '2018-10-17 17:44:23', '2018-10-17 17:44:23'),
(17, 2, 2, '2018-10-17 17:47:52', '2018-10-17 17:47:52'),
(18, 2, 3, '2018-10-17 17:47:52', '2018-10-17 17:47:52');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES
(1, 'Damascus', 1),
(2, 'Cairo', 2),
(3, 'Ankara', 3),
(4, 'Aleppo', 1);

-- --------------------------------------------------------

--
-- Table structure for table `colleges`
--

CREATE TABLE `colleges` (
  `id` int(11) NOT NULL,
  `universities_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'college_picture.png',
  `cover_picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'college_cover.png',
  `cover_font_color` enum('#d8d8d8','#150d0b') COLLATE utf8_unicode_ci NOT NULL,
  `num_of_years` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `colleges`
--

INSERT INTO `colleges` (`id`, `universities_id`, `name`, `description`, `picture`, `cover_picture`, `cover_font_color`, `num_of_years`, `created_at`, `updated_at`) VALUES
(12, 9, 'Information Technology Engineering', '<p>Information technology (IT) is the use of computers to store, retrieve, transmit, and manipulate data, or information, often in the context of a business or other enterprise. IT is considered to be a subset of information and communications technology (ICT).</p>\r\n\r\n<p>Humans have been storing, retrieving, manipulating, and communicating information since the Sumerians in Mesopotamia developed writing in about 3000 BC, but the term information technology in its modern sense first appeared in a 1958 article published in the Harvard Business Review; authors Harold J. Leavitt and Thomas L. Whisler commented that &quot;the new technology does not yet have a single established name. We shall call it information technology (IT).&quot; Their definition consists of three categories: techniques for processing, the application of statistical and mathematical methods to decision-making, and the simulation of higher-order thinking through computer programs.</p>', 'images/Colleges/acdba16d302b073e962b11d99565e1e8.jpg', 'images/colleges/acb99e3670e3d01cef40114e2a39d0e2.jpg', '#150d0b', 5, '2018-10-17 01:29:47', '2018-10-17 01:37:01'),
(13, 9, 'Civil Engineering', '<p>Civil engineering is a professional engineering discipline that deals with the design, construction, and maintenance of the physical and naturally built environment, including works such as roads, bridges, canals, dams, airports, sewerage systems, pipelines, and railways. Civil engineering is traditionally broken into a number of sub-disciplines. It is considered the second-oldest engineering discipline after military engineering, and it is defined to distinguish non-military engineering from military engineering. Civil engineering takes place in the public sector from municipal through to national governments, and in the private sector from individual homeowners through to international companies.</p>', 'images/Colleges/f979bc94e2168a1ac75083c80015a69d.jpg', 'images/colleges/bd981a5a8f5a0829c3906351458bb8b7.jpg', '#150d0b', 5, '2018-10-17 01:33:06', '2018-10-17 01:35:41'),
(14, 9, 'Medical College', '<p>The Faculty of Medicine of Damascus University, founded in 1903, was the first university college established in Syria.</p>\r\n\r\n<p>The school moved to Beirut temporarily during World War I. At one point later it was called the Arab Medical Institute.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>', 'images/Colleges/8f414709c2dd5bc6b916b11491dd0981.jpg', 'images/colleges/2b156c14fbf220e39cfcbec89c95a0b1.jpg', '#150d0b', 5, '2018-10-17 13:15:29', '2018-10-17 13:17:18'),
(15, 9, 'Faculty of Dentistry', '<p>The University of Damascus is the largest and oldest university in Syria, located in the capital Damascus and has campuses in other Syrian cities.</p>', 'images/Colleges/04c3a8ea3282c83b060ee3da9c17cc42.jpg', 'images/colleges/be28315105d09b09225879ba0292787c.jpg', '#d8d8d8', 5, '2018-10-17 13:21:27', '2018-10-17 13:21:27'),
(16, 9, 'Faculty of Electronic and Telecommunication', '<p>The Institution of Electronics and Telecommunication Engineers (IETE) is India&#39;s leading recognized professional society devoted to the advancement of science, technology, electronics, telecommunication and information technology. Founded in 1953, it serves more than 69,000 members through 59 centers/ sub centers primarily located in India (3 abroad) . The Institution provides leadership in scientific and technical areas of direct importance to the national development and economy.Association of Indian Universities (AIU) has recognized AMIETE. Government of India has recognised IETE as a Scientific and Industrial Research Organization (SIRO) and also notified as an educational Institution of national eminence. The objectives of IETE focus on advancing electro-technology. The IETE conducts and sponsors technical meetings, conferences, symposia, and exhibitions all over India, publishes technical journals and provides continuing education as well as career advancement opportunities to its members.</p>', 'images/Colleges/007d48e4831596a8290598b067802ba6.jpg', 'images/colleges/6969fa718dcc9f952a450bd6738e2368.jpg', '#d8d8d8', 5, '2018-10-17 13:29:08', '2018-10-17 13:29:08'),
(17, 10, 'Information Technology Engineering', '<p>Information Technology Engineering</p>', 'images/Colleges/caf9bc3767f3b09f591c6acfea9f887d.jpg', 'images/colleges/9878a563c0d3b08ed5e0406b5598afb6.JPG', '#150d0b', 5, '2018-10-17 13:31:04', '2018-10-17 13:31:04');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`) VALUES
(1, 'Syria'),
(2, 'Egypt'),
(3, 'turkey');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `professor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'course_picture.png',
  `lectures_num` int(11) DEFAULT NULL,
  `year` enum('1','2','3','4','5','6') COLLATE utf8_unicode_ci DEFAULT NULL,
  `semester` enum('1','2','3') COLLATE utf8_unicode_ci DEFAULT NULL,
  `rating` float NOT NULL,
  `universities_id` int(11) NOT NULL,
  `colleges_id` int(11) DEFAULT NULL,
  `majors_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `name`, `professor`, `description`, `picture`, `lectures_num`, `year`, `semester`, `rating`, `universities_id`, `colleges_id`, `majors_id`, `created_at`, `updated_at`) VALUES
(1, 'Programming 1', 'Rakan', '<p>Computer programming is the process of designing and building an executable computer program for accomplishing a specific computing task. Programming involves tasks such as analysis, generating algorithms, profiling algorithms&#39; accuracy and resourc', 'images/Courses/e01f184005a46d3f00c76223a796e1c2.jpg', 10, '1', '1', 0, 9, 12, 11, '2018-10-17 13:38:13', '2018-10-17 13:38:13'),
(2, 'Machine Learning', 'george', '<p>Machine learning is an application of artificial intelligence (AI) that provides systems the ability to automatically learn and improve from experience without being explicitly programmed. Machine learning focuses on the development of computer program', 'images/Courses/8ad240d911031f5e4c521ee12d54df5a.jpg', 8, '5', '1', 0, 9, 12, 12, '2018-10-17 13:40:00', '2018-10-17 13:40:00'),
(3, 'Physics', 'someone', '<p>Physics, science that deals with the structure of matter and the interactions between the fundamental constituents of the observable universe. In the broadest sense, physics (from the Greek physikos) is concerned with all aspects of nature on both the ', 'images/Courses/c9f40f8435d23219fd80db020379fc25.jpg', 12, '1', '1', 0, 9, 12, 11, '2018-10-17 13:41:27', '2018-10-17 13:41:27'),
(4, 'Algorithms 1', 'Basem kosiba', '<p>In mathematics and computer science, an algorithm (/ˈ&aelig;lɡərɪ&eth;əm/ (About this sound listen)) is an unambiguous specification of how to solve a class of problems. Algorithms can perform calculation, data processing and automated reasoning tasks.', 'images/Courses/f23a14dff1e30106353ab694d87caa32.jpg', 11, '2', '1', 0, 9, 12, 11, '2018-10-17 13:43:24', '2018-10-17 13:43:24'),
(5, 'Software Engineering', 'someone', '<p>A software engineer is a person who applies the principles of software engineering to the design, development, maintenance, testing, and evaluation of computer software.</p>\r\n\r\n<p>Prior to the mid-1970s, software practitioners called themselves compute', 'images/Courses/ef51b3e22c62ea06978c9362313a7589.jpg', 12, '4', '2', 0, 9, 12, 13, '2018-10-17 13:44:25', '2018-10-17 13:44:25'),
(6, 'civil 1', 'someone', '<p>dasda</p>', 'images/Courses/77bf27cb3392520499b7c0eadc684bd6.jpg', 1, '1', '1', 0, 9, 13, 15, '2018-10-17 14:11:41', '2018-10-17 14:11:41');

-- --------------------------------------------------------

--
-- Table structure for table `courses_rating`
--

CREATE TABLE `courses_rating` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `rate` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lecture_text_answers`
--

CREATE TABLE `lecture_text_answers` (
  `id` int(11) NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `picture` text COLLATE utf8_unicode_ci NOT NULL,
  `up_vote` int(11) UNSIGNED NOT NULL,
  `down_vote` int(11) UNSIGNED NOT NULL,
  `marked` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `lecture_text_questions_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lecture_text_bookmarks`
--

CREATE TABLE `lecture_text_bookmarks` (
  `id` int(11) NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `up_vote` int(11) NOT NULL DEFAULT '0',
  `down_vote` int(11) NOT NULL DEFAULT '0',
  `private` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `text_section_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lecture_text_questions`
--

CREATE TABLE `lecture_text_questions` (
  `id` int(11) NOT NULL,
  `title` text CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `picture` text COLLATE utf8_unicode_ci NOT NULL,
  `up_vote` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `down_vote` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `bounty` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `text_lecture_id` int(11) NOT NULL,
  `text_sections_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lecture_text_summaries`
--

CREATE TABLE `lecture_text_summaries` (
  `id` int(11) NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `up_vote` int(11) NOT NULL DEFAULT '0',
  `down_vote` int(11) NOT NULL DEFAULT '0',
  `private` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `text_lecture_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lecture_video_answers`
--

CREATE TABLE `lecture_video_answers` (
  `id` int(11) NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `picture` text COLLATE utf8_unicode_ci NOT NULL,
  `up_vote` int(11) NOT NULL,
  `down_vote` int(11) NOT NULL,
  `marked` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `lecture_video_questions_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lecture_video_bookmarks`
--

CREATE TABLE `lecture_video_bookmarks` (
  `id` int(11) NOT NULL,
  `text` text CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `up_vote` int(11) NOT NULL DEFAULT '0',
  `down_vote` int(11) NOT NULL DEFAULT '0',
  `private` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `video_section_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lecture_video_questions`
--

CREATE TABLE `lecture_video_questions` (
  `id` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `picture` text COLLATE utf8_unicode_ci NOT NULL,
  `up_vote` int(11) NOT NULL,
  `down_vote` int(11) NOT NULL,
  `bounty` int(11) DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `video_lecture_id` int(11) NOT NULL,
  `video_sections_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lecture_video_summaries`
--

CREATE TABLE `lecture_video_summaries` (
  `id` int(11) NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `up_vote` int(11) NOT NULL DEFAULT '0',
  `down_vote` int(11) NOT NULL DEFAULT '0',
  `private` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `video_lecture_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `limitations`
--

CREATE TABLE `limitations` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `display_name` text COLLATE utf8_unicode_ci NOT NULL,
  `http_method` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `limitations`
--

INSERT INTO `limitations` (`id`, `name`, `display_name`, `http_method`, `created_at`, `updated_at`) VALUES
(1, 'read users questions', 'Read other users questions.', 'get', '2018-09-22 01:16:27', '2018-09-22 01:16:27'),
(2, 'vote questions', '', 'all', '2018-09-22 01:16:27', '2018-09-22 01:16:27'),
(3, 'add questions', '', 'post', '2018-09-22 11:38:35', '2018-09-22 11:38:35'),
(4, 'read users summaries', '', 'get', '2018-09-22 11:42:16', '2018-09-22 11:42:16'),
(5, 'vote summaries', '', 'all', '2018-09-22 11:42:16', '2018-09-22 11:42:16'),
(6, 'add summaries', '', 'post', '2018-09-22 11:42:16', '2018-09-22 11:42:16'),
(7, 'read users bookmarks', '', 'get', '2018-09-22 11:42:16', '2018-09-22 11:42:16'),
(8, 'vote bookmarks', '', 'all', '2018-09-22 11:42:16', '2018-09-22 11:42:16'),
(9, 'add bookmarks', '', 'post', '2018-09-22 11:42:16', '2018-09-22 11:42:16'),
(10, 'read users answers', '', 'get', '2018-09-22 11:42:16', '2018-09-22 11:42:16'),
(11, 'vote answers', '', 'all', '2018-09-22 11:42:16', '2018-09-22 11:42:16'),
(13, 'add bounty on question', '', 'post', '2018-09-22 11:44:35', '2018-09-22 11:44:35'),
(14, 'mark answer', '', 'post', '2018-09-22 11:44:35', '2018-09-22 11:44:35');

-- --------------------------------------------------------

--
-- Table structure for table `limitations_offers`
--

CREATE TABLE `limitations_offers` (
  `id` int(11) NOT NULL,
  `offers_id` int(11) NOT NULL,
  `limitations_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `limitations_offers`
--

INSERT INTO `limitations_offers` (`id`, `offers_id`, `limitations_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-10-17 17:38:25', '2018-10-17 17:38:25'),
(2, 1, 2, '2018-10-17 17:38:25', '2018-10-17 17:38:25'),
(3, 1, 3, '2018-10-17 17:38:25', '2018-10-17 17:38:25'),
(4, 1, 4, '2018-10-17 17:38:25', '2018-10-17 17:38:25'),
(5, 1, 5, '2018-10-17 17:38:25', '2018-10-17 17:38:25'),
(6, 1, 6, '2018-10-17 17:38:25', '2018-10-17 17:38:25'),
(7, 1, 7, '2018-10-17 17:38:25', '2018-10-17 17:38:25'),
(8, 1, 8, '2018-10-17 17:38:25', '2018-10-17 17:38:25'),
(9, 1, 9, '2018-10-17 17:38:25', '2018-10-17 17:38:25'),
(10, 1, 10, '2018-10-17 17:38:25', '2018-10-17 17:38:25'),
(11, 1, 11, '2018-10-17 17:38:25', '2018-10-17 17:38:25'),
(12, 1, 13, '2018-10-17 17:38:25', '2018-10-17 17:38:25'),
(13, 1, 14, '2018-10-17 17:38:25', '2018-10-17 17:38:25');

-- --------------------------------------------------------

--
-- Table structure for table `majors`
--

CREATE TABLE `majors` (
  `id` int(11) NOT NULL,
  `universities_id` int(11) NOT NULL,
  `colleges_id` int(11) NOT NULL,
  `years` text COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `majors`
--

INSERT INTO `majors` (`id`, `universities_id`, `colleges_id`, `years`, `name`, `description`, `picture`, `created_at`, `updated_at`) VALUES
(11, 9, 12, '1,2,3', 'General', '<p>General</p>', 'images/Majors/616be017b6e34f544802391a7c7e8ebc.png', '2018-10-17 01:39:36', '2018-10-17 01:39:36'),
(12, 9, 12, '4,5', 'Artificial Intelligence', '<p>Artificial Intelligence</p>', 'images/Majors/2815213429f6422050c782967986b68e.jpg', '2018-10-17 01:40:39', '2018-10-17 01:40:39'),
(13, 9, 12, '4,5', 'Software Engineering', '<p>Software Engineering</p>', 'images/Majors/88742d6b38bcc2c16a869b906de5ce8a.jpg', '2018-10-17 01:42:24', '2018-10-17 01:42:24'),
(14, 9, 12, '4,5', 'Networking', '<p>Networking</p>', 'images/Majors/f4c16b4e592f9aec9701bd14cf429514.jpg', '2018-10-17 01:43:31', '2018-10-17 01:43:31'),
(15, 9, 13, '1,2,3,4,5', 'General', '<p>General</p>', 'images/Majors/60158b89b568356d2a88eda52b07b887.png', '2018-10-17 01:44:05', '2018-10-17 01:44:05');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `uri` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `parent_id`, `order`, `title`, `icon`, `uri`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 'Index', 'fa-bar-chart', '/', NULL, NULL),
(2, 0, 2, 'Admin', 'fa-tasks', '', NULL, NULL),
(3, 2, 3, 'Users', 'fa-users', 'auth/users', NULL, '2018-08-20 22:57:53'),
(4, 2, 4, 'Roles', 'fa-user', 'auth/roles', NULL, NULL),
(5, 2, 5, 'Permission', 'fa-ban', 'auth/permissions', NULL, NULL),
(6, 2, 7, 'Menu', 'fa-bars', 'auth/menu', NULL, '2018-10-04 11:34:27'),
(7, 2, 8, 'Operation log', 'fa-history', 'auth/logs', NULL, '2018-10-04 11:34:27'),
(12, 2, 9, 'Routes', 'fa-list-alt', 'helpers/routes', '2018-08-19 12:05:29', '2018-10-04 11:34:27'),
(17, 0, 10, 'Managment', 'fa-sitemap', NULL, '2018-08-22 20:06:55', '2018-10-04 11:34:27'),
(18, 17, 11, 'Universities', 'fa-university', '/Universities', '2018-08-22 20:07:53', '2018-10-04 11:34:27'),
(19, 17, 12, 'Colleges', 'fa-building-o', '/Colleges', '2018-08-22 20:11:43', '2018-10-04 11:34:27'),
(20, 17, 13, 'Majors', 'fa-gg', '/Majors', '2018-08-22 20:13:29', '2018-10-04 11:34:27'),
(21, 0, 14, 'Courses', 'fa-book', NULL, '2018-08-23 00:27:46', '2018-10-04 11:34:28'),
(22, 21, 16, 'Text Lecture', 'fa-file-text', '/Text_lecture', '2018-08-23 00:30:26', '2018-10-04 11:34:28'),
(23, 22, 18, 'Text Sections', 'fa-align-justify', '/Text_lecture_sections', '2018-08-23 00:31:29', '2018-10-04 11:34:28'),
(24, 21, 19, 'Video Lecture', 'fa-video-camera', '/video_lecture', '2018-08-23 00:32:30', '2018-10-04 11:34:28'),
(25, 24, 21, 'Video Sections', 'fa-chrome', '/Video_lecture_sections', '2018-08-23 00:33:24', '2018-10-04 11:34:28'),
(26, 21, 15, 'Course', 'fa-book', 'Course', '2018-08-23 02:19:41', '2018-10-04 11:34:28'),
(27, 0, 29, 'Reports', 'fa-bug', NULL, '2018-08-24 02:31:10', '2018-10-04 12:29:37'),
(28, 36, 23, 'Badges', 'fa-ra', 'Badges', '2018-08-24 02:31:43', '2018-10-04 11:34:28'),
(29, 22, 17, 'Text Lecture', 'fa-file-text-o', 'Text_lecture', '2018-08-25 00:43:49', '2018-10-04 11:34:28'),
(30, 24, 20, 'Video_lecture', 'fa-camera-retro', 'Video_lecture', '2018-08-25 00:44:22', '2018-10-04 11:34:28'),
(31, 27, 30, 'Reports', 'fa-mail-reply-all', '/Reports', '2018-09-21 16:21:27', '2018-10-04 11:34:28'),
(32, 27, 31, 'Reports Messges', 'fa-commenting', 'Reports_msg', '2018-09-21 21:14:02', '2018-10-04 11:34:28'),
(33, 34, 26, 'Offers', 'fa-coffee', 'offers', '2018-09-21 22:23:46', '2018-10-04 11:34:28'),
(34, 0, 25, 'Offers', 'fa-diamond', NULL, '2018-09-22 00:37:03', '2018-10-04 11:34:28'),
(35, 34, 28, 'Users Offers', 'fa-anchor', 'users_offers', '2018-09-22 00:37:35', '2018-10-04 11:34:28'),
(36, 0, 22, 'Badges', 'fa-500px', NULL, '2018-09-22 01:03:28', '2018-10-04 11:34:28'),
(37, 36, 24, 'Users Badges', 'fa-area-chart', 'users_badges', '2018-09-22 02:03:46', '2018-10-04 11:34:28'),
(38, 34, 27, 'Offer Period', 'fa-calendar-times-o', '/Offers_period', '2018-09-28 10:26:18', '2018-10-04 11:34:28'),
(39, 2, 6, 'Static Images', 'fa-image', '/static_images', '2018-10-04 11:34:09', '2018-10-04 11:34:27');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_04_173148_create_admin_tables', 1),
(4, '2018_08_19_150833_create_test_table', 2),
(12, '2016_06_01_000001_create_oauth_auth_codes_table', 3),
(13, '2016_06_01_000002_create_oauth_access_tokens_table', 3),
(14, '2016_06_01_000003_create_oauth_refresh_tokens_table', 3),
(15, '2016_06_01_000004_create_oauth_clients_table', 3),
(16, '2016_06_01_000005_create_oauth_personal_access_clients_table', 3),
(17, '2018_09_13_232647_create_jobs_table', 3),
(18, '2018_09_13_232714_create_failed_jobs_table', 3),
(19, '2018_09_28_230606_add_to_users', 3),
(20, '2018_10_05_154307_create_notifications_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `text` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `picture` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `event_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notification_type`
--

CREATE TABLE `notification_type` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `title` text CHARACTER SET utf8 NOT NULL,
  `text` text CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification_type`
--

INSERT INTO `notification_type` (`id`, `name`, `title`, `text`, `created_at`, `updated_at`) VALUES
(1, 'Question', 'New question', 'New Question has been added to Course $course_name.', '2018-10-08 19:05:30', '2018-10-08 18:56:17'),
(2, 'Question Vote', 'Question Vote', ' $user_name has $vote_type your question.', '2018-10-10 17:41:47', '2018-10-08 18:59:02'),
(3, 'Question answered', 'Question answered', ' $user_name has added a new answer on question you follow.', '2018-10-10 17:41:49', '2018-10-08 18:59:02'),
(4, 'Answer vote', 'Answer vote', ' $user_name has $vote_type your answer.', '2018-10-10 17:41:50', '2018-10-08 19:00:21'),
(5, 'Answer marked', 'Answer marked', ' $user_name has marked your answer as best answer on his question.', '2018-10-10 17:41:53', '2018-10-08 19:00:21'),
(6, 'Summary vote', 'Summary vote', ' $user_name has $vote_type your summary.', '2018-10-10 17:41:56', '2018-10-08 19:02:42'),
(7, 'Bookmark vote', 'Bookmark vote', ' $user_name has $vote_type your bookmark.', '2018-10-10 17:42:00', '2018-10-08 19:02:42'),
(8, 'Badge', 'New badge earned', 'You have earned $badge_name badge.', '2018-10-10 17:42:03', '2018-10-08 19:03:20'),
(9, 'Question bounty', 'Question bounty', ' $user_name has added bounty on his question.', '2018-10-10 17:47:10', '2018-10-08 19:05:22');

-- --------------------------------------------------------

--
-- Table structure for table `notification_users`
--

CREATE TABLE `notification_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `notifications_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `is_seen` tinyint(1) NOT NULL,
  `is_seen_date` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification_users`
--

INSERT INTO `notification_users` (`id`, `notifications_id`, `user_id`, `is_seen`, `is_seen_date`, `created_at`, `updated_at`) VALUES
(105, 75, 1, 0, '0000-00-00 00:00:00', '2018-10-16 13:45:17', '2018-10-16 13:45:17'),
(106, 76, 1, 0, '0000-00-00 00:00:00', '2018-10-16 13:45:27', '2018-10-16 13:45:27'),
(107, 77, 1, 0, '0000-00-00 00:00:00', '2018-10-16 13:45:39', '2018-10-16 13:45:39'),
(108, 78, 1, 0, '0000-00-00 00:00:00', '2018-10-16 13:46:18', '2018-10-16 13:46:18'),
(109, 79, 1, 0, '0000-00-00 00:00:00', '2018-10-16 13:46:35', '2018-10-16 13:46:35'),
(110, 80, 1, 0, '0000-00-00 00:00:00', '2018-10-16 13:46:51', '2018-10-16 13:46:51'),
(111, 81, 1, 0, '0000-00-00 00:00:00', '2018-10-16 13:46:57', '2018-10-16 13:46:57'),
(112, 82, 1, 0, '0000-00-00 00:00:00', '2018-10-16 13:47:08', '2018-10-16 13:47:08'),
(113, 83, 1, 0, '0000-00-00 00:00:00', '2018-10-16 13:51:12', '2018-10-16 13:51:12'),
(114, 84, 4, 0, '0000-00-00 00:00:00', '2018-10-16 14:01:53', '2018-10-16 14:01:53'),
(115, 85, 4, 0, '0000-00-00 00:00:00', '2018-10-16 14:01:53', '2018-10-16 14:01:53'),
(116, 86, 1, 0, '0000-00-00 00:00:00', '2018-10-16 14:01:53', '2018-10-16 14:01:53'),
(117, 87, 4, 0, '0000-00-00 00:00:00', '2018-10-16 14:01:53', '2018-10-16 14:01:53'),
(118, 88, 1, 0, '0000-00-00 00:00:00', '2018-10-16 14:02:14', '2018-10-16 14:02:14'),
(119, 89, 2, 0, '0000-00-00 00:00:00', '2018-10-16 15:54:15', '2018-10-16 15:54:15'),
(120, 89, 3, 0, '0000-00-00 00:00:00', '2018-10-16 15:54:15', '2018-10-16 15:54:15'),
(121, 90, 1, 0, '0000-00-00 00:00:00', '2018-10-16 15:57:00', '2018-10-16 15:57:00'),
(122, 90, 2, 0, '0000-00-00 00:00:00', '2018-10-16 15:57:00', '2018-10-16 15:57:00'),
(123, 90, 3, 0, '0000-00-00 00:00:00', '2018-10-16 15:57:00', '2018-10-16 15:57:00'),
(124, 90, 4, 0, '0000-00-00 00:00:00', '2018-10-16 15:57:00', '2018-10-16 15:57:00'),
(125, 90, 10, 0, '0000-00-00 00:00:00', '2018-10-16 15:57:00', '2018-10-16 15:57:00'),
(126, 91, 1, 0, '0000-00-00 00:00:00', '2018-10-16 15:57:59', '2018-10-16 15:57:59'),
(127, 92, 1, 0, '0000-00-00 00:00:00', '2018-10-16 16:04:57', '2018-10-16 16:04:57'),
(128, 93, 1, 0, '0000-00-00 00:00:00', '2018-10-16 16:05:41', '2018-10-16 16:05:41'),
(129, 94, 1, 0, '0000-00-00 00:00:00', '2018-10-16 16:05:56', '2018-10-16 16:05:56'),
(130, 95, 1, 0, '0000-00-00 00:00:00', '2018-10-16 16:06:16', '2018-10-16 16:06:16'),
(131, 96, 1, 0, '0000-00-00 00:00:00', '2018-10-16 16:12:26', '2018-10-16 16:12:26'),
(132, 97, 4, 0, '0000-00-00 00:00:00', '2018-10-16 16:12:36', '2018-10-16 16:12:36'),
(133, 98, 1, 0, '0000-00-00 00:00:00', '2018-10-16 16:13:06', '2018-10-16 16:13:06'),
(134, 99, 2, 0, '0000-00-00 00:00:00', '2018-10-16 16:15:50', '2018-10-16 16:15:50'),
(135, 100, 2, 0, '0000-00-00 00:00:00', '2018-10-16 16:44:19', '2018-10-16 16:44:19'),
(136, 100, 3, 0, '0000-00-00 00:00:00', '2018-10-16 16:44:19', '2018-10-16 16:44:19'),
(137, 101, 2, 0, '0000-00-00 00:00:00', '2018-10-16 16:51:20', '2018-10-16 16:51:20'),
(138, 101, 3, 0, '0000-00-00 00:00:00', '2018-10-16 16:51:20', '2018-10-16 16:51:20'),
(139, 102, 2, 0, '0000-00-00 00:00:00', '2018-10-16 16:53:08', '2018-10-16 16:53:08'),
(140, 102, 3, 0, '0000-00-00 00:00:00', '2018-10-16 16:53:08', '2018-10-16 16:53:08'),
(141, 103, 2, 0, '0000-00-00 00:00:00', '2018-10-16 16:53:19', '2018-10-16 16:53:19'),
(142, 103, 3, 0, '0000-00-00 00:00:00', '2018-10-16 16:53:19', '2018-10-16 16:53:19'),
(143, 104, 2, 0, '0000-00-00 00:00:00', '2018-10-16 16:53:52', '2018-10-16 16:53:52'),
(144, 104, 3, 0, '0000-00-00 00:00:00', '2018-10-16 16:53:52', '2018-10-16 16:53:52'),
(145, 105, 2, 0, '0000-00-00 00:00:00', '2018-10-16 16:54:59', '2018-10-16 16:54:59'),
(146, 105, 3, 0, '0000-00-00 00:00:00', '2018-10-16 16:54:59', '2018-10-16 16:54:59'),
(147, 106, 2, 0, '0000-00-00 00:00:00', '2018-10-16 16:55:26', '2018-10-16 16:55:26'),
(148, 106, 3, 0, '0000-00-00 00:00:00', '2018-10-16 16:55:26', '2018-10-16 16:55:26'),
(149, 107, 2, 0, '0000-00-00 00:00:00', '2018-10-16 16:56:27', '2018-10-16 16:56:27'),
(150, 107, 3, 0, '0000-00-00 00:00:00', '2018-10-16 16:56:27', '2018-10-16 16:56:27'),
(151, 108, 2, 0, '0000-00-00 00:00:00', '2018-10-16 16:56:52', '2018-10-16 16:56:52'),
(152, 108, 3, 0, '0000-00-00 00:00:00', '2018-10-16 16:56:52', '2018-10-16 16:56:52'),
(153, 109, 2, 0, '0000-00-00 00:00:00', '2018-10-16 17:02:44', '2018-10-16 17:02:44'),
(154, 109, 3, 0, '0000-00-00 00:00:00', '2018-10-16 17:02:44', '2018-10-16 17:02:44'),
(155, 110, 2, 0, '0000-00-00 00:00:00', '2018-10-16 17:05:51', '2018-10-16 17:05:51'),
(156, 110, 3, 0, '0000-00-00 00:00:00', '2018-10-16 17:05:51', '2018-10-16 17:05:51'),
(157, 111, 2, 0, '0000-00-00 00:00:00', '2018-10-16 17:52:23', '2018-10-16 17:52:23'),
(158, 111, 3, 0, '0000-00-00 00:00:00', '2018-10-16 17:52:23', '2018-10-16 17:52:23'),
(159, 112, 1, 0, '0000-00-00 00:00:00', '2018-10-16 18:34:32', '2018-10-16 18:34:32'),
(160, 113, 1, 0, '0000-00-00 00:00:00', '2018-10-16 18:34:35', '2018-10-16 18:34:35'),
(161, 114, 2, 0, '0000-00-00 00:00:00', '2018-10-16 18:37:10', '2018-10-16 18:37:10'),
(162, 115, 1, 0, '0000-00-00 00:00:00', '2018-10-16 19:12:48', '2018-10-16 19:12:48'),
(163, 115, 2, 0, '0000-00-00 00:00:00', '2018-10-16 19:12:48', '2018-10-16 19:12:48'),
(164, 115, 3, 0, '0000-00-00 00:00:00', '2018-10-16 19:12:48', '2018-10-16 19:12:48'),
(165, 115, 4, 0, '0000-00-00 00:00:00', '2018-10-16 19:12:48', '2018-10-16 19:12:48'),
(166, 115, 10, 0, '0000-00-00 00:00:00', '2018-10-16 19:12:48', '2018-10-16 19:12:48'),
(167, 116, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:02:55', '2018-10-16 20:02:55'),
(168, 116, 2, 0, '0000-00-00 00:00:00', '2018-10-16 20:02:55', '2018-10-16 20:02:55'),
(169, 116, 3, 0, '0000-00-00 00:00:00', '2018-10-16 20:02:55', '2018-10-16 20:02:55'),
(170, 116, 4, 0, '0000-00-00 00:00:00', '2018-10-16 20:02:55', '2018-10-16 20:02:55'),
(171, 116, 10, 0, '0000-00-00 00:00:00', '2018-10-16 20:02:55', '2018-10-16 20:02:55'),
(172, 117, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:02:59', '2018-10-16 20:02:59'),
(173, 117, 3, 0, '0000-00-00 00:00:00', '2018-10-16 20:02:59', '2018-10-16 20:02:59'),
(174, 118, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:03:16', '2018-10-16 20:03:16'),
(175, 119, 2, 0, '0000-00-00 00:00:00', '2018-10-16 20:03:17', '2018-10-16 20:03:17'),
(178, 122, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:19:19', '2018-10-16 20:19:19'),
(179, 123, 2, 0, '0000-00-00 00:00:00', '2018-10-16 20:19:23', '2018-10-16 20:19:23'),
(181, 125, 4, 0, '0000-00-00 00:00:00', '2018-10-16 20:19:42', '2018-10-16 20:19:42'),
(182, 126, 4, 0, '0000-00-00 00:00:00', '2018-10-16 20:19:42', '2018-10-16 20:19:42'),
(185, 129, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:20:18', '2018-10-16 20:20:18'),
(186, 130, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:20:37', '2018-10-16 20:20:37'),
(187, 131, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:20:39', '2018-10-16 20:20:39'),
(188, 132, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:20:46', '2018-10-16 20:20:46'),
(189, 133, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:20:55', '2018-10-16 20:20:55'),
(191, 135, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:21:44', '2018-10-16 20:21:44'),
(193, 137, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:22:12', '2018-10-16 20:22:12'),
(196, 140, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:22:52', '2018-10-16 20:22:52'),
(198, 142, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:23:22', '2018-10-16 20:23:22'),
(199, 143, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:24:13', '2018-10-16 20:24:13'),
(200, 144, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:24:14', '2018-10-16 20:24:14'),
(201, 145, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:24:37', '2018-10-16 20:24:37'),
(202, 146, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:24:45', '2018-10-16 20:24:45'),
(203, 147, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:24:54', '2018-10-16 20:24:54'),
(204, 148, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:25:08', '2018-10-16 20:25:08'),
(205, 149, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:25:21', '2018-10-16 20:25:21'),
(206, 150, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:25:25', '2018-10-16 20:25:25'),
(207, 151, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:25:34', '2018-10-16 20:25:34'),
(208, 152, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:25:34', '2018-10-16 20:25:34'),
(209, 153, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:25:59', '2018-10-16 20:25:59'),
(210, 154, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:26:13', '2018-10-16 20:26:13'),
(211, 155, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:26:13', '2018-10-16 20:26:13'),
(212, 156, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:26:22', '2018-10-16 20:26:22'),
(213, 157, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:26:22', '2018-10-16 20:26:22'),
(214, 158, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:26:29', '2018-10-16 20:26:29'),
(215, 159, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:26:36', '2018-10-16 20:26:36'),
(216, 160, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:26:45', '2018-10-16 20:26:45'),
(217, 161, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:27:19', '2018-10-16 20:27:19'),
(218, 162, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:27:19', '2018-10-16 20:27:19'),
(219, 163, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:27:29', '2018-10-16 20:27:29'),
(220, 164, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:27:41', '2018-10-16 20:27:41'),
(221, 165, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:27:41', '2018-10-16 20:27:41'),
(222, 166, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:27:47', '2018-10-16 20:27:47'),
(223, 167, 2, 0, '0000-00-00 00:00:00', '2018-10-16 20:27:48', '2018-10-16 20:27:48'),
(224, 168, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:27:48', '2018-10-16 20:27:48'),
(225, 169, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:28:01', '2018-10-16 20:28:01'),
(226, 170, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:28:01', '2018-10-16 20:28:01'),
(227, 171, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:28:30', '2018-10-16 20:28:30'),
(228, 172, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:28:30', '2018-10-16 20:28:30'),
(229, 173, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:28:32', '2018-10-16 20:28:32'),
(230, 174, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:28:32', '2018-10-16 20:28:32'),
(231, 175, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:41:33', '2018-10-16 20:41:33'),
(232, 175, 2, 0, '0000-00-00 00:00:00', '2018-10-16 20:41:33', '2018-10-16 20:41:33'),
(233, 175, 3, 0, '0000-00-00 00:00:00', '2018-10-16 20:41:33', '2018-10-16 20:41:33'),
(234, 175, 4, 0, '0000-00-00 00:00:00', '2018-10-16 20:41:33', '2018-10-16 20:41:33'),
(235, 175, 10, 0, '0000-00-00 00:00:00', '2018-10-16 20:41:33', '2018-10-16 20:41:33'),
(236, 176, 2, 0, '0000-00-00 00:00:00', '2018-10-16 20:41:36', '2018-10-16 20:41:36'),
(237, 177, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:41:36', '2018-10-16 20:41:36'),
(238, 177, 2, 0, '0000-00-00 00:00:00', '2018-10-16 20:41:36', '2018-10-16 20:41:36'),
(239, 177, 3, 0, '0000-00-00 00:00:00', '2018-10-16 20:41:36', '2018-10-16 20:41:36'),
(240, 177, 4, 0, '0000-00-00 00:00:00', '2018-10-16 20:41:36', '2018-10-16 20:41:36'),
(241, 177, 10, 0, '0000-00-00 00:00:00', '2018-10-16 20:41:36', '2018-10-16 20:41:36'),
(242, 178, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:42:52', '2018-10-16 20:42:52'),
(243, 178, 2, 0, '0000-00-00 00:00:00', '2018-10-16 20:42:52', '2018-10-16 20:42:52'),
(244, 178, 3, 0, '0000-00-00 00:00:00', '2018-10-16 20:42:52', '2018-10-16 20:42:52'),
(245, 178, 4, 0, '0000-00-00 00:00:00', '2018-10-16 20:42:52', '2018-10-16 20:42:52'),
(246, 178, 10, 0, '0000-00-00 00:00:00', '2018-10-16 20:42:52', '2018-10-16 20:42:52'),
(247, 179, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:42:54', '2018-10-16 20:42:54'),
(248, 179, 2, 0, '0000-00-00 00:00:00', '2018-10-16 20:42:54', '2018-10-16 20:42:54'),
(249, 179, 3, 0, '0000-00-00 00:00:00', '2018-10-16 20:42:54', '2018-10-16 20:42:54'),
(250, 179, 4, 0, '0000-00-00 00:00:00', '2018-10-16 20:42:54', '2018-10-16 20:42:54'),
(251, 179, 10, 0, '0000-00-00 00:00:00', '2018-10-16 20:42:54', '2018-10-16 20:42:54'),
(252, 180, 3, 0, '0000-00-00 00:00:00', '2018-10-16 20:43:06', '2018-10-16 20:43:06'),
(253, 180, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:43:06', '2018-10-16 20:43:06'),
(254, 181, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:44:39', '2018-10-16 20:44:39'),
(255, 182, 2, 0, '0000-00-00 00:00:00', '2018-10-16 20:44:40', '2018-10-16 20:44:40'),
(256, 183, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:44:41', '2018-10-16 20:44:41'),
(257, 184, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:44:41', '2018-10-16 20:44:41'),
(258, 185, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:44:42', '2018-10-16 20:44:42'),
(259, 186, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:44:43', '2018-10-16 20:44:43'),
(260, 187, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:44:44', '2018-10-16 20:44:44'),
(261, 188, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:44:44', '2018-10-16 20:44:44'),
(262, 189, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:53:52', '2018-10-16 20:53:52'),
(263, 190, 2, 0, '0000-00-00 00:00:00', '2018-10-16 20:53:53', '2018-10-16 20:53:53'),
(264, 191, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:53:54', '2018-10-16 20:53:54'),
(265, 192, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:53:54', '2018-10-16 20:53:54'),
(266, 193, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:53:55', '2018-10-16 20:53:55'),
(267, 194, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:53:55', '2018-10-16 20:53:55'),
(268, 195, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:53:56', '2018-10-16 20:53:56'),
(269, 196, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:53:57', '2018-10-16 20:53:57'),
(270, 197, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:53:58', '2018-10-16 20:53:58'),
(271, 198, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:53:58', '2018-10-16 20:53:58'),
(272, 199, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:59:54', '2018-10-16 20:59:54'),
(273, 200, 2, 0, '0000-00-00 00:00:00', '2018-10-16 20:59:55', '2018-10-16 20:59:55'),
(274, 201, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:59:56', '2018-10-16 20:59:56'),
(275, 202, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:59:56', '2018-10-16 20:59:56'),
(276, 203, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:59:56', '2018-10-16 20:59:56'),
(277, 204, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:59:56', '2018-10-16 20:59:56'),
(278, 205, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:59:57', '2018-10-16 20:59:57'),
(279, 206, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:59:58', '2018-10-16 20:59:58'),
(280, 207, 1, 0, '0000-00-00 00:00:00', '2018-10-16 20:59:59', '2018-10-16 20:59:59'),
(281, 208, 1, 0, '0000-00-00 00:00:00', '2018-10-16 21:00:00', '2018-10-16 21:00:00'),
(282, 209, 1, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:12', '2018-10-16 21:13:12'),
(283, 209, 2, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:12', '2018-10-16 21:13:12'),
(284, 209, 3, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:12', '2018-10-16 21:13:12'),
(285, 209, 4, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:12', '2018-10-16 21:13:12'),
(286, 209, 10, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:12', '2018-10-16 21:13:12'),
(287, 210, 1, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:14', '2018-10-16 21:13:14'),
(288, 210, 2, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:14', '2018-10-16 21:13:14'),
(289, 210, 3, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:14', '2018-10-16 21:13:14'),
(290, 210, 4, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:14', '2018-10-16 21:13:14'),
(291, 210, 10, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:14', '2018-10-16 21:13:14'),
(292, 211, 3, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:24', '2018-10-16 21:13:24'),
(293, 211, 1, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:24', '2018-10-16 21:13:24'),
(294, 212, 1, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:50', '2018-10-16 21:13:50'),
(295, 213, 2, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:51', '2018-10-16 21:13:51'),
(296, 214, 1, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:52', '2018-10-16 21:13:52'),
(297, 215, 1, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:52', '2018-10-16 21:13:52'),
(298, 216, 1, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:53', '2018-10-16 21:13:53'),
(299, 217, 1, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:53', '2018-10-16 21:13:53'),
(300, 218, 1, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:54', '2018-10-16 21:13:54'),
(301, 219, 1, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:54', '2018-10-16 21:13:54'),
(302, 220, 1, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:55', '2018-10-16 21:13:55'),
(303, 221, 1, 0, '0000-00-00 00:00:00', '2018-10-16 21:13:56', '2018-10-16 21:13:56'),
(304, 224, 1, 0, '0000-00-00 00:00:00', '2018-10-16 21:48:32', '2018-10-16 21:48:32'),
(305, 225, 1, 0, '0000-00-00 00:00:00', '2018-10-16 21:48:33', '2018-10-16 21:48:33');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('014883b030b5985b0e39af6c0b67c7874b3a10450127c555362c58fd3f75f5fb8d28168852aed261', 2, 1, 'Personal Access Token', '[]', 0, '2018-10-16 22:56:24', '2018-10-16 22:56:24', '2018-10-23 22:56:24'),
('046f027e416a30dd1a1b4c379cb75b906463a885edf324623d02e2b00f0461665129cfa4c50c2f83', 43, 1, 'Personal Access Token', '[]', 0, '2018-09-29 11:17:17', '2018-09-29 11:17:17', '2018-10-06 14:17:17'),
('09966613df22e7eadcd76d9f099869ed9c8ee2ee8396f6b761df50ad78df09a81ce6ec186dc9e20c', 43, 1, 'Personal Access Token', '[]', 0, '2018-09-29 19:47:33', '2018-09-29 19:47:33', '2018-10-06 22:47:33'),
('0b24608efea163e2fb78814dfdbcdd457408aa93cdcd202c3e57025905dc20b8075dfb217c1fca6b', 2, 1, 'Personal Access Token', '[]', 0, '2018-10-16 22:57:43', '2018-10-16 22:57:43', '2018-10-23 22:57:43'),
('1e506784b8cea62fa5a79722079fcb0a831e673a6db2bf85b3878c7bdba5f2b4b111d49ccde9eb10', 42, 1, 'Personal Access Token', '[]', 0, '2018-09-28 21:01:43', '2018-09-28 21:01:43', '2018-10-06 00:01:44'),
('3026e48dc51535bc789b1dcfcf6958afb9545a377555855e085c3f60b99fe18632df442cef1e9f2b', 2, 1, 'Personal Access Token', '[]', 0, '2018-10-16 23:00:16', '2018-10-16 23:00:16', '2018-10-23 23:00:16'),
('302e349eb2734614bbef594ed6cdd2adc634caeebe0785a1d2227e9e9d9a4d85246ff1e50454661f', 43, 1, 'Personal Access Token', '[]', 0, '2018-10-03 16:45:47', '2018-10-03 16:45:47', '2018-10-10 19:45:48'),
('3bb274b0eed5dae263c74f3ea3380ee2300c5b0a8dcd5c8d1c35e804a78ba86ab4b3f9eb9af42515', 43, 1, 'Personal Access Token', '[]', 0, '2018-10-13 22:46:07', '2018-10-13 22:46:07', '2018-10-20 22:46:08'),
('3d23f9d79115f6f3ff505c68a455492abc9ba59b277ce2b3f3496e5523e0ec465c528b1ef8935936', 2, 1, 'Personal Access Token', '[]', 0, '2018-10-13 23:35:39', '2018-10-13 23:35:39', '2019-10-13 23:35:39'),
('40d1e2cf21f513e80344ce0aad58ea0f62cd048aea9b4922d09500d63e26a2751d0c05522b2b9304', 43, 1, 'Personal Access Token', '[]', 0, '2018-09-30 01:08:24', '2018-09-30 01:08:24', '2018-10-07 04:08:25'),
('514d14831cabef16b5946ee4a9d858dcda2f04db637bcf4871676cfeff38ef725e21786339328a6f', 2, 1, 'Personal Access Token', '[]', 0, '2018-10-17 01:29:49', '2018-10-17 01:29:49', '2019-10-17 01:29:49'),
('539f2ed9f1c9b11edcf0fb11c003de28264f585e78586ca60876342de52acfa7ff679869d4f36c58', 43, 1, 'Personal Access Token', '[]', 0, '2018-09-29 19:27:30', '2018-09-29 19:27:30', '2019-09-29 22:27:30'),
('5609fcb54c03277f93b81dba21d883c971fc0ca7c3f46372aea1d8a642b7210715fee5484283e272', 43, 1, 'Personal Access Token', '[]', 0, '2018-09-29 11:08:44', '2018-09-29 11:08:44', '2018-10-06 14:08:45'),
('59bc4f79d3e7ed3f7be38312b5fa6f171f503c60a9e768d9ca082d72d1182d38e6828b4950edacb9', 41, 1, 'Personal Access Token', '[]', 0, '2018-09-28 20:44:47', '2018-09-28 20:44:47', '2018-10-05 23:44:47'),
('660e92785276ee442135a5a686801524b2e6a99a65092e8b06c592bae64412eb97680ad8ce002240', 1, 1, 'Personal Access Token', '[]', 0, '2018-10-15 22:21:27', '2018-10-15 22:21:27', '2019-10-15 22:21:27'),
('6883d4d8b11a96fc08a2bd86acd40281581fbfe02ed3677fec6dacf458d84253fe92f9e56843c2c8', 2, 1, 'Personal Access Token', '[]', 0, '2018-10-13 23:06:49', '2018-10-13 23:06:49', '2018-10-20 23:06:49'),
('77eecfb833716c8d6387ff53b50b7988d3fdffb3ba3a3adc0b214812eef6dbd338c4ee942a9adbc4', 2, 1, 'Personal Access Token', '[]', 0, '2018-10-14 16:21:13', '2018-10-14 16:21:13', '2018-10-21 16:21:13'),
('88c20f07ae47d9217828e5de944f50976f49da79ca213365c77ba4ed028272109bcbe3cb2aa6ae38', 2, 1, 'Personal Access Token', '[]', 0, '2018-10-15 22:04:58', '2018-10-15 22:04:58', '2019-10-15 22:04:58'),
('8aeb527381cd8dce540cddda510f6a1fe5ece352bf7e5687ac55d8a7d89e8bbf9bb66c6f275760b1', 2, 1, 'Personal Access Token', '[]', 0, '2018-10-14 16:18:17', '2018-10-14 16:18:17', '2018-10-21 16:18:17'),
('9fad04c81d09f49cef4a342c30f3a994a34cd682effc277acc499f5fccac298016023c5a23386220', 2, 1, 'Personal Access Token', '[]', 0, '2018-10-16 22:12:26', '2018-10-16 22:12:26', '2018-10-23 22:12:27'),
('b595a32993fb5701d767bae3ac239bc39308de3d9ecedc3398e63459cb824e10709cea0015332992', 43, 1, 'Personal Access Token', '[]', 0, '2018-09-29 19:50:33', '2018-09-29 19:50:33', '2018-10-06 22:50:34'),
('c211a70b2f4e74c26dbbecfbdae6392b98b53f44ee1e7536dcad7644032b522f25242b19015ffc7d', 1, 1, 'Personal Access Token', '[]', 0, '2018-10-13 23:35:10', '2018-10-13 23:35:10', '2019-10-13 23:35:10'),
('c281aa796183932a7a2f4f2cf169c5e3d69bd6db8402d20984686e7fc6dcbe9cba999974c28729c3', 43, 1, 'Personal Access Token', '[]', 0, '2018-09-29 19:08:08', '2018-09-29 19:08:08', '2018-10-06 22:08:09'),
('cb61736a4f5466282bfaf001bf348663258ea81534a494d0ea19f6b114805d02fadc45ae8a5bcc73', 41, 1, 'Personal Access Token', '[]', 0, '2018-09-28 20:30:46', '2018-09-28 20:30:46', '2018-10-05 23:30:46'),
('d788fad120467efdefdb54ab0b8d1d05899c3b539ad29184ea91b618a7dfd4f1853aaa19d0cd5a6c', 43, 1, 'Personal Access Token', '[]', 0, '2018-09-29 19:51:15', '2018-09-29 19:51:15', '2018-10-06 22:51:16'),
('dbe15f66e2055b0ff4fa3ab94ec62e53aa3b2e014d78af5b19951178bca63f6f0df5f7687cf42bca', 43, 1, 'Personal Access Token', '[]', 0, '2018-10-13 23:05:51', '2018-10-13 23:05:51', '2018-10-20 23:05:51'),
('f43f570b93f9f8e351f34c2c6682ff14fc1344136d045d3ca64bc931188d3847eda64b64dcc853a6', 2, 1, 'Personal Access Token', '[]', 0, '2018-10-16 22:38:28', '2018-10-16 22:38:28', '2018-10-23 22:38:28'),
('fc3564327419171c7450eeb622efe81dc57227573bab62d9725506302f91cbe878d7f916a75b822b', 41, 1, 'Personal Access Token', '[]', 0, '2018-09-28 20:30:10', '2018-09-28 20:30:10', '2019-09-28 23:30:10');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'a4rafIWwlI8gNzQoRpOgfEhuxjgELyT1zXiE29kv', 'http://localhost', 1, 0, 0, '2018-09-28 20:30:01', '2018-09-28 20:30:01');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-09-28 20:30:01', '2018-09-28 20:30:01');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `mini_description` text COLLATE utf8_unicode_ci NOT NULL,
  `universities_id` int(11) NOT NULL DEFAULT '0',
  `colleges_id` int(11) NOT NULL DEFAULT '0',
  `years` text COLLATE utf8_unicode_ci NOT NULL,
  `semesters` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `first_color` text COLLATE utf8_unicode_ci NOT NULL,
  `second_color` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `name`, `description`, `mini_description`, `universities_id`, `colleges_id`, `years`, `semesters`, `price`, `first_color`, `second_color`, `created_at`, `updated_at`) VALUES
(1, 'Gold', '<p>The golden offer bla bla bla.. The golden offer bla bla bla.. The golden offer bla bla bla..The golden offer bla bla bla..v</p>\r\n\r\n<p>The golden offer bla bla bla..The golden offer bla bla bla..</p>\r\n\r\n<p>The golden offer bla bla bla..The golden offer bla bla bla..The golden offer bla bla bla..</p>\r\n\r\n<p>The golden offer bla bla bla..</p>', 'The golden offer bla bla bla.. The golden offer bla bla bla.. The golden offer bla bla bla..', 0, 0, 'all', '1,2', 0, '#d4c846', '#c51f1f', '2018-10-17 17:38:25', '2018-10-17 17:38:25');

-- --------------------------------------------------------

--
-- Table structure for table `offers_period`
--

CREATE TABLE `offers_period` (
  `id` int(11) NOT NULL,
  `offers_id` int(11) NOT NULL,
  `period_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `offers_period`
--

INSERT INTO `offers_period` (`id`, `offers_id`, `period_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-10-17 17:38:25', '2018-10-17 17:38:25'),
(2, 1, 2, '2018-10-17 17:38:25', '2018-10-17 17:38:25'),
(3, 1, 3, '2018-10-17 17:38:25', '2018-10-17 17:38:25');

-- --------------------------------------------------------

--
-- Table structure for table `offers_user`
--

CREATE TABLE `offers_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `offers_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `offers_user`
--

INSERT INTO `offers_user` (`id`, `user_id`, `offers_id`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2018-10-17 17:41:57', '2018-10-17 17:41:57'),
(2, 2, 1, 1, '2018-10-17 17:42:27', '2018-10-17 17:42:27');

-- --------------------------------------------------------

--
-- Table structure for table `operation_log`
--

CREATE TABLE `operation_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `input` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `operation_log`
--

INSERT INTO `operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-16 23:35:05', '2018-10-16 23:35:05'),
(2, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 00:55:00', '2018-10-17 00:55:00'),
(3, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 00:55:05', '2018-10-17 00:55:05'),
(4, 1, 'admin/auth/roles/3/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 00:55:09', '2018-10-17 00:55:09'),
(5, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 00:55:22', '2018-10-17 00:55:22'),
(6, 1, 'admin/auth/permissions/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 00:55:28', '2018-10-17 00:55:28'),
(7, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 00:55:35', '2018-10-17 00:55:35'),
(8, 1, 'admin/Universities', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 00:55:39', '2018-10-17 00:55:39'),
(9, 1, 'admin/Universities/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 00:55:40', '2018-10-17 00:55:40'),
(10, 1, 'admin/Universities', 'POST', '127.0.0.1', '{\"name\":\"Damascus University\",\"description\":\"<p>The University of Damascus is the largest and oldest university in Syria, located in the capital Damascus and has campuses in other Syrian cities. It was founded in 1923 through the merger of the School of Medicine (established 1903) and the Institute of Law (established 1913). Until 1958 it was named the Syrian University, but the name changed after the founding of the University of Aleppo. There are nine public universities and more than ten private ones in Syria. Damascus university was one of the most reputable universities in the Middle East before the war in Syria started in 2011<\\/p>\",\"_token\":\"tO3PWrqJQ1jLegqNG3Syz9yEHVdibP0fukF1xRxy\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Universities\"}', '2018-10-17 00:58:32', '2018-10-17 00:58:32'),
(11, 1, 'admin/Universities', 'GET', '127.0.0.1', '[]', '2018-10-17 00:58:32', '2018-10-17 00:58:32'),
(12, 1, 'admin/Universities/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 00:58:36', '2018-10-17 00:58:36'),
(13, 1, 'admin/Universities', 'POST', '127.0.0.1', '{\"name\":\"Aleppo University\",\"description\":\"<p>University of Aleppo is a public university located in Aleppo, Syria. It is the second largest university in Syria after the University of Damascus.<\\/p>\\r\\n\\r\\n<p>During 2005-2006 the University had over 61,000 undergraduate students, over 1,500 postgraduate students and approximately 2,400 faculty members. The university has 25 faculties and 10 intermediate colleges.<\\/p>\",\"_token\":\"tO3PWrqJQ1jLegqNG3Syz9yEHVdibP0fukF1xRxy\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Universities\"}', '2018-10-17 01:00:44', '2018-10-17 01:00:44'),
(14, 1, 'admin/Universities', 'GET', '127.0.0.1', '[]', '2018-10-17 01:00:44', '2018-10-17 01:00:44'),
(15, 1, 'admin/Universities/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 01:02:07', '2018-10-17 01:02:07'),
(16, 1, 'admin/Universities', 'POST', '127.0.0.1', '{\"name\":\"Tishreen University\",\"description\":\"<p>Tishreen University, is a public university located in Latakia, Syria. It is the third largest university in Syria. It was established on 20 May 1971 at the University of Latakia before the name was changed to commemorate October War.<\\/p>\\r\\n\\r\\n<p>In the beginning, the university only had 3 faculties, Arabic literature, science, and agriculture and an enrollment of 983 students during the 1970s.However that number largely grew throughout the years to reach more than 70,000 students, making Tishreen University the third largest in Syria, with the number of its faculties rising from 3 to 21; including Medicine, Pharmacy, Dentistry, Science, Nursing, Education, Agriculture, Law, History, Electrical and Technical Engineering and Arts, among others<\\/p>\",\"_token\":\"tO3PWrqJQ1jLegqNG3Syz9yEHVdibP0fukF1xRxy\"}', '2018-10-17 01:02:52', '2018-10-17 01:02:52'),
(17, 1, 'admin/Universities', 'GET', '127.0.0.1', '[]', '2018-10-17 01:02:52', '2018-10-17 01:02:52'),
(18, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 01:05:24', '2018-10-17 01:05:24'),
(19, 1, 'admin/Universities', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 01:05:27', '2018-10-17 01:05:27'),
(20, 1, 'admin/Universities/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 01:05:29', '2018-10-17 01:05:29'),
(21, 1, 'admin/Universities', 'POST', '127.0.0.1', '{\"name\":\"Al-Baath University\",\"description\":\"<p>Al-Baath University, founded in 1979, is a public university located in the city of Homs, Syria, 180 km north of Damascus. It is Syria&#39;s fourth-largest university.<\\/p>\\r\\n\\r\\n<p>The university was established in 1979; it was established by Presidential Decree No. 44 issued by Hafez al-Assad.[citation needed]<\\/p>\\r\\n\\r\\n<p>Al-Baath University has 22 faculties, 5 intermediate institutes, 40,000 regular students, 20,000 students in open learning, 1310 high studies students and 622 faculty members. The library contains some 63,000 volumes (as of 2011).<\\/p>\",\"_token\":\"tO3PWrqJQ1jLegqNG3Syz9yEHVdibP0fukF1xRxy\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Universities\"}', '2018-10-17 01:07:01', '2018-10-17 01:07:01'),
(22, 1, 'admin/Universities', 'GET', '127.0.0.1', '[]', '2018-10-17 01:07:08', '2018-10-17 01:07:08'),
(23, 1, 'admin/Universities/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 01:08:15', '2018-10-17 01:08:15'),
(24, 1, 'admin/Universities', 'POST', '127.0.0.1', '{\"name\":\"Syrian Virtual University\",\"description\":\"<p>The Syrian Virtual University is a Syrian educational institution established by the Syrian Ministry of Higher Education. It provides virtual education (using the Internet) to students from around the world. It was established on 2 September 2002 and is the first virtual education institution in the region, and as of 2006, remains the only one. The goals of the SVU include offering education to those who want to learn but cannot afford to do so by going to a &quot;brick and mortar&quot; university. It is headquartered at the Ministry of Higher Education building, Damascus.<\\/p>\",\"_token\":\"tO3PWrqJQ1jLegqNG3Syz9yEHVdibP0fukF1xRxy\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Universities\"}', '2018-10-17 01:08:47', '2018-10-17 01:08:47'),
(25, 1, 'admin/Universities', 'GET', '127.0.0.1', '[]', '2018-10-17 01:08:47', '2018-10-17 01:08:47'),
(26, 1, 'admin/Universities/12/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 01:09:34', '2018-10-17 01:09:34'),
(27, 1, 'admin/Universities/12', 'PUT', '127.0.0.1', '{\"name\":\"Al-Baath University\",\"description\":\"<p>Al-Baath University, founded in 1979, is a public university located in the city of Homs, Syria, 180 km north of Damascus. It is Syria&#39;s fourth-largest university.<\\/p>\\r\\n\\r\\n<p>The university was established in 1979, it was established by Presidential Decree No. 44 issued by Hafez al-Assad.[citation needed]<\\/p>\\r\\n\\r\\n<p>Al-Baath University has 22 faculties, 5 intermediate institutes, 40,000 regular students, 20,000 students in open learning, 1310 high studies students and 622 faculty members. The library contains some 63,000 volumes (as of 2011).<\\/p>\",\"_token\":\"tO3PWrqJQ1jLegqNG3Syz9yEHVdibP0fukF1xRxy\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Universities\"}', '2018-10-17 01:09:40', '2018-10-17 01:09:40'),
(28, 1, 'admin/Universities', 'GET', '127.0.0.1', '[]', '2018-10-17 01:09:40', '2018-10-17 01:09:40'),
(29, 1, 'admin/Colleges', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 01:15:07', '2018-10-17 01:15:07'),
(30, 1, 'admin/Colleges/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 01:15:08', '2018-10-17 01:15:08'),
(31, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 01:15:12', '2018-10-17 01:15:12'),
(32, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 01:29:21', '2018-10-17 01:29:21'),
(33, 1, 'admin/Colleges', 'POST', '127.0.0.1', '{\"name\":\"Information Technology Engineering\",\"description\":\"<p>The Syrian Virtual University is a Syrian educational institution established by the Syrian Ministry of Higher Education. It provides virtual education (using the Internet) to students from around the world. It was established on 2 September 2002 and is the first virtual education institution in the region, and as of 2006, remains the only one. The goals of the SVU include offering education to those who want to learn but cannot afford to do so by going to a &quot;brick and mortar&quot; university. It is headquartered at the Ministry of Higher Education building, Damascus.<\\/p>\",\"num_of_years\":\"5\",\"universities_id\":\"9\",\"cover_font_color\":\"#150d0b\",\"_token\":\"tO3PWrqJQ1jLegqNG3Syz9yEHVdibP0fukF1xRxy\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Colleges\"}', '2018-10-17 01:29:37', '2018-10-17 01:29:37'),
(34, 1, 'admin/Colleges', 'GET', '127.0.0.1', '[]', '2018-10-17 01:29:47', '2018-10-17 01:29:47'),
(35, 1, 'admin/Colleges/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 01:29:51', '2018-10-17 01:29:51'),
(36, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 01:31:37', '2018-10-17 01:31:37'),
(37, 1, 'admin/Colleges', 'POST', '127.0.0.1', '{\"name\":\"Civil Engineering\",\"description\":\"<p>The Syrian Virtual University is a Syrian educational institution established by the Syrian Ministry of Higher Education. It provides virtual education (using the Internet) to students from around the world. It was established on 2 September 2002 and is the first virtual education institution in the region, and as of 2006, remains the only one. The goals of the SVU include offering education to those who want to learn but cannot afford to do so by going to a &quot;brick and mortar&quot; university. It is headquartered at the Ministry of Higher Education building, Damascus.<\\/p>\",\"num_of_years\":\"5\",\"universities_id\":\"9\",\"cover_font_color\":\"#150d0b\",\"_token\":\"tO3PWrqJQ1jLegqNG3Syz9yEHVdibP0fukF1xRxy\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Colleges\"}', '2018-10-17 01:31:50', '2018-10-17 01:31:50'),
(38, 1, 'admin/Colleges/create', 'GET', '127.0.0.1', '[]', '2018-10-17 01:31:53', '2018-10-17 01:31:53'),
(39, 1, 'admin/Colleges', 'POST', '127.0.0.1', '{\"name\":\"Civil Engineering\",\"description\":\"<p>The Syrian Virtual University is a Syrian educational institution established by the Syrian Ministry of Higher Education. It provides virtual education (using the Internet) to students from around the world. It was established on 2 September 2002 and is the first virtual education institution in the region, and as of 2006, remains the only one. The goals of the SVU include offering education to those who want to learn but cannot afford to do so by going to a &quot;brick and mortar&quot; university. It is headquartered at the Ministry of Higher Education building, Damascus.<\\/p>\",\"num_of_years\":\"5\",\"universities_id\":null,\"cover_font_color\":\"#150d0b\",\"_token\":\"tO3PWrqJQ1jLegqNG3Syz9yEHVdibP0fukF1xRxy\"}', '2018-10-17 01:32:47', '2018-10-17 01:32:47'),
(40, 1, 'admin/Colleges/create', 'GET', '127.0.0.1', '[]', '2018-10-17 01:32:47', '2018-10-17 01:32:47'),
(41, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 01:32:51', '2018-10-17 01:32:51'),
(42, 1, 'admin/Colleges', 'POST', '127.0.0.1', '{\"name\":\"Civil Engineering\",\"description\":\"<p>The Syrian Virtual University is a Syrian educational institution established by the Syrian Ministry of Higher Education. It provides virtual education (using the Internet) to students from around the world. It was established on 2 September 2002 and is the first virtual education institution in the region, and as of 2006, remains the only one. The goals of the SVU include offering education to those who want to learn but cannot afford to do so by going to a &quot;brick and mortar&quot; university. It is headquartered at the Ministry of Higher Education building, Damascus.<\\/p>\",\"num_of_years\":\"5\",\"universities_id\":\"9\",\"cover_font_color\":\"#150d0b\",\"_token\":\"tO3PWrqJQ1jLegqNG3Syz9yEHVdibP0fukF1xRxy\"}', '2018-10-17 01:33:00', '2018-10-17 01:33:00'),
(43, 1, 'admin/Colleges', 'GET', '127.0.0.1', '[]', '2018-10-17 01:33:10', '2018-10-17 01:33:10'),
(44, 1, 'admin/Colleges/13/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 01:35:13', '2018-10-17 01:35:13'),
(45, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 01:35:38', '2018-10-17 01:35:38'),
(46, 1, 'admin/Colleges/13', 'PUT', '127.0.0.1', '{\"name\":\"Civil Engineering\",\"description\":\"<p>Civil engineering is a professional engineering discipline that deals with the design, construction, and maintenance of the physical and naturally built environment, including works such as roads, bridges, canals, dams, airports, sewerage systems, pipelines, and railways. Civil engineering is traditionally broken into a number of sub-disciplines. It is considered the second-oldest engineering discipline after military engineering, and it is defined to distinguish non-military engineering from military engineering. Civil engineering takes place in the public sector from municipal through to national governments, and in the private sector from individual homeowners through to international companies.<\\/p>\",\"num_of_years\":\"5\",\"universities_id\":\"9\",\"cover_font_color\":\"#150d0b\",\"_token\":\"tO3PWrqJQ1jLegqNG3Syz9yEHVdibP0fukF1xRxy\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Colleges\"}', '2018-10-17 01:35:41', '2018-10-17 01:35:41'),
(47, 1, 'admin/Colleges', 'GET', '127.0.0.1', '[]', '2018-10-17 01:35:41', '2018-10-17 01:35:41'),
(48, 1, 'admin/Colleges/12/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 01:36:51', '2018-10-17 01:36:51'),
(49, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 01:36:59', '2018-10-17 01:36:59'),
(50, 1, 'admin/Colleges/12', 'PUT', '127.0.0.1', '{\"name\":\"Information Technology Engineering\",\"description\":\"<p>Information technology (IT) is the use of computers to store, retrieve, transmit, and manipulate data, or information, often in the context of a business or other enterprise. IT is considered to be a subset of information and communications technology (ICT).<\\/p>\\r\\n\\r\\n<p>Humans have been storing, retrieving, manipulating, and communicating information since the Sumerians in Mesopotamia developed writing in about 3000 BC, but the term information technology in its modern sense first appeared in a 1958 article published in the Harvard Business Review; authors Harold J. Leavitt and Thomas L. Whisler commented that &quot;the new technology does not yet have a single established name. We shall call it information technology (IT).&quot; Their definition consists of three categories: techniques for processing, the application of statistical and mathematical methods to decision-making, and the simulation of higher-order thinking through computer programs.<\\/p>\",\"num_of_years\":\"5\",\"universities_id\":\"9\",\"cover_font_color\":\"#150d0b\",\"_token\":\"tO3PWrqJQ1jLegqNG3Syz9yEHVdibP0fukF1xRxy\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Colleges\"}', '2018-10-17 01:37:01', '2018-10-17 01:37:01'),
(51, 1, 'admin/Colleges', 'GET', '127.0.0.1', '[]', '2018-10-17 01:37:02', '2018-10-17 01:37:02'),
(52, 1, 'admin/Majors', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 01:37:06', '2018-10-17 01:37:06'),
(53, 1, 'admin/Majors/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 01:37:08', '2018-10-17 01:37:08'),
(54, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 01:37:47', '2018-10-17 01:37:47'),
(55, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 01:37:48', '2018-10-17 01:37:48'),
(56, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 01:37:48', '2018-10-17 01:37:48'),
(57, 1, 'admin/Majors', 'POST', '127.0.0.1', '{\"name\":\"General\",\"description\":\"<p>General<\\/p>\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"years\":[\"1\",\"2\",\"3\",null],\"_token\":\"tO3PWrqJQ1jLegqNG3Syz9yEHVdibP0fukF1xRxy\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Majors\"}', '2018-10-17 01:39:28', '2018-10-17 01:39:28'),
(58, 1, 'admin/Majors', 'GET', '127.0.0.1', '[]', '2018-10-17 01:39:36', '2018-10-17 01:39:36'),
(59, 1, 'admin/Majors/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 01:39:57', '2018-10-17 01:39:57'),
(60, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 01:40:09', '2018-10-17 01:40:09'),
(61, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 01:40:10', '2018-10-17 01:40:10'),
(62, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 01:40:10', '2018-10-17 01:40:10'),
(63, 1, 'admin/Majors', 'POST', '127.0.0.1', '{\"name\":\"Artificial Intelligence\",\"description\":\"<p>Artificial Intelligence<\\/p>\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"years\":[\"4\",\"5\",null],\"_token\":\"tO3PWrqJQ1jLegqNG3Syz9yEHVdibP0fukF1xRxy\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Majors\"}', '2018-10-17 01:40:39', '2018-10-17 01:40:39'),
(64, 1, 'admin/Majors', 'GET', '127.0.0.1', '[]', '2018-10-17 01:40:39', '2018-10-17 01:40:39'),
(65, 1, 'admin/Majors/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 01:40:50', '2018-10-17 01:40:50'),
(66, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 01:40:56', '2018-10-17 01:40:56'),
(67, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 01:40:56', '2018-10-17 01:40:56'),
(68, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 01:40:57', '2018-10-17 01:40:57'),
(69, 1, 'admin/Majors', 'POST', '127.0.0.1', '{\"name\":\"Software Engineering\",\"description\":\"<p>Software Engineering<\\/p>\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"years\":[\"4\",\"5\",null],\"_token\":\"tO3PWrqJQ1jLegqNG3Syz9yEHVdibP0fukF1xRxy\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Majors\"}', '2018-10-17 01:42:21', '2018-10-17 01:42:21'),
(70, 1, 'admin/Majors', 'GET', '127.0.0.1', '[]', '2018-10-17 01:42:25', '2018-10-17 01:42:25'),
(71, 1, 'admin/Majors/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 01:42:29', '2018-10-17 01:42:29'),
(72, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 01:42:46', '2018-10-17 01:42:46'),
(73, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 01:42:47', '2018-10-17 01:42:47'),
(74, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 01:42:47', '2018-10-17 01:42:47'),
(75, 1, 'admin/Majors', 'POST', '127.0.0.1', '{\"name\":\"Networking\",\"description\":\"<p>Networking<\\/p>\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"years\":[\"4\",\"5\",null],\"_token\":\"tO3PWrqJQ1jLegqNG3Syz9yEHVdibP0fukF1xRxy\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Majors\"}', '2018-10-17 01:43:24', '2018-10-17 01:43:24'),
(76, 1, 'admin/Majors', 'GET', '127.0.0.1', '[]', '2018-10-17 01:43:32', '2018-10-17 01:43:32'),
(77, 1, 'admin/Majors/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 01:43:36', '2018-10-17 01:43:36'),
(78, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 01:43:43', '2018-10-17 01:43:43'),
(79, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 01:43:43', '2018-10-17 01:43:43'),
(80, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 01:43:44', '2018-10-17 01:43:44'),
(81, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"13\"}', '2018-10-17 01:43:45', '2018-10-17 01:43:45'),
(82, 1, 'admin/Majors', 'POST', '127.0.0.1', '{\"name\":\"General\",\"description\":\"<p>General<\\/p>\",\"universities_id\":\"9\",\"colleges_id\":\"13\",\"years\":[\"1\",\"2\",\"3\",\"4\",\"5\",null],\"_token\":\"tO3PWrqJQ1jLegqNG3Syz9yEHVdibP0fukF1xRxy\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Majors\"}', '2018-10-17 01:43:52', '2018-10-17 01:43:52'),
(83, 1, 'admin/Majors', 'GET', '127.0.0.1', '[]', '2018-10-17 01:44:05', '2018-10-17 01:44:05'),
(84, 1, 'admin/Course', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 01:44:14', '2018-10-17 01:44:14'),
(85, 1, 'admin/Course/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 02:04:15', '2018-10-17 02:04:15'),
(86, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-10-17 02:06:45', '2018-10-17 02:06:45'),
(87, 1, 'admin/Course', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 02:06:48', '2018-10-17 02:06:48'),
(88, 1, 'admin/Course/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 02:06:50', '2018-10-17 02:06:50'),
(89, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-10-17 02:26:17', '2018-10-17 02:26:17'),
(90, 1, 'admin/Course', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 02:26:48', '2018-10-17 02:26:48'),
(91, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-10-17 13:10:43', '2018-10-17 13:10:43'),
(92, 1, 'admin/Universities', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:10:47', '2018-10-17 13:10:47'),
(93, 1, 'admin/Colleges', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:10:53', '2018-10-17 13:10:53'),
(94, 1, 'admin/Majors', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:11:01', '2018-10-17 13:11:01'),
(95, 1, 'admin/Universities', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:11:12', '2018-10-17 13:11:12'),
(96, 1, 'admin/Colleges', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:11:13', '2018-10-17 13:11:13'),
(97, 1, 'admin/Colleges/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:11:31', '2018-10-17 13:11:31'),
(98, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:13:33', '2018-10-17 13:13:33'),
(99, 1, 'admin/Colleges', 'POST', '127.0.0.1', '{\"name\":\"Medical College\",\"description\":\"<p>The <strong>Faculty of Medicine of <a href=\\\"https:\\/\\/en.wikipedia.org\\/wiki\\/Damascus_University\\\" title=\\\"Damascus University\\\">Damascus University<\\/a><\\/strong> (<a class=\\\"mw-redirect\\\" href=\\\"https:\\/\\/en.wikipedia.org\\/wiki\\/Arabic_language\\\" title=\\\"Arabic language\\\">Arabic<\\/a>: <span dir=\\\"rtl\\\" lang=\\\"ar\\\">\\u0643\\u0644\\u064a\\u0629 \\u0627\\u0644\\u0637\\u0628 \\u0627\\u0644\\u0628\\u0634\\u0631\\u064a \\u0641\\u064a \\u062c\\u0627\\u0645\\u0639\\u0629 \\u062f\\u0645\\u0634\\u0642<\\/span>&lrm;), founded in 1903, was the first university college established in <a href=\\\"https:\\/\\/en.wikipedia.org\\/wiki\\/Syria\\\" title=\\\"Syria\\\">Syria<\\/a>.<\\/p>\\r\\n\\r\\n<p>The school moved to Beirut temporarily during <a href=\\\"https:\\/\\/en.wikipedia.org\\/wiki\\/World_War_I\\\" title=\\\"World War I\\\">World War I<\\/a>. At one point later it was called the <strong>Arab Medical Institute<\\/strong>.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\",\"num_of_years\":\"5\",\"universities_id\":\"9\",\"cover_font_color\":\"#150d0b\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Colleges\"}', '2018-10-17 13:13:53', '2018-10-17 13:13:53'),
(100, 1, 'admin/Colleges/create', 'GET', '127.0.0.1', '[]', '2018-10-17 13:13:53', '2018-10-17 13:13:53'),
(101, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:13:58', '2018-10-17 13:13:58'),
(102, 1, 'admin/Colleges', 'POST', '127.0.0.1', '{\"name\":\"Medical College\",\"description\":\"<p>The <strong>Faculty of Medicine of <a href=\\\"https:\\/\\/en.wikipedia.org\\/wiki\\/Damascus_University\\\" title=\\\"Damascus University\\\">Damascus University<\\/a><\\/strong> (<a class=\\\"mw-redirect\\\" href=\\\"https:\\/\\/en.wikipedia.org\\/wiki\\/Arabic_language\\\" title=\\\"Arabic language\\\">Arabic<\\/a>: <span dir=\\\"rtl\\\" lang=\\\"ar\\\">\\u0643\\u0644\\u064a\\u0629 \\u0627\\u0644\\u0637\\u0628 \\u0627\\u0644\\u0628\\u0634\\u0631\\u064a \\u0641\\u064a \\u062c\\u0627\\u0645\\u0639\\u0629 \\u062f\\u0645\\u0634\\u0642<\\/span>&lrm;), founded in 1903, was the first university college established in <a href=\\\"https:\\/\\/en.wikipedia.org\\/wiki\\/Syria\\\" title=\\\"Syria\\\">Syria<\\/a>.<\\/p>\\r\\n\\r\\n<p>The school moved to Beirut temporarily during <a href=\\\"https:\\/\\/en.wikipedia.org\\/wiki\\/World_War_I\\\" title=\\\"World War I\\\">World War I<\\/a>. At one point later it was called the <strong>Arab Medical Institute<\\/strong>.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\",\"num_of_years\":\"5\",\"universities_id\":\"9\",\"cover_font_color\":\"#150d0b\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\"}', '2018-10-17 13:14:55', '2018-10-17 13:14:55'),
(103, 1, 'admin/Colleges/create', 'GET', '127.0.0.1', '[]', '2018-10-17 13:15:02', '2018-10-17 13:15:02'),
(104, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:15:10', '2018-10-17 13:15:10'),
(105, 1, 'admin/Colleges', 'POST', '127.0.0.1', '{\"name\":\"Medical College\",\"description\":\"<p>The <strong>Faculty of Medicine of <a href=\\\"https:\\/\\/en.wikipedia.org\\/wiki\\/Damascus_University\\\" title=\\\"Damascus University\\\">Damascus University<\\/a><\\/strong> (<a class=\\\"mw-redirect\\\" href=\\\"https:\\/\\/en.wikipedia.org\\/wiki\\/Arabic_language\\\" title=\\\"Arabic language\\\">Arabic<\\/a>: <span dir=\\\"rtl\\\" lang=\\\"ar\\\">\\u0643\\u0644\\u064a\\u0629 \\u0627\\u0644\\u0637\\u0628 \\u0627\\u0644\\u0628\\u0634\\u0631\\u064a \\u0641\\u064a \\u062c\\u0627\\u0645\\u0639\\u0629 \\u062f\\u0645\\u0634\\u0642<\\/span>&lrm;), founded in 1903, was the first university college established in <a href=\\\"https:\\/\\/en.wikipedia.org\\/wiki\\/Syria\\\" title=\\\"Syria\\\">Syria<\\/a>.<\\/p>\\r\\n\\r\\n<p>The school moved to Beirut temporarily during <a href=\\\"https:\\/\\/en.wikipedia.org\\/wiki\\/World_War_I\\\" title=\\\"World War I\\\">World War I<\\/a>. At one point later it was called the <strong>Arab Medical Institute<\\/strong>.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\",\"num_of_years\":\"5\",\"universities_id\":\"9\",\"cover_font_color\":\"#150d0b\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\"}', '2018-10-17 13:15:23', '2018-10-17 13:15:23'),
(106, 1, 'admin/Colleges', 'GET', '127.0.0.1', '[]', '2018-10-17 13:15:29', '2018-10-17 13:15:29'),
(107, 1, 'admin/Colleges/14/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:15:33', '2018-10-17 13:15:33'),
(108, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:15:55', '2018-10-17 13:15:55'),
(109, 1, 'admin/Colleges/14', 'PUT', '127.0.0.1', '{\"name\":\"Medical College\",\"description\":\"<p>The Faculty of Medicine of Damascus University, founded in 1903, was the first university college established in Syria.<\\/p>\\r\\n\\r\\n<p>The school moved to Beirut temporarily during World War I. At one point later it was called the Arab Medical Institute.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\",\"num_of_years\":\"5\",\"universities_id\":\"9\",\"cover_font_color\":\"#150d0b\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Colleges\"}', '2018-10-17 13:15:58', '2018-10-17 13:15:58'),
(110, 1, 'admin/Colleges', 'GET', '127.0.0.1', '[]', '2018-10-17 13:15:58', '2018-10-17 13:15:58'),
(111, 1, 'admin/Colleges/14/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:16:53', '2018-10-17 13:16:53'),
(112, 1, 'admin/Colleges/14/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:16:56', '2018-10-17 13:16:56'),
(113, 1, 'admin/Colleges/14/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:17:03', '2018-10-17 13:17:03'),
(114, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:17:11', '2018-10-17 13:17:11'),
(115, 1, 'admin/Colleges/14', 'PUT', '127.0.0.1', '{\"name\":\"Medical College\",\"description\":\"<p>The Faculty of Medicine of Damascus University, founded in 1903, was the first university college established in Syria.<\\/p>\\r\\n\\r\\n<p>The school moved to Beirut temporarily during World War I. At one point later it was called the Arab Medical Institute.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\",\"num_of_years\":\"5\",\"universities_id\":\"9\",\"cover_font_color\":\"#150d0b\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\",\"_method\":\"PUT\"}', '2018-10-17 13:17:17', '2018-10-17 13:17:17'),
(116, 1, 'admin/Colleges', 'GET', '127.0.0.1', '[]', '2018-10-17 13:17:18', '2018-10-17 13:17:18'),
(117, 1, 'admin/Colleges/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:19:58', '2018-10-17 13:19:58'),
(118, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:20:15', '2018-10-17 13:20:15'),
(119, 1, 'admin/Colleges', 'POST', '127.0.0.1', '{\"name\":\"Faculty of Dentistry\",\"description\":\"<p>The University of Damascus is the largest and oldest university in Syria, located in the capital Damascus and has campuses in other Syrian cities.<\\/p>\",\"num_of_years\":\"5\",\"universities_id\":\"9\",\"cover_font_color\":\"#150d0b\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Colleges\"}', '2018-10-17 13:20:28', '2018-10-17 13:20:28'),
(120, 1, 'admin/Colleges/create', 'GET', '127.0.0.1', '[]', '2018-10-17 13:20:40', '2018-10-17 13:20:40'),
(121, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:20:44', '2018-10-17 13:20:44'),
(122, 1, 'admin/Colleges', 'POST', '127.0.0.1', '{\"name\":\"Faculty of Dentistry\",\"description\":\"<p>The University of Damascus is the largest and oldest university in Syria, located in the capital Damascus and has campuses in other Syrian cities.<\\/p>\",\"num_of_years\":\"5\",\"universities_id\":\"9\",\"cover_font_color\":\"#d8d8d8\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\"}', '2018-10-17 13:21:27', '2018-10-17 13:21:27'),
(123, 1, 'admin/Colleges', 'GET', '127.0.0.1', '[]', '2018-10-17 13:21:27', '2018-10-17 13:21:27'),
(124, 1, 'admin/Colleges/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:24:35', '2018-10-17 13:24:35'),
(125, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:28:49', '2018-10-17 13:28:49'),
(126, 1, 'admin/Colleges', 'POST', '127.0.0.1', '{\"name\":\"Faculty of Electronic and Telecommunication\",\"description\":\"<p>The Institution of Electronics and Telecommunication Engineers (IETE) is India&#39;s leading recognized professional society devoted to the advancement of science, technology, electronics, telecommunication and information technology. Founded in 1953, it serves more than 69,000 members through 59 centers\\/ sub centers primarily located in India (3 abroad) . The Institution provides leadership in scientific and technical areas of direct importance to the national development and economy.Association of Indian Universities (AIU) has recognized AMIETE. Government of India has recognised IETE as a Scientific and Industrial Research Organization (SIRO) and also notified as an educational Institution of national eminence. The objectives of IETE focus on advancing electro-technology. The IETE conducts and sponsors technical meetings, conferences, symposia, and exhibitions all over India, publishes technical journals and provides continuing education as well as career advancement opportunities to its members.<\\/p>\",\"num_of_years\":\"5\",\"universities_id\":\"9\",\"cover_font_color\":\"#d8d8d8\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Colleges\"}', '2018-10-17 13:29:08', '2018-10-17 13:29:08'),
(127, 1, 'admin/Colleges', 'GET', '127.0.0.1', '[]', '2018-10-17 13:29:08', '2018-10-17 13:29:08'),
(128, 1, 'admin/Colleges/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:29:12', '2018-10-17 13:29:12'),
(129, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"a\"}', '2018-10-17 13:29:23', '2018-10-17 13:29:23'),
(130, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"t\"}', '2018-10-17 13:29:24', '2018-10-17 13:29:24'),
(131, 1, 'admin/Colleges', 'POST', '127.0.0.1', '{\"name\":\"Information Technology Engineering\",\"description\":\"<p>Information Technology Engineering<\\/p>\",\"num_of_years\":\"5\",\"universities_id\":\"10\",\"cover_font_color\":\"#150d0b\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Colleges\"}', '2018-10-17 13:31:00', '2018-10-17 13:31:00'),
(132, 1, 'admin/Colleges', 'GET', '127.0.0.1', '[]', '2018-10-17 13:31:04', '2018-10-17 13:31:04'),
(133, 1, 'admin/Course', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:31:12', '2018-10-17 13:31:12'),
(134, 1, 'admin/Course', 'GET', '127.0.0.1', '[]', '2018-10-17 13:37:12', '2018-10-17 13:37:12'),
(135, 1, 'admin/Course/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:37:14', '2018-10-17 13:37:14'),
(136, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:37:35', '2018-10-17 13:37:35'),
(137, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 13:37:35', '2018-10-17 13:37:35'),
(138, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 13:37:36', '2018-10-17 13:37:36'),
(139, 1, 'admin/Course', 'POST', '127.0.0.1', '{\"name\":\"Programming 1\",\"professor\":\"Rakan\",\"description\":\"<p>Computer programming is the process of designing and building an executable computer program for accomplishing a specific computing task. Programming involves tasks such as analysis, generating algorithms, profiling algorithms&#39; accuracy and resource consumption, and the implementation of algorithms in a chosen programming language (commonly referred to as coding[1][2]). The source code of a program is written in one or more programming languages. The purpose of programming is to find a sequence of instructions that will automate the performance of a task for solving a given problem. The process of programming thus often requires expertise in several different subjects, including knowledge of the application domain, specialized algorithms, and formal logic.<\\/p>\",\"lectures_num\":\"10\",\"year\":\"1\",\"semester\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Course\"}', '2018-10-17 13:38:13', '2018-10-17 13:38:13'),
(140, 1, 'admin/Course', 'GET', '127.0.0.1', '[]', '2018-10-17 13:38:13', '2018-10-17 13:38:13'),
(141, 1, 'admin/Course/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:38:18', '2018-10-17 13:38:18'),
(142, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:39:35', '2018-10-17 13:39:35'),
(143, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 13:39:35', '2018-10-17 13:39:35'),
(144, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 13:39:36', '2018-10-17 13:39:36'),
(145, 1, 'admin/Course', 'POST', '127.0.0.1', '{\"name\":\"Machine Learning\",\"professor\":\"george\",\"description\":\"<p>Machine learning is an application of artificial intelligence (AI) that provides systems the ability to automatically learn and improve from experience without being explicitly programmed. Machine learning focuses on the development of computer programs that can access data and use it learn for themselves.<\\/p>\\r\\n\\r\\n<p>The process of learning begins with observations or data, such as examples, direct experience, or instruction, in order to look for patterns in data and make better decisions in the future based on the examples that we provide. The primary aim is to allow the computers learn automatically without human intervention or assistance and adjust actions accordingly.<\\/p>\",\"lectures_num\":\"8\",\"year\":\"5\",\"semester\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"12\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Course\"}', '2018-10-17 13:40:00', '2018-10-17 13:40:00'),
(146, 1, 'admin/Course', 'GET', '127.0.0.1', '[]', '2018-10-17 13:40:01', '2018-10-17 13:40:01'),
(147, 1, 'admin/Course/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:40:06', '2018-10-17 13:40:06'),
(148, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:41:13', '2018-10-17 13:41:13'),
(149, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 13:41:14', '2018-10-17 13:41:14'),
(150, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 13:41:14', '2018-10-17 13:41:14'),
(151, 1, 'admin/Course', 'POST', '127.0.0.1', '{\"name\":\"Physics\",\"professor\":\"someone\",\"description\":\"<p>Physics, science that deals with the structure of matter and the interactions between the fundamental constituents of the observable universe. In the broadest sense, physics (from the Greek physikos) is concerned with all aspects of nature on both the macroscopic and submicroscopic levels. Its scope of study encompasses not only the behaviour of objects under the action of given forces but also the nature and origin of gravitational, electromagnetic, and nuclear force fields. Its ultimate objective is the formulation of a few comprehensive principles that bring together and explain all such disparate phenomena.<\\/p>\",\"lectures_num\":\"12\",\"year\":\"1\",\"semester\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Course\"}', '2018-10-17 13:41:26', '2018-10-17 13:41:26'),
(152, 1, 'admin/Course', 'GET', '127.0.0.1', '[]', '2018-10-17 13:41:27', '2018-10-17 13:41:27'),
(153, 1, 'admin/Course/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:42:06', '2018-10-17 13:42:06'),
(154, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:43:02', '2018-10-17 13:43:02'),
(155, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 13:43:02', '2018-10-17 13:43:02'),
(156, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 13:43:03', '2018-10-17 13:43:03'),
(157, 1, 'admin/Course', 'POST', '127.0.0.1', '{\"name\":\"Algorithms 1\",\"professor\":\"Basem kosiba\",\"description\":\"<p>In mathematics and computer science, an algorithm (\\/\\u02c8&aelig;l\\u0261\\u0259r\\u026a&eth;\\u0259m\\/ (About this sound listen)) is an unambiguous specification of how to solve a class of problems. Algorithms can perform calculation, data processing and automated reasoning tasks.<\\/p>\\r\\n\\r\\n<p>As an effective method, an algorithm can be expressed within a finite amount of space and time and in a well-defined formal language for calculating a function. Starting from an initial state and initial input (perhaps empty), the instructions describe a computation that, when executed, proceeds through a finite number of well-defined successive states, eventually producing &quot;output&quot; and terminating at a final ending state. The transition from one state to the next is not necessarily deterministic; some algorithms, known as randomized algorithms, incorporate random input.<\\/p>\",\"lectures_num\":\"11\",\"year\":\"2\",\"semester\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Course\"}', '2018-10-17 13:43:12', '2018-10-17 13:43:12'),
(158, 1, 'admin/Course', 'GET', '127.0.0.1', '[]', '2018-10-17 13:43:25', '2018-10-17 13:43:25'),
(159, 1, 'admin/Course/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:43:28', '2018-10-17 13:43:28'),
(160, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:44:16', '2018-10-17 13:44:16'),
(161, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 13:44:17', '2018-10-17 13:44:17'),
(162, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 13:44:17', '2018-10-17 13:44:17'),
(163, 1, 'admin/Course', 'POST', '127.0.0.1', '{\"name\":\"Software Engineering\",\"professor\":\"someone\",\"description\":\"<p>A software engineer is a person who applies the principles of software engineering to the design, development, maintenance, testing, and evaluation of computer software.<\\/p>\\r\\n\\r\\n<p>Prior to the mid-1970s, software practitioners called themselves computer programmers or software developers, regardless of their actual jobs. Many people prefer to call themselves software developer and programmer, because most widely agree what these terms mean, while the exact meaning of software engineer is still being debated.<\\/p>\",\"lectures_num\":\"12\",\"year\":\"4\",\"semester\":\"2\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"13\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Course\"}', '2018-10-17 13:44:25', '2018-10-17 13:44:25'),
(164, 1, 'admin/Course', 'GET', '127.0.0.1', '[]', '2018-10-17 13:44:26', '2018-10-17 13:44:26'),
(165, 1, 'admin/Text_lecture', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:44:39', '2018-10-17 13:44:39'),
(166, 1, 'admin/Text_lecture/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:44:42', '2018-10-17 13:44:42'),
(167, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"c\"}', '2018-10-17 13:44:51', '2018-10-17 13:44:51'),
(168, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:44:52', '2018-10-17 13:44:52'),
(169, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 13:44:52', '2018-10-17 13:44:52'),
(170, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 13:44:52', '2018-10-17 13:44:52'),
(171, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 13:44:53', '2018-10-17 13:44:53'),
(172, 1, 'admin/Text_lecture', 'POST', '127.0.0.1', '{\"name\":\"introduction\",\"number\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"courses_id\":\"1\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Text_lecture\"}', '2018-10-17 13:44:55', '2018-10-17 13:44:55'),
(173, 1, 'admin/Text_lecture', 'GET', '127.0.0.1', '[]', '2018-10-17 13:44:55', '2018-10-17 13:44:55'),
(174, 1, 'admin/Text_lecture/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:44:57', '2018-10-17 13:44:57'),
(175, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:45:03', '2018-10-17 13:45:03'),
(176, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 13:45:04', '2018-10-17 13:45:04'),
(177, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 13:45:04', '2018-10-17 13:45:04'),
(178, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 13:45:05', '2018-10-17 13:45:05'),
(179, 1, 'admin/Text_lecture', 'POST', '127.0.0.1', '{\"name\":\"second lec\",\"number\":\"2\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"courses_id\":\"1\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Text_lecture\"}', '2018-10-17 13:45:05', '2018-10-17 13:45:05'),
(180, 1, 'admin/Text_lecture', 'GET', '127.0.0.1', '[]', '2018-10-17 13:45:06', '2018-10-17 13:45:06'),
(181, 1, 'admin/Video_lecture', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:45:11', '2018-10-17 13:45:11'),
(182, 1, 'admin/Video_lecture/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:45:13', '2018-10-17 13:45:13'),
(183, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:45:38', '2018-10-17 13:45:38'),
(184, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:45:42', '2018-10-17 13:45:42'),
(185, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:45:45', '2018-10-17 13:45:45'),
(186, 1, 'admin/Video_lecture/create', 'GET', '127.0.0.1', '[]', '2018-10-17 13:45:47', '2018-10-17 13:45:47'),
(187, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:45:53', '2018-10-17 13:45:53'),
(188, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 13:45:54', '2018-10-17 13:45:54'),
(189, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 13:45:54', '2018-10-17 13:45:54'),
(190, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 13:45:55', '2018-10-17 13:45:55'),
(191, 1, 'admin/Video_lecture', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:47:41', '2018-10-17 13:47:41'),
(192, 1, 'admin/Video_lecture/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:47:45', '2018-10-17 13:47:45'),
(193, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:48:06', '2018-10-17 13:48:06'),
(194, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"da\"}', '2018-10-17 13:48:48', '2018-10-17 13:48:48'),
(195, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 13:48:49', '2018-10-17 13:48:49'),
(196, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 13:48:49', '2018-10-17 13:48:49'),
(197, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 13:48:49', '2018-10-17 13:48:49'),
(198, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-10-17 13:53:53', '2018-10-17 13:53:53'),
(199, 1, 'admin/Video_lecture', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:53:59', '2018-10-17 13:53:59'),
(200, 1, 'admin/Video_lecture/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:54:01', '2018-10-17 13:54:01'),
(201, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 13:54:41', '2018-10-17 13:54:41'),
(202, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 13:54:42', '2018-10-17 13:54:42'),
(203, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 13:54:42', '2018-10-17 13:54:42'),
(204, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 13:54:42', '2018-10-17 13:54:42'),
(205, 1, 'admin/Video_lecture', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 13:55:15', '2018-10-17 13:55:15'),
(206, 1, 'admin/Video_lecture', 'POST', '127.0.0.1', '{\"name\":\"lec three\",\"number\":\"3\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"courses_id\":\"1\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Video_lecture\"}', '2018-10-17 14:00:46', '2018-10-17 14:00:46'),
(207, 1, 'admin/Video_lecture', 'GET', '127.0.0.1', '[]', '2018-10-17 14:00:47', '2018-10-17 14:00:47'),
(208, 1, 'admin/Video_lecture/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 14:01:30', '2018-10-17 14:01:30'),
(209, 1, 'admin/Text_lecture', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 14:01:36', '2018-10-17 14:01:36'),
(210, 1, 'admin/Text_lecture/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 14:01:39', '2018-10-17 14:01:39'),
(211, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 14:01:45', '2018-10-17 14:01:45'),
(212, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 14:01:45', '2018-10-17 14:01:45'),
(213, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 14:01:45', '2018-10-17 14:01:45'),
(214, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 14:01:46', '2018-10-17 14:01:46'),
(215, 1, 'admin/Text_lecture', 'POST', '127.0.0.1', '{\"name\":\"4\",\"number\":\"4\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"courses_id\":\"1\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Text_lecture\"}', '2018-10-17 14:01:47', '2018-10-17 14:01:47'),
(216, 1, 'admin/Text_lecture/create', 'GET', '127.0.0.1', '[]', '2018-10-17 14:01:47', '2018-10-17 14:01:47'),
(217, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 14:01:57', '2018-10-17 14:01:57'),
(218, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 14:01:58', '2018-10-17 14:01:58');
INSERT INTO `operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(219, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 14:01:58', '2018-10-17 14:01:58'),
(220, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 14:01:58', '2018-10-17 14:01:58'),
(221, 1, 'admin/Text_lecture', 'POST', '127.0.0.1', '{\"name\":\"fouth mannnnnnn\",\"number\":\"4\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"courses_id\":\"1\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\"}', '2018-10-17 14:01:59', '2018-10-17 14:01:59'),
(222, 1, 'admin/Text_lecture', 'GET', '127.0.0.1', '[]', '2018-10-17 14:01:59', '2018-10-17 14:01:59'),
(223, 1, 'admin/Video_lecture', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 14:02:03', '2018-10-17 14:02:03'),
(224, 1, 'admin/Video_lecture/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 14:02:05', '2018-10-17 14:02:05'),
(225, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 14:02:22', '2018-10-17 14:02:22'),
(226, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 14:02:30', '2018-10-17 14:02:30'),
(227, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"f\"}', '2018-10-17 14:02:34', '2018-10-17 14:02:34'),
(228, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"inf\"}', '2018-10-17 14:02:35', '2018-10-17 14:02:35'),
(229, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 14:02:46', '2018-10-17 14:02:46'),
(230, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 14:02:57', '2018-10-17 14:02:57'),
(231, 1, 'admin/Video_lecture', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 14:03:01', '2018-10-17 14:03:01'),
(232, 1, 'admin/Video_lecture/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 14:07:37', '2018-10-17 14:07:37'),
(233, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 14:07:54', '2018-10-17 14:07:54'),
(234, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 14:07:55', '2018-10-17 14:07:55'),
(235, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 14:07:55', '2018-10-17 14:07:55'),
(236, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 14:07:56', '2018-10-17 14:07:56'),
(237, 1, 'admin/Video_lecture', 'POST', '127.0.0.1', '{\"name\":\"final baby\",\"number\":\"5\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"courses_id\":\"1\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Video_lecture\"}', '2018-10-17 14:08:15', '2018-10-17 14:08:15'),
(238, 1, 'admin/Video_lecture', 'GET', '127.0.0.1', '[]', '2018-10-17 14:08:15', '2018-10-17 14:08:15'),
(239, 1, 'admin/Video_lecture/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 14:09:38', '2018-10-17 14:09:38'),
(240, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 14:09:44', '2018-10-17 14:09:44'),
(241, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 14:09:45', '2018-10-17 14:09:45'),
(242, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 14:09:45', '2018-10-17 14:09:45'),
(243, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 14:09:46', '2018-10-17 14:09:46'),
(244, 1, 'admin/Video_lecture', 'POST', '127.0.0.1', '{\"name\":\"sosos\",\"number\":\"6\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"courses_id\":\"1\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Video_lecture\"}', '2018-10-17 14:09:52', '2018-10-17 14:09:52'),
(245, 1, 'admin/Video_lecture/create', 'GET', '127.0.0.1', '[]', '2018-10-17 14:09:52', '2018-10-17 14:09:52'),
(246, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 14:10:10', '2018-10-17 14:10:10'),
(247, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 14:10:11', '2018-10-17 14:10:11'),
(248, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 14:10:11', '2018-10-17 14:10:11'),
(249, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 14:10:12', '2018-10-17 14:10:12'),
(250, 1, 'admin/Video_lecture', 'POST', '127.0.0.1', '{\"name\":\"now\",\"number\":\"6\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"courses_id\":\"1\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\"}', '2018-10-17 14:10:22', '2018-10-17 14:10:22'),
(251, 1, 'admin/Video_lecture', 'GET', '127.0.0.1', '[]', '2018-10-17 14:10:34', '2018-10-17 14:10:34'),
(252, 1, 'admin/Text_lecture', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 14:10:42', '2018-10-17 14:10:42'),
(253, 1, 'admin/Text_lecture/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 14:10:53', '2018-10-17 14:10:53'),
(254, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 14:10:59', '2018-10-17 14:10:59'),
(255, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 14:11:00', '2018-10-17 14:11:00'),
(256, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 14:11:00', '2018-10-17 14:11:00'),
(257, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 14:11:00', '2018-10-17 14:11:00'),
(258, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"13\"}', '2018-10-17 14:11:04', '2018-10-17 14:11:04'),
(259, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"15\"}', '2018-10-17 14:11:04', '2018-10-17 14:11:04'),
(260, 1, 'admin/Course', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 14:11:11', '2018-10-17 14:11:11'),
(261, 1, 'admin/Course/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 14:11:17', '2018-10-17 14:11:17'),
(262, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 14:11:28', '2018-10-17 14:11:28'),
(263, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 14:11:29', '2018-10-17 14:11:29'),
(264, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 14:11:29', '2018-10-17 14:11:29'),
(265, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 14:11:30', '2018-10-17 14:11:30'),
(266, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"13\"}', '2018-10-17 14:11:31', '2018-10-17 14:11:31'),
(267, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"15\"}', '2018-10-17 14:11:32', '2018-10-17 14:11:32'),
(268, 1, 'admin/Course', 'POST', '127.0.0.1', '{\"name\":\"civil 1\",\"professor\":\"someone\",\"description\":\"<p>dasda<\\/p>\",\"lectures_num\":\"1\",\"year\":\"1\",\"semester\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"13\",\"majors_id\":\"15\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Course\"}', '2018-10-17 14:11:41', '2018-10-17 14:11:41'),
(269, 1, 'admin/Course', 'GET', '127.0.0.1', '[]', '2018-10-17 14:11:41', '2018-10-17 14:11:41'),
(270, 1, 'admin/Text_lecture', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 14:11:45', '2018-10-17 14:11:45'),
(271, 1, 'admin/Text_lecture/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 14:11:46', '2018-10-17 14:11:46'),
(272, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 14:11:53', '2018-10-17 14:11:53'),
(273, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 14:11:54', '2018-10-17 14:11:54'),
(274, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 14:11:54', '2018-10-17 14:11:54'),
(275, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 14:11:55', '2018-10-17 14:11:55'),
(276, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"13\"}', '2018-10-17 14:11:56', '2018-10-17 14:11:56'),
(277, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"15\"}', '2018-10-17 14:11:57', '2018-10-17 14:11:57'),
(278, 1, 'admin/Text_lecture', 'POST', '127.0.0.1', '{\"name\":\"lec 1\",\"number\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"13\",\"majors_id\":\"15\",\"courses_id\":\"6\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Text_lecture\"}', '2018-10-17 14:11:59', '2018-10-17 14:11:59'),
(279, 1, 'admin/Text_lecture', 'GET', '127.0.0.1', '[]', '2018-10-17 14:11:59', '2018-10-17 14:11:59'),
(280, 1, 'admin/Video_lecture', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 14:12:05', '2018-10-17 14:12:05'),
(281, 1, 'admin/Video_lecture/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 14:12:06', '2018-10-17 14:12:06'),
(282, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 14:12:12', '2018-10-17 14:12:12'),
(283, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 14:12:13', '2018-10-17 14:12:13'),
(284, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 14:12:13', '2018-10-17 14:12:13'),
(285, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 14:12:13', '2018-10-17 14:12:13'),
(286, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"13\"}', '2018-10-17 14:12:17', '2018-10-17 14:12:17'),
(287, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"15\"}', '2018-10-17 14:12:17', '2018-10-17 14:12:17'),
(288, 1, 'admin/Video_lecture', 'POST', '127.0.0.1', '{\"name\":\"civil 2\",\"number\":\"2\",\"universities_id\":\"9\",\"colleges_id\":\"13\",\"majors_id\":\"15\",\"courses_id\":\"6\",\"_token\":\"9zdTiPaiVdGzuZZcnbaWbOLKrLPyjTEyPNS6j5np\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Video_lecture\"}', '2018-10-17 14:12:26', '2018-10-17 14:12:26'),
(289, 1, 'admin/Video_lecture', 'GET', '127.0.0.1', '[]', '2018-10-17 14:12:27', '2018-10-17 14:12:27'),
(290, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-10-17 17:26:08', '2018-10-17 17:26:08'),
(291, 1, 'admin/Text_lecture_sections', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:26:17', '2018-10-17 17:26:17'),
(292, 1, 'admin/Text_lecture_sections/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:26:19', '2018-10-17 17:26:19'),
(293, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 17:26:23', '2018-10-17 17:26:23'),
(294, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 17:26:23', '2018-10-17 17:26:23'),
(295, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 17:26:24', '2018-10-17 17:26:24'),
(296, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 17:26:24', '2018-10-17 17:26:24'),
(297, 1, 'admin/api/text_lectures', 'GET', '127.0.0.1', '{\"q\":\"1\"}', '2018-10-17 17:26:24', '2018-10-17 17:26:24'),
(298, 1, 'admin/Text_lecture_sections', 'POST', '127.0.0.1', '{\"name\":\"section 1\",\"text\":\"<p>The process of learning begins with observations or data, such as examples, direct experience, or instruction, in order to look for patterns in data and make better decisions in the future based on the examples that we provide. <strong>The primary aim is to allow the computers learn automatically<\\/strong> without human intervention or assistance and adjust actions accordingly.<\\/p>\\r\\n\\r\\n<h3>Some machine learning methods<\\/h3>\\r\\n\\r\\n<p>Machine learning algorithms are often categorized as supervised or unsupervised.<\\/p>\\r\\n\\r\\n<ul>\\r\\n\\t<li><strong>Supervised machine learning algorithms <\\/strong>can apply what has been learned in the past to new data using labeled examples to predict future events. Starting from the analysis of a known training dataset, the learning algorithm produces an inferred function to make predictions about the output values. The system is able to provide targets for any new input after sufficient training. The learning algorithm can also compare its output with the correct, intended output and find errors in order to modify the model accordingly.<\\/li>\\r\\n\\t<li>In contrast, <strong>unsupervised machine learning algorithms <\\/strong>are used when the information used to train is neither classified nor labeled. Unsupervised learning studies how systems can infer a function to describe a hidden structure from unlabeled data. The system doesn&rsquo;t figure out the right output, but it explores the data and can draw inferences from datasets to describe hidden structures from unlabeled data.<\\/li>\\r\\n\\t<li><strong>Semi-supervised machine learning algorithms<\\/strong> fall somewhere in between supervised and unsupervised learning, since they use both labeled and unlabeled data for training &ndash; typically a small amount of labeled data and a large amount of unlabeled data. The systems that use this method are able to considerably improve learning accuracy. Usually, semi-supervised learning is chosen when the acquired labeled data requires skilled and relevant resources in order to train it \\/ learn from it. Otherwise, acquiringunlabeled data generally doesn&rsquo;t require additional resources.<\\/li>\\r\\n\\t<li><strong>Reinforcement machine learning algorithms <\\/strong>is a learning method that interacts with its environment by producing actions and discovers errors or rewards. Trial and error search and delayed reward are the most relevant characteristics of reinforcement learning. This method allows machines and software agents to automatically determine the ideal behavior within a specific context in order to maximize its performance. Simple reward feedback is required for the agent to learn which action is best; this is known as the reinforcement signal.<\\/li>\\r\\n<\\/ul>\\r\\n\\r\\n<p>Machine learning enables analysis of massive quantities of data. While it generally delivers faster, more accurate results in order to identify profitable opportunities or dangerous risks, it may also require additional time and resources to train it properly. Combining machine learning with AI and cognitive technologies can make it even more effective in processing large volumes of information.<\\/p>\",\"section_number\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"courses_id\":\"1\",\"text_lecture_id\":\"1\",\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Text_lecture_sections\"}', '2018-10-17 17:29:35', '2018-10-17 17:29:35'),
(299, 1, 'admin/Text_lecture_sections', 'GET', '127.0.0.1', '[]', '2018-10-17 17:29:36', '2018-10-17 17:29:36'),
(300, 1, 'admin/Text_lecture_sections/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:29:38', '2018-10-17 17:29:38'),
(301, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 17:29:48', '2018-10-17 17:29:48'),
(302, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 17:29:48', '2018-10-17 17:29:48'),
(303, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 17:29:49', '2018-10-17 17:29:49'),
(304, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 17:29:49', '2018-10-17 17:29:49'),
(305, 1, 'admin/api/text_lectures', 'GET', '127.0.0.1', '{\"q\":\"1\"}', '2018-10-17 17:29:49', '2018-10-17 17:29:49'),
(306, 1, 'admin/Text_lecture_sections', 'POST', '127.0.0.1', '{\"name\":\"section 2\",\"text\":\"<p>The process of learning begins with observations or data, such as examples, direct experience, or instruction, in order to look for patterns in data and make better decisions in the future based on the examples that we provide. <strong>The primary aim is to allow the computers learn automatically<\\/strong> without human intervention or assistance and adjust actions accordingly.<\\/p>\\r\\n\\r\\n<h3>Some machine learning methods<\\/h3>\\r\\n\\r\\n<p>Machine learning algorithms are often categorized as supervised or unsupervised.<\\/p>\\r\\n\\r\\n<ul>\\r\\n\\t<li><strong>Supervised machine learning algorithms <\\/strong>can apply what has been learned in the past to new data using labeled examples to predict future events. Starting from the analysis of a known training dataset, the learning algorithm produces an inferred function to make predictions about the output values. The system is able to provide targets for any new input after sufficient training. The learning algorithm can also compare its output with the correct, intended output and find errors in order to modify the model accordingly.<\\/li>\\r\\n\\t<li>In contrast, <strong>unsupervised machine learning algorithms <\\/strong>are used when the information used to train is neither classified nor labeled. Unsupervised learning studies how systems can infer a function to describe a hidden structure from unlabeled data. The system doesn&rsquo;t figure out the right output, but it explores the data and can draw inferences from datasets to describe hidden structures from unlabeled data.<\\/li>\\r\\n\\t<li><strong>Semi-supervised machine learning algorithms<\\/strong> fall somewhere in between supervised and unsupervised learning, since they use both labeled and unlabeled data for training &ndash; typically a small amount of labeled data and a large amount of unlabeled data. The systems that use this method are able to considerably improve learning accuracy. Usually, semi-supervised learning is chosen when the acquired labeled data requires skilled and relevant resources in order to train it \\/ learn from it. Otherwise, acquiringunlabeled data generally doesn&rsquo;t require additional resources.<\\/li>\\r\\n\\t<li><strong>Reinforcement machine learning algorithms <\\/strong>is a learning method that interacts with its environment by producing actions and discovers errors or rewards. Trial and error search and delayed reward are the most relevant characteristics of reinforcement learning. This method allows machines and software agents to automatically determine the ideal behavior within a specific context in order to maximize its performance. Simple reward feedback is required for the agent to learn which action is best; this is known as the reinforcement signal.<\\/li>\\r\\n<\\/ul>\\r\\n\\r\\n<p>Machine learning enables analysis of massive quantities of data. While it generally delivers faster, more accurate results in order to identify profitable opportunities or dangerous risks, it may also require additional time and resources to train it properly. Combining machine learning with AI and cognitive technologies can make it even more effective in processing large volumes of information.<\\/p>\",\"section_number\":\"2\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"courses_id\":\"1\",\"text_lecture_id\":\"1\",\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Text_lecture_sections\"}', '2018-10-17 17:29:58', '2018-10-17 17:29:58'),
(307, 1, 'admin/Text_lecture_sections', 'GET', '127.0.0.1', '[]', '2018-10-17 17:30:10', '2018-10-17 17:30:10'),
(308, 1, 'admin/Text_lecture_sections/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:30:18', '2018-10-17 17:30:18'),
(309, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 17:30:25', '2018-10-17 17:30:25'),
(310, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 17:30:26', '2018-10-17 17:30:26'),
(311, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 17:30:26', '2018-10-17 17:30:26'),
(312, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 17:30:27', '2018-10-17 17:30:27'),
(313, 1, 'admin/api/text_lectures', 'GET', '127.0.0.1', '{\"q\":\"1\"}', '2018-10-17 17:30:27', '2018-10-17 17:30:27'),
(314, 1, 'admin/Text_lecture_sections', 'POST', '127.0.0.1', '{\"name\":\"section 3\",\"text\":\"<p>The process of learning begins with observations or data, such as examples, direct experience, or instruction, in order to look for patterns in data and make better decisions in the future based on the examples that we provide. <strong>The primary aim is to allow the computers learn automatically<\\/strong> without human intervention or assistance and adjust actions accordingly.<\\/p>\\r\\n\\r\\n<h3>Some machine learning methods<\\/h3>\\r\\n\\r\\n<p>Machine learning algorithms are often categorized as supervised or unsupervised.<\\/p>\\r\\n\\r\\n<ul>\\r\\n\\t<li><strong>Supervised machine learning algorithms <\\/strong>can apply what has been learned in the past to new data using labeled examples to predict future events. Starting from the analysis of a known training dataset, the learning algorithm produces an inferred function to make predictions about the output values. The system is able to provide targets for any new input after sufficient training. The learning algorithm can also compare its output with the correct, intended output and find errors in order to modify the model accordingly.<\\/li>\\r\\n\\t<li>In contrast, <strong>unsupervised machine learning algorithms <\\/strong>are used when the information used to train is neither classified nor labeled. Unsupervised learning studies how systems can infer a function to describe a hidden structure from unlabeled data. The system doesn&rsquo;t figure out the right output, but it explores the data and can draw inferences from datasets to describe hidden structures from unlabeled data.<\\/li>\\r\\n\\t<li><strong>Semi-supervised machine learning algorithms<\\/strong> fall somewhere in between supervised and unsupervised learning, since they use both labeled and unlabeled data for training &ndash; typically a small amount of labeled data and a large amount of unlabeled data. The systems that use this method are able to considerably improve learning accuracy. Usually, semi-supervised learning is chosen when the acquired labeled data requires skilled and relevant resources in order to train it \\/ learn from it. Otherwise, acquiringunlabeled data generally doesn&rsquo;t require additional resources.<\\/li>\\r\\n\\t<li><strong>Reinforcement machine learning algorithms <\\/strong>is a learning method that interacts with its environment by producing actions and discovers errors or rewards. Trial and error search and delayed reward are the most relevant characteristics of reinforcement learning. This method allows machines and software agents to automatically determine the ideal behavior within a specific context in order to maximize its performance. Simple reward feedback is required for the agent to learn which action is best; this is known as the reinforcement signal.<\\/li>\\r\\n<\\/ul>\\r\\n\\r\\n<p>Machine learning enables analysis of massive quantities of data. While it generally delivers faster, more accurate results in order to identify profitable opportunities or dangerous risks, it may also require additional time and resources to train it properly. Combining machine learning with AI and cognitive technologies can make it even more effective in processing large volumes of information.<\\/p>\",\"section_number\":\"3\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"courses_id\":\"1\",\"text_lecture_id\":\"1\",\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Text_lecture_sections\"}', '2018-10-17 17:30:28', '2018-10-17 17:30:28'),
(315, 1, 'admin/Text_lecture_sections', 'GET', '127.0.0.1', '[]', '2018-10-17 17:30:29', '2018-10-17 17:30:29'),
(316, 1, 'admin/Text_lecture_sections/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:30:34', '2018-10-17 17:30:34'),
(317, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 17:30:48', '2018-10-17 17:30:48'),
(318, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 17:30:48', '2018-10-17 17:30:48'),
(319, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 17:30:49', '2018-10-17 17:30:49'),
(320, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 17:30:49', '2018-10-17 17:30:49'),
(321, 1, 'admin/api/text_lectures', 'GET', '127.0.0.1', '{\"q\":\"1\"}', '2018-10-17 17:30:49', '2018-10-17 17:30:49'),
(322, 1, 'admin/Text_lecture_sections', 'POST', '127.0.0.1', '{\"name\":\"section 1 baby\",\"text\":\"<p>The process of learning begins with observations or data, such as examples, direct experience, or instruction, in order to look for patterns in data and make better decisions in the future based on the examples that we provide. <strong>The primary aim is to allow the computers learn automatically<\\/strong> without human intervention or assistance and adjust actions accordingly.<\\/p>\\r\\n\\r\\n<h3>Some machine learning methods<\\/h3>\\r\\n\\r\\n<p>Machine learning algorithms are often categorized as supervised or unsupervised.<\\/p>\\r\\n\\r\\n<ul>\\r\\n\\t<li><strong>Supervised machine learning algorithms <\\/strong>can apply what has been learned in the past to new data using labeled examples to predict future events. Starting from the analysis of a known training dataset, the learning algorithm produces an inferred function to make predictions about the output values. The system is able to provide targets for any new input after sufficient training. The learning algorithm can also compare its output with the correct, intended output and find errors in order to modify the model accordingly.<\\/li>\\r\\n\\t<li>In contrast, <strong>unsupervised machine learning algorithms <\\/strong>are used when the information used to train is neither classified nor labeled. Unsupervised learning studies how systems can infer a function to describe a hidden structure from unlabeled data. The system doesn&rsquo;t figure out the right output, but it explores the data and can draw inferences from datasets to describe hidden structures from unlabeled data.<\\/li>\\r\\n\\t<li><strong>Semi-supervised machine learning algorithms<\\/strong> fall somewhere in between supervised and unsupervised learning, since they use both labeled and unlabeled data for training &ndash; typically a small amount of labeled data and a large amount of unlabeled data. The systems that use this method are able to considerably improve learning accuracy. Usually, semi-supervised learning is chosen when the acquired labeled data requires skilled and relevant resources in order to train it \\/ learn from it. Otherwise, acquiringunlabeled data generally doesn&rsquo;t require additional resources.<\\/li>\\r\\n\\t<li><strong>Reinforcement machine learning algorithms <\\/strong>is a learning method that interacts with its environment by producing actions and discovers errors or rewards. Trial and error search and delayed reward are the most relevant characteristics of reinforcement learning. This method allows machines and software agents to automatically determine the ideal behavior within a specific context in order to maximize its performance. Simple reward feedback is required for the agent to learn which action is best; this is known as the reinforcement signal.<\\/li>\\r\\n<\\/ul>\\r\\n\\r\\n<p>Machine learning enables analysis of massive quantities of data. While it generally delivers faster, more accurate results in order to identify profitable opportunities or dangerous risks, it may also require additional time and resources to train it properly. Combining machine learning with AI and cognitive technologies can make it even more effective in processing large volumes of information.<\\/p>\",\"section_number\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"courses_id\":\"1\",\"text_lecture_id\":\"2\",\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Text_lecture_sections\"}', '2018-10-17 17:30:57', '2018-10-17 17:30:57'),
(323, 1, 'admin/Text_lecture_sections', 'GET', '127.0.0.1', '[]', '2018-10-17 17:31:11', '2018-10-17 17:31:11'),
(324, 1, 'admin/Course', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:31:20', '2018-10-17 17:31:20'),
(325, 1, 'admin/Text_lecture', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:31:26', '2018-10-17 17:31:26'),
(326, 1, 'admin/Video_lecture_sections', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:31:38', '2018-10-17 17:31:38'),
(327, 1, 'admin/Video_lecture_sections/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:31:40', '2018-10-17 17:31:40'),
(328, 1, 'admin/Video_lecture_sections/create', 'GET', '127.0.0.1', '[]', '2018-10-17 17:32:09', '2018-10-17 17:32:09'),
(329, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 17:32:18', '2018-10-17 17:32:18'),
(330, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 17:32:19', '2018-10-17 17:32:19'),
(331, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 17:32:19', '2018-10-17 17:32:19'),
(332, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 17:32:20', '2018-10-17 17:32:20'),
(333, 1, 'admin/api/video_lectures', 'GET', '127.0.0.1', '{\"q\":\"1\"}', '2018-10-17 17:32:20', '2018-10-17 17:32:20'),
(334, 1, 'admin/Video_lecture_sections', 'POST', '127.0.0.1', '{\"name\":\"soso\",\"section_start\":\"0\",\"section_end\":\"1.2\",\"section_number\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"courses_id\":\"1\",\"video_lecture_id\":\"1\",\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\"}', '2018-10-17 17:32:28', '2018-10-17 17:32:28'),
(335, 1, 'admin/Video_lecture_sections', 'GET', '127.0.0.1', '[]', '2018-10-17 17:32:28', '2018-10-17 17:32:28'),
(336, 1, 'admin/Video_lecture_sections/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:32:30', '2018-10-17 17:32:30'),
(337, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 17:32:32', '2018-10-17 17:32:32'),
(338, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 17:32:33', '2018-10-17 17:32:33'),
(339, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 17:32:33', '2018-10-17 17:32:33'),
(340, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 17:32:33', '2018-10-17 17:32:33'),
(341, 1, 'admin/api/video_lectures', 'GET', '127.0.0.1', '{\"q\":\"1\"}', '2018-10-17 17:32:34', '2018-10-17 17:32:34'),
(342, 1, 'admin/Video_lecture_sections', 'POST', '127.0.0.1', '{\"name\":\"sec 2\",\"section_start\":\"1.2\",\"section_end\":\"4\",\"section_number\":\"2\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"courses_id\":\"1\",\"video_lecture_id\":\"1\",\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Video_lecture_sections\"}', '2018-10-17 17:32:48', '2018-10-17 17:32:48'),
(343, 1, 'admin/Video_lecture_sections', 'GET', '127.0.0.1', '[]', '2018-10-17 17:32:49', '2018-10-17 17:32:49'),
(344, 1, 'admin/Video_lecture_sections/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:32:51', '2018-10-17 17:32:51'),
(345, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 17:33:02', '2018-10-17 17:33:02'),
(346, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 17:33:03', '2018-10-17 17:33:03'),
(347, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 17:33:03', '2018-10-17 17:33:03'),
(348, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 17:33:03', '2018-10-17 17:33:03'),
(349, 1, 'admin/api/video_lectures', 'GET', '127.0.0.1', '{\"q\":\"1\"}', '2018-10-17 17:33:04', '2018-10-17 17:33:04'),
(350, 1, 'admin/Video_lecture_sections', 'POST', '127.0.0.1', '{\"name\":\"three\",\"section_start\":\"4\",\"section_end\":\"4.2\",\"section_number\":\"3\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"courses_id\":\"1\",\"video_lecture_id\":\"1\",\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Video_lecture_sections\"}', '2018-10-17 17:33:09', '2018-10-17 17:33:09'),
(351, 1, 'admin/Video_lecture_sections', 'GET', '127.0.0.1', '[]', '2018-10-17 17:33:09', '2018-10-17 17:33:09'),
(352, 1, 'admin/Video_lecture_sections/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:33:27', '2018-10-17 17:33:27'),
(353, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 17:33:34', '2018-10-17 17:33:34'),
(354, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 17:33:34', '2018-10-17 17:33:34'),
(355, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 17:33:35', '2018-10-17 17:33:35'),
(356, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 17:33:35', '2018-10-17 17:33:35'),
(357, 1, 'admin/api/video_lectures', 'GET', '127.0.0.1', '{\"q\":\"1\"}', '2018-10-17 17:33:35', '2018-10-17 17:33:35'),
(358, 1, 'admin/Video_lecture_sections', 'POST', '127.0.0.1', '{\"name\":\"one\",\"section_start\":\"0\",\"section_end\":\"2\",\"section_number\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"courses_id\":\"1\",\"video_lecture_id\":\"2\",\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Video_lecture_sections\"}', '2018-10-17 17:33:38', '2018-10-17 17:33:38'),
(359, 1, 'admin/Video_lecture_sections', 'GET', '127.0.0.1', '[]', '2018-10-17 17:33:39', '2018-10-17 17:33:39'),
(360, 1, 'admin/Video_lecture_sections/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:33:40', '2018-10-17 17:33:40'),
(361, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 17:33:49', '2018-10-17 17:33:49'),
(362, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 17:33:50', '2018-10-17 17:33:50'),
(363, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 17:33:50', '2018-10-17 17:33:50'),
(364, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 17:33:50', '2018-10-17 17:33:50'),
(365, 1, 'admin/api/video_lectures', 'GET', '127.0.0.1', '{\"q\":\"1\"}', '2018-10-17 17:33:51', '2018-10-17 17:33:51'),
(366, 1, 'admin/Video_lecture_sections', 'POST', '127.0.0.1', '{\"name\":\"two\",\"section_start\":\"2\",\"section_end\":\"3\",\"section_number\":\"2\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"courses_id\":\"1\",\"video_lecture_id\":\"2\",\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Video_lecture_sections\"}', '2018-10-17 17:33:53', '2018-10-17 17:33:53'),
(367, 1, 'admin/Video_lecture_sections', 'GET', '127.0.0.1', '[]', '2018-10-17 17:33:53', '2018-10-17 17:33:53'),
(368, 1, 'admin/Text_lecture', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:34:09', '2018-10-17 17:34:09'),
(369, 1, 'admin/Text_lecture_sections', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:34:14', '2018-10-17 17:34:14'),
(370, 1, 'admin/Text_lecture_sections/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:34:16', '2018-10-17 17:34:16'),
(371, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 17:34:19', '2018-10-17 17:34:19'),
(372, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 17:34:19', '2018-10-17 17:34:19'),
(373, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 17:34:20', '2018-10-17 17:34:20'),
(374, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 17:34:20', '2018-10-17 17:34:20'),
(375, 1, 'admin/api/text_lectures', 'GET', '127.0.0.1', '{\"q\":\"1\"}', '2018-10-17 17:34:20', '2018-10-17 17:34:20'),
(376, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"13\"}', '2018-10-17 17:34:22', '2018-10-17 17:34:22'),
(377, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"15\"}', '2018-10-17 17:34:22', '2018-10-17 17:34:22'),
(378, 1, 'admin/api/text_lectures', 'GET', '127.0.0.1', '{\"q\":\"6\"}', '2018-10-17 17:34:22', '2018-10-17 17:34:22'),
(379, 1, 'admin/Text_lecture_sections', 'POST', '127.0.0.1', '{\"name\":\"one\",\"text\":\"<p>The process of learning begins with observations or data, such as examples, direct experience, or instruction, in order to look for patterns in data and make better decisions in the future based on the examples that we provide. <strong>The primary aim is to allow the computers learn automatically<\\/strong> without human intervention or assistance and adjust actions accordingly.<\\/p>\\r\\n\\r\\n<h3>Some machine learning methods<\\/h3>\\r\\n\\r\\n<p>Machine learning algorithms are often categorized as supervised or unsupervised.<\\/p>\\r\\n\\r\\n<ul>\\r\\n\\t<li><strong>Supervised machine learning algorithms <\\/strong>can apply what has been learned in the past to new data using labeled examples to predict future events. Starting from the analysis of a known training dataset, the learning algorithm produces an inferred function to make predictions about the output values. The system is able to provide targets for any new input after sufficient training. The learning algorithm can also compare its output with the correct, intended output and find errors in order to modify the model accordingly.<\\/li>\\r\\n\\t<li>In contrast, <strong>unsupervised machine learning algorithms <\\/strong>are used when the information used to train is neither classified nor labeled. Unsupervised learning studies how systems can infer a function to describe a hidden structure from unlabeled data. The system doesn&rsquo;t figure out the right output, but it explores the data and can draw inferences from datasets to describe hidden structures from unlabeled data.<\\/li>\\r\\n\\t<li><strong>Semi-supervised machine learning algorithms<\\/strong> fall somewhere in between supervised and unsupervised learning, since they use both labeled and unlabeled data for training &ndash; typically a small amount of labeled data and a large amount of unlabeled data. The systems that use this method are able to considerably improve learning accuracy. Usually, semi-supervised learning is chosen when the acquired labeled data requires skilled and relevant resources in order to train it \\/ learn from it. Otherwise, acquiringunlabeled data generally doesn&rsquo;t require additional resources.<\\/li>\\r\\n\\t<li><strong>Reinforcement machine learning algorithms <\\/strong>is a learning method that interacts with its environment by producing actions and discovers errors or rewards. Trial and error search and delayed reward are the most relevant characteristics of reinforcement learning. This method allows machines and software agents to automatically determine the ideal behavior within a specific context in order to maximize its performance. Simple reward feedback is required for the agent to learn which action is best; this is known as the reinforcement signal.<\\/li>\\r\\n<\\/ul>\\r\\n\\r\\n<p>Machine learning enables analysis of massive quantities of data. While it generally delivers faster, more accurate results in order to identify profitable opportunities or dangerous risks, it may also require additional time and resources to train it properly. Combining machine learning with AI and cognitive technologies can make it even more effective in processing large volumes of information.<\\/p>\",\"section_number\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"13\",\"majors_id\":\"15\",\"courses_id\":\"6\",\"text_lecture_id\":\"4\",\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Text_lecture_sections\"}', '2018-10-17 17:34:37', '2018-10-17 17:34:37'),
(380, 1, 'admin/Text_lecture_sections', 'GET', '127.0.0.1', '[]', '2018-10-17 17:34:49', '2018-10-17 17:34:49'),
(381, 1, 'admin/Text_lecture_sections/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:34:51', '2018-10-17 17:34:51'),
(382, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 17:34:59', '2018-10-17 17:34:59'),
(383, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 17:34:59', '2018-10-17 17:34:59'),
(384, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 17:35:00', '2018-10-17 17:35:00'),
(385, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 17:35:00', '2018-10-17 17:35:00'),
(386, 1, 'admin/api/text_lectures', 'GET', '127.0.0.1', '{\"q\":\"1\"}', '2018-10-17 17:35:00', '2018-10-17 17:35:00'),
(387, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"13\"}', '2018-10-17 17:35:02', '2018-10-17 17:35:02'),
(388, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"15\"}', '2018-10-17 17:35:02', '2018-10-17 17:35:02'),
(389, 1, 'admin/api/text_lectures', 'GET', '127.0.0.1', '{\"q\":\"6\"}', '2018-10-17 17:35:02', '2018-10-17 17:35:02'),
(390, 1, 'admin/Text_lecture_sections', 'POST', '127.0.0.1', '{\"name\":\"two\",\"text\":\"<p>The process of learning begins with observations or data, such as examples, direct experience, or instruction, in order to look for patterns in data and make better decisions in the future based on the examples that we provide. <strong>The primary aim is to allow the computers learn automatically<\\/strong> without human intervention or assistance and adjust actions accordingly.<\\/p>\\r\\n\\r\\n<h3>Some machine learning methods<\\/h3>\\r\\n\\r\\n<p>Machine learning algorithms are often categorized as supervised or unsupervised.<\\/p>\\r\\n\\r\\n<ul>\\r\\n\\t<li><strong>Supervised machine learning algorithms <\\/strong>can apply what has been learned in the past to new data using labeled examples to predict future events. Starting from the analysis of a known training dataset, the learning algorithm produces an inferred function to make predictions about the output values. The system is able to provide targets for any new input after sufficient training. The learning algorithm can also compare its output with the correct, intended output and find errors in order to modify the model accordingly.<\\/li>\\r\\n\\t<li>In contrast, <strong>unsupervised machine learning algorithms <\\/strong>are used when the information used to train is neither classified nor labeled. Unsupervised learning studies how systems can infer a function to describe a hidden structure from unlabeled data. The system doesn&rsquo;t figure out the right output, but it explores the data and can draw inferences from datasets to describe hidden structures from unlabeled data.<\\/li>\\r\\n\\t<li><strong>Semi-supervised machine learning algorithms<\\/strong> fall somewhere in between supervised and unsupervised learning, since they use both labeled and unlabeled data for training &ndash; typically a small amount of labeled data and a large amount of unlabeled data. The systems that use this method are able to considerably improve learning accuracy. Usually, semi-supervised learning is chosen when the acquired labeled data requires skilled and relevant resources in order to train it \\/ learn from it. Otherwise, acquiringunlabeled data generally doesn&rsquo;t require additional resources.<\\/li>\\r\\n\\t<li><strong>Reinforcement machine learning algorithms <\\/strong>is a learning method that interacts with its environment by producing actions and discovers errors or rewards. Trial and error search and delayed reward are the most relevant characteristics of reinforcement learning. This method allows machines and software agents to automatically determine the ideal behavior within a specific context in order to maximize its performance. Simple reward feedback is required for the agent to learn which action is best; this is known as the reinforcement signal.<\\/li>\\r\\n<\\/ul>\\r\\n\\r\\n<p>Machine learning enables analysis of massive quantities of data. While it generally delivers faster, more accurate results in order to identify profitable opportunities or dangerous risks, it may also require additional time and resources to train it properly. Combining machine learning with AI and cognitive technologies can make it even more effective in processing large volumes of information.<\\/p>\",\"section_number\":\"2\",\"universities_id\":\"9\",\"colleges_id\":\"13\",\"majors_id\":\"15\",\"courses_id\":\"6\",\"text_lecture_id\":\"4\",\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Text_lecture_sections\"}', '2018-10-17 17:35:04', '2018-10-17 17:35:04'),
(391, 1, 'admin/Text_lecture_sections', 'GET', '127.0.0.1', '[]', '2018-10-17 17:35:04', '2018-10-17 17:35:04'),
(392, 1, 'admin/Video_lecture', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:35:11', '2018-10-17 17:35:11'),
(393, 1, 'admin/Video_lecture_sections', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:35:16', '2018-10-17 17:35:16'),
(394, 1, 'admin/Video_lecture_sections/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:35:17', '2018-10-17 17:35:17'),
(395, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 17:35:20', '2018-10-17 17:35:20'),
(396, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 17:35:20', '2018-10-17 17:35:20'),
(397, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 17:35:20', '2018-10-17 17:35:20'),
(398, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 17:35:21', '2018-10-17 17:35:21'),
(399, 1, 'admin/api/video_lectures', 'GET', '127.0.0.1', '{\"q\":\"1\"}', '2018-10-17 17:35:21', '2018-10-17 17:35:21'),
(400, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"13\"}', '2018-10-17 17:35:23', '2018-10-17 17:35:23'),
(401, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"15\"}', '2018-10-17 17:35:23', '2018-10-17 17:35:23'),
(402, 1, 'admin/api/video_lectures', 'GET', '127.0.0.1', '{\"q\":\"6\"}', '2018-10-17 17:35:23', '2018-10-17 17:35:23'),
(403, 1, 'admin/Video_lecture_sections', 'POST', '127.0.0.1', '{\"name\":\"one\",\"section_start\":\"0\",\"section_end\":\"1\",\"section_number\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"13\",\"majors_id\":\"15\",\"courses_id\":\"6\",\"video_lecture_id\":\"4\",\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Video_lecture_sections\"}', '2018-10-17 17:35:27', '2018-10-17 17:35:27'),
(404, 1, 'admin/Video_lecture_sections', 'GET', '127.0.0.1', '[]', '2018-10-17 17:35:28', '2018-10-17 17:35:28'),
(405, 1, 'admin/Video_lecture_sections/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:35:30', '2018-10-17 17:35:30'),
(406, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 17:35:37', '2018-10-17 17:35:37'),
(407, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 17:35:38', '2018-10-17 17:35:38'),
(408, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 17:35:38', '2018-10-17 17:35:38'),
(409, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 17:35:39', '2018-10-17 17:35:39'),
(410, 1, 'admin/api/video_lectures', 'GET', '127.0.0.1', '{\"q\":\"1\"}', '2018-10-17 17:35:39', '2018-10-17 17:35:39'),
(411, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"13\"}', '2018-10-17 17:35:40', '2018-10-17 17:35:40'),
(412, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"15\"}', '2018-10-17 17:35:40', '2018-10-17 17:35:40'),
(413, 1, 'admin/api/video_lectures', 'GET', '127.0.0.1', '{\"q\":\"6\"}', '2018-10-17 17:35:41', '2018-10-17 17:35:41'),
(414, 1, 'admin/Video_lecture_sections', 'POST', '127.0.0.1', '{\"name\":\"two\",\"section_start\":\"1\",\"section_end\":\"2\",\"section_number\":\"2\",\"universities_id\":\"9\",\"colleges_id\":\"13\",\"majors_id\":\"15\",\"courses_id\":\"6\",\"video_lecture_id\":\"4\",\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/Video_lecture_sections\"}', '2018-10-17 17:35:42', '2018-10-17 17:35:42'),
(415, 1, 'admin/Video_lecture_sections', 'GET', '127.0.0.1', '[]', '2018-10-17 17:35:42', '2018-10-17 17:35:42'),
(416, 1, 'admin/users_badges', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:36:07', '2018-10-17 17:36:07'),
(417, 1, 'admin/offers', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:36:13', '2018-10-17 17:36:13'),
(418, 1, 'admin/Offers_period', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:36:15', '2018-10-17 17:36:15'),
(419, 1, 'admin/users_offers', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:36:17', '2018-10-17 17:36:17'),
(420, 1, 'admin/Offers_period', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:36:18', '2018-10-17 17:36:18'),
(421, 1, 'admin/offers', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:36:28', '2018-10-17 17:36:28'),
(422, 1, 'admin/offers/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:36:29', '2018-10-17 17:36:29');
INSERT INTO `operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(423, 1, 'admin/offers', 'POST', '127.0.0.1', '{\"name\":\"Gold\",\"description\":\"<p>The golden offer bla bla bla.. The golden offer bla bla bla.. The golden offer bla bla bla..The golden offer bla bla bla..v<\\/p>\\r\\n\\r\\n<p>The golden offer bla bla bla..The golden offer bla bla bla..<\\/p>\\r\\n\\r\\n<p>The golden offer bla bla bla..The golden offer bla bla bla..The golden offer bla bla bla..<\\/p>\\r\\n\\r\\n<p>The golden offer bla bla bla..<\\/p>\",\"mini_description\":\"The golden offer bla bla bla..\",\"universities_id\":null,\"colleges_id\":null,\"years\":[\"all\",null],\"semesters\":[\"1\",\"2\",null],\"limitations\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"13\",\"14\",null],\"periods\":[\"1\",\"2\",\"3\",null],\"price\":\"0\",\"first_color\":\"#d4c946\",\"second_color\":\"#c51f1f\",\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/offers\"}', '2018-10-17 17:37:31', '2018-10-17 17:37:31'),
(424, 1, 'admin/offers/create', 'GET', '127.0.0.1', '[]', '2018-10-17 17:37:31', '2018-10-17 17:37:31'),
(425, 1, 'admin/offers', 'POST', '127.0.0.1', '{\"name\":\"Gold\",\"description\":\"<p>The golden offer bla bla bla.. The golden offer bla bla bla.. The golden offer bla bla bla..The golden offer bla bla bla..v<\\/p>\\r\\n\\r\\n<p>The golden offer bla bla bla..The golden offer bla bla bla..<\\/p>\\r\\n\\r\\n<p>The golden offer bla bla bla..The golden offer bla bla bla..The golden offer bla bla bla..<\\/p>\\r\\n\\r\\n<p>The golden offer bla bla bla..<\\/p>\",\"mini_description\":\"The golden offer bla bla bla.. The golden offer bla bla bla.. The golden offer bla bla bla..\",\"universities_id\":null,\"colleges_id\":null,\"years\":[\"all\",null],\"semesters\":[\"1\",\"2\",null],\"limitations\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"13\",\"14\",null],\"periods\":[\"1\",\"2\",\"3\",null],\"price\":\"0\",\"first_color\":\"#d4c846\",\"second_color\":\"#c51f1f\",\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\"}', '2018-10-17 17:38:24', '2018-10-17 17:38:24'),
(426, 1, 'admin/offers', 'GET', '127.0.0.1', '[]', '2018-10-17 17:38:25', '2018-10-17 17:38:25'),
(427, 1, 'admin/offers/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:38:33', '2018-10-17 17:38:33'),
(428, 1, 'admin/offers', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:38:54', '2018-10-17 17:38:54'),
(429, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:39:01', '2018-10-17 17:39:01'),
(430, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:39:03', '2018-10-17 17:39:03'),
(431, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 17:39:13', '2018-10-17 17:39:13'),
(432, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 17:39:14', '2018-10-17 17:39:14'),
(433, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 17:39:14', '2018-10-17 17:39:14'),
(434, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 17:39:14', '2018-10-17 17:39:14'),
(435, 1, 'admin/auth/users/1', 'PUT', '127.0.0.1', '{\"username\":\"admin\",\"email\":\"admin@admin.com\",\"first_name\":\"aoso\",\"middle_name\":\"Mazen\",\"last_name\":\"Albaghdady\",\"year_of_study\":\"5\",\"countries_id\":\"1\",\"cities_id\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"mobile\":\"00963 991235652\",\"age\":\"20\",\"gender\":\"Male\",\"roles\":[\"1\",null],\"offers\":[\"1\",null],\"badges\":[null],\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/auth\\/users\"}', '2018-10-17 17:39:18', '2018-10-17 17:39:18'),
(436, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '[]', '2018-10-17 17:39:18', '2018-10-17 17:39:18'),
(437, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 17:39:46', '2018-10-17 17:39:46'),
(438, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 17:39:46', '2018-10-17 17:39:46'),
(439, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 17:39:46', '2018-10-17 17:39:46'),
(440, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"11\"}', '2018-10-17 17:39:47', '2018-10-17 17:39:47'),
(441, 1, 'admin/api/courses', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 17:39:49', '2018-10-17 17:39:49'),
(442, 1, 'admin/auth/users/1', 'PUT', '127.0.0.1', '{\"username\":\"admin\",\"email\":\"admin@admin.com\",\"first_name\":\"aoso\",\"middle_name\":\"Mazen\",\"last_name\":\"Albaghdady\",\"year_of_study\":\"5\",\"countries_id\":\"1\",\"cities_id\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"12\",\"mobile\":\"00963 991235652\",\"age\":\"20\",\"gender\":\"Male\",\"roles\":[\"1\",null],\"offers\":[\"1\",null],\"badges\":[null],\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_method\":\"PUT\"}', '2018-10-17 17:39:51', '2018-10-17 17:39:51'),
(443, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '[]', '2018-10-17 17:39:52', '2018-10-17 17:39:52'),
(444, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '[]', '2018-10-17 17:41:07', '2018-10-17 17:41:07'),
(445, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 17:41:20', '2018-10-17 17:41:20'),
(446, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 17:41:20', '2018-10-17 17:41:20'),
(447, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 17:41:21', '2018-10-17 17:41:21'),
(448, 1, 'admin/auth/users/1', 'PUT', '127.0.0.1', '{\"username\":\"admin\",\"email\":\"admin@admin.com\",\"first_name\":\"aoso\",\"middle_name\":\"Mazen\",\"last_name\":\"Albaghdady\",\"year_of_study\":\"5\",\"countries_id\":\"1\",\"cities_id\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"mobile\":\"00963 991235652\",\"birthdate_\":\"2018-10-01\",\"gender\":\"Male\",\"roles\":[\"1\",null],\"offers\":[\"1\",null],\"badges\":[null],\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_method\":\"PUT\"}', '2018-10-17 17:41:57', '2018-10-17 17:41:57'),
(449, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:41:57', '2018-10-17 17:41:57'),
(450, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:42:04', '2018-10-17 17:42:04'),
(451, 1, 'admin/api/universities', 'GET', '127.0.0.1', '{\"q\":\"d\"}', '2018-10-17 17:42:07', '2018-10-17 17:42:07'),
(452, 1, 'admin/api/colleges', 'GET', '127.0.0.1', '{\"q\":\"9\"}', '2018-10-17 17:42:08', '2018-10-17 17:42:08'),
(453, 1, 'admin/api/majors', 'GET', '127.0.0.1', '{\"q\":\"12\"}', '2018-10-17 17:42:08', '2018-10-17 17:42:08'),
(454, 1, 'admin/auth/users/2', 'PUT', '127.0.0.1', '{\"username\":\"Aoso\",\"email\":\"baghdady.usama33@gmail.com\",\"first_name\":\"oooooo\",\"middle_name\":\"dasdsad\",\"last_name\":\"asdsadsad\",\"year_of_study\":\"2\",\"countries_id\":\"1\",\"cities_id\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"mobile\":\"00963 991235652\",\"birthdate_\":\"2018-10-09\",\"gender\":\"Male\",\"roles\":[\"2\",null],\"offers\":[\"1\",null],\"badges\":[null],\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/auth\\/users\"}', '2018-10-17 17:42:27', '2018-10-17 17:42:27'),
(455, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:42:27', '2018-10-17 17:42:27'),
(456, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:42:59', '2018-10-17 17:42:59'),
(457, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:43:09', '2018-10-17 17:43:09'),
(458, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:43:43', '2018-10-17 17:43:43'),
(459, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:43:52', '2018-10-17 17:43:52'),
(460, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:44:04', '2018-10-17 17:44:04'),
(461, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:44:16', '2018-10-17 17:44:16'),
(462, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:44:20', '2018-10-17 17:44:20'),
(463, 1, 'admin/auth/users/1', 'PUT', '127.0.0.1', '{\"username\":\"admin\",\"email\":\"admin@admin.com\",\"first_name\":\"aoso\",\"middle_name\":\"Mazen\",\"last_name\":\"Albaghdady\",\"year_of_study\":\"5\",\"countries_id\":\"1\",\"cities_id\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"mobile\":\"00963 991235652\",\"birthdate_\":null,\"gender\":\"Male\",\"roles\":[\"1\",null],\"offers\":[\"1\",null],\"badges\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"12\",\"13\",\"14\",\"15\",\"16\",\"17\",null],\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/auth\\/users\"}', '2018-10-17 17:44:23', '2018-10-17 17:44:23'),
(464, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:44:24', '2018-10-17 17:44:24'),
(465, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:45:48', '2018-10-17 17:45:48'),
(466, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:45:57', '2018-10-17 17:45:57'),
(467, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:47:17', '2018-10-17 17:47:17'),
(468, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:47:30', '2018-10-17 17:47:30'),
(469, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:47:33', '2018-10-17 17:47:33'),
(470, 1, 'admin/auth/users/2', 'PUT', '127.0.0.1', '{\"username\":\"Aoso\",\"email\":\"baghdady.usama33@gmail.com\",\"first_name\":\"oooooo\",\"middle_name\":\"dasdsad\",\"last_name\":\"asdsadsad\",\"year_of_study\":\"2\",\"countries_id\":\"1\",\"cities_id\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"mobile\":\"00963 991235652\",\"birthdate_\":\"2018-10-24\",\"gender\":\"Male\",\"roles\":[\"2\",null],\"offers\":[\"1\",null],\"badges\":[\"2\",\"3\",null],\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/auth\\/users\"}', '2018-10-17 17:47:52', '2018-10-17 17:47:52'),
(471, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:47:52', '2018-10-17 17:47:52'),
(472, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:48:38', '2018-10-17 17:48:38'),
(473, 1, 'admin/auth/users/2', 'PUT', '127.0.0.1', '{\"username\":\"Aoso\",\"email\":\"baghdady.usama33@gmail.com\",\"first_name\":\"oooooo\",\"middle_name\":\"dasdsad\",\"last_name\":\"asdsadsad\",\"year_of_study\":\"2\",\"countries_id\":\"1\",\"cities_id\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"mobile\":\"00963 991235652\",\"birthdate_\":\"2018-10-03\",\"gender\":\"Male\",\"roles\":[\"2\",null],\"offers\":[\"1\",null],\"badges\":[\"2\",\"3\",null],\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/auth\\/users\"}', '2018-10-17 17:48:46', '2018-10-17 17:48:46'),
(474, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:48:46', '2018-10-17 17:48:46'),
(475, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:49:44', '2018-10-17 17:49:44'),
(476, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:49:47', '2018-10-17 17:49:47'),
(477, 1, 'admin/auth/users/2', 'PUT', '127.0.0.1', '{\"username\":\"Aoso\",\"email\":\"baghdady.usama33@gmail.com\",\"first_name\":\"oooooo\",\"middle_name\":\"dasdsad\",\"last_name\":\"asdsadsad\",\"year_of_study\":\"2\",\"countries_id\":\"1\",\"cities_id\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"mobile\":\"00963 991235652\",\"birthdate_\":\"2018-10-18 00:00:00\",\"gender\":\"Male\",\"roles\":[\"2\",null],\"offers\":[\"1\",null],\"badges\":[\"2\",\"3\",null],\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/auth\\/users\"}', '2018-10-17 17:49:54', '2018-10-17 17:49:54'),
(478, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:49:55', '2018-10-17 17:49:55'),
(479, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:50:46', '2018-10-17 17:50:46'),
(480, 1, 'admin/auth/users/2', 'PUT', '127.0.0.1', '{\"username\":\"Aoso\",\"email\":\"baghdady.usama33@gmail.com\",\"first_name\":\"oooooo\",\"middle_name\":\"dasdsad\",\"last_name\":\"asdsadsad\",\"year_of_study\":\"2\",\"countries_id\":\"1\",\"cities_id\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"mobile\":\"00963 991235652\",\"birthdate_\":\"2018-07-10\",\"gender\":\"Male\",\"roles\":[\"2\",null],\"offers\":[\"1\",null],\"badges\":[\"2\",\"3\",null],\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/auth\\/users\"}', '2018-10-17 17:50:54', '2018-10-17 17:50:54'),
(481, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:50:55', '2018-10-17 17:50:55'),
(482, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:52:15', '2018-10-17 17:52:15'),
(483, 1, 'admin/auth/users/2', 'PUT', '127.0.0.1', '{\"username\":\"Aoso\",\"email\":\"baghdady.usama33@gmail.com\",\"first_name\":\"oooooo\",\"middle_name\":\"dasdsad\",\"last_name\":\"asdsadsad\",\"year_of_study\":\"2\",\"countries_id\":\"1\",\"cities_id\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"mobile\":\"00963 991235652\",\"birthdate_\":\"2018-10-15\",\"gender\":\"Male\",\"roles\":[\"2\",null],\"offers\":[\"1\",null],\"badges\":[\"2\",\"3\",null],\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/auth\\/users\"}', '2018-10-17 17:52:20', '2018-10-17 17:52:20'),
(484, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:52:21', '2018-10-17 17:52:21'),
(485, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:52:39', '2018-10-17 17:52:39'),
(486, 1, 'admin/auth/users/2', 'PUT', '127.0.0.1', '{\"username\":\"Aoso\",\"email\":\"baghdady.usama33@gmail.com\",\"first_name\":\"oooooo\",\"middle_name\":\"dasdsad\",\"last_name\":\"asdsadsad\",\"year_of_study\":\"2\",\"countries_id\":\"1\",\"cities_id\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"mobile\":\"00963 991235652\",\"birthdate_\":\"2018-10-10\",\"gender\":\"Male\",\"roles\":[\"2\",null],\"offers\":[\"1\",null],\"badges\":[\"2\",\"3\",null],\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/auth\\/users\"}', '2018-10-17 17:52:48', '2018-10-17 17:52:48'),
(487, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:52:49', '2018-10-17 17:52:49'),
(488, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:53:32', '2018-10-17 17:53:32'),
(489, 1, 'admin/auth/users/2', 'PUT', '127.0.0.1', '{\"username\":\"Aoso\",\"email\":\"baghdady.usama33@gmail.com\",\"first_name\":\"oooooo\",\"middle_name\":\"dasdsad\",\"last_name\":\"asdsadsad\",\"year_of_study\":\"2\",\"countries_id\":\"1\",\"cities_id\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"mobile\":\"00963 991235652\",\"birthdate_\":\"2018-10-10\",\"gender\":\"Male\",\"roles\":[\"2\",null],\"offers\":[\"1\",null],\"badges\":[\"2\",\"3\",null],\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/auth\\/users\"}', '2018-10-17 17:53:37', '2018-10-17 17:53:37'),
(490, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:53:38', '2018-10-17 17:53:38'),
(491, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:54:01', '2018-10-17 17:54:01'),
(492, 1, 'admin/auth/users/2', 'PUT', '127.0.0.1', '{\"username\":\"Aoso\",\"email\":\"baghdady.usama33@gmail.com\",\"first_name\":\"oooooo\",\"middle_name\":\"dasdsad\",\"last_name\":\"asdsadsad\",\"year_of_study\":\"2\",\"countries_id\":\"1\",\"cities_id\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"mobile\":\"00963 991235652\",\"birthdate_\":\"2018-10-04\",\"gender\":\"Male\",\"roles\":[\"2\",null],\"offers\":[\"1\",null],\"badges\":[\"2\",\"3\",null],\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/auth\\/users\"}', '2018-10-17 17:54:09', '2018-10-17 17:54:09'),
(493, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:54:09', '2018-10-17 17:54:09'),
(494, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:54:55', '2018-10-17 17:54:55'),
(495, 1, 'admin/auth/users/2', 'PUT', '127.0.0.1', '{\"username\":\"Aoso\",\"email\":\"baghdady.usama33@gmail.com\",\"first_name\":\"oooooo\",\"middle_name\":\"dasdsad\",\"last_name\":\"asdsadsad\",\"year_of_study\":\"2\",\"countries_id\":\"1\",\"cities_id\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"mobile\":\"00963 991235652\",\"birthdate\":\"2018-10-11\",\"gender\":\"Male\",\"roles\":[\"2\",null],\"offers\":[\"1\",null],\"badges\":[\"2\",\"3\",null],\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/auth\\/users\"}', '2018-10-17 17:55:01', '2018-10-17 17:55:01'),
(496, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:55:01', '2018-10-17 17:55:01'),
(497, 1, 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-10-17 17:55:18', '2018-10-17 17:55:18'),
(498, 1, 'admin/auth/users/1', 'PUT', '127.0.0.1', '{\"username\":\"admin\",\"email\":\"admin@admin.com\",\"first_name\":\"aoso\",\"middle_name\":\"Mazen\",\"last_name\":\"Albaghdady\",\"year_of_study\":\"5\",\"countries_id\":\"1\",\"cities_id\":\"1\",\"universities_id\":\"9\",\"colleges_id\":\"12\",\"majors_id\":\"11\",\"mobile\":\"00963 991235652\",\"birthdate\":\"1995-11-27\",\"gender\":\"Male\",\"roles\":[\"1\",null],\"offers\":[\"1\",null],\"badges\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"12\",\"13\",\"14\",\"15\",\"16\",\"17\",null],\"_token\":\"xt8hMRRYLpT9UeRoFQJsYWDVk0ihYwFo0sbN162U\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/learning_platform.test\\/admin\\/auth\\/users\"}', '2018-10-17 17:55:52', '2018-10-17 17:55:52'),
(499, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:55:53', '2018-10-17 17:55:53'),
(500, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:56:13', '2018-10-17 17:56:13'),
(501, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2018-10-17 17:57:11', '2018-10-17 17:57:11');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`id`, `email`, `token`, `created_at`, `updated_at`) VALUES
(3, 'baghdady.usama@gmail.com', 'CUICVZ0Mjvj6QJnQYLO7VPss1wOMlyS4nD68cyAMrPs8sxzzzyWLJhCsG9V0', '2018-09-28 21:02:06', '2018-10-16 23:02:25');

-- --------------------------------------------------------

--
-- Table structure for table `periods`
--

CREATE TABLE `periods` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `periods`
--

INSERT INTO `periods` (`id`, `name`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(1, 'semester 1', '2018-09-21', '2019-02-01', '2018-09-28 10:27:59', '2018-09-28 10:32:49'),
(2, 'semester 2', '2019-02-08', '2019-07-08', '2018-09-28 10:29:05', '2018-09-28 10:33:30'),
(3, 'takmily', '2019-07-10', '2019-09-10', '2018-09-28 10:34:13', '2018-09-28 10:34:13');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `http_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `http_path` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `http_method`, `http_path`, `created_at`, `updated_at`) VALUES
(1, 'All permission', '*', '', '*', NULL, NULL),
(2, 'Dashboard', 'dashboard', 'GET', '/', NULL, NULL),
(3, 'Login', 'auth.login', '', '/auth/login\r\n/auth/logout', NULL, NULL),
(4, 'User setting', 'auth.setting', 'GET,PUT', '/auth/setting', NULL, NULL),
(5, 'Auth management', 'auth.management', '', '/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs', NULL, NULL),
(6, 'Admin helpers', 'ext.helpers', NULL, '/helpers/*', '2018-08-19 12:05:29', '2018-08-19 12:05:29'),
(7, 'Media manager', 'ext.media-manager', NULL, '/media*', '2018-08-19 12:20:43', '2018-08-19 12:20:43'),
(8, 'Api tester', 'ext.api-tester', NULL, '/api-tester*', '2018-08-20 00:23:38', '2018-08-20 00:23:38'),
(9, 'Course', 'course', '', '/Course*', '2018-08-24 21:56:53', '2018-08-24 22:40:42'),
(10, 'Text Lecture', 'Text_lecture', '', '/Text_lecture*', '2018-08-24 22:19:08', '2018-08-25 00:35:08'),
(13, 'API', 'api', '', '/api*', '2018-08-24 22:49:46', '2018-08-24 22:50:19'),
(14, 'Text Section', 'text_lecture_section', '', '/Text_lecture_section*', '2018-08-25 00:46:42', '2018-08-25 00:46:42'),
(15, 'Video Lecture', 'video_lecture', '', '/Video_lecture*', '2018-08-25 00:47:08', '2018-08-25 00:47:08'),
(16, 'Video Section', 'video_section', '', '/Video_lecture_section', '2018-08-25 00:47:36', '2018-08-25 00:47:36');

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `reports_msg_id` int(11) NOT NULL,
  `url` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reports_msg`
--

CREATE TABLE `reports_msg` (
  `id` int(11) NOT NULL,
  `type` enum('user','question','answer','summary','bookmark') COLLATE utf8_unicode_ci NOT NULL,
  `msg` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reports_msg`
--

INSERT INTO `reports_msg` (`id`, `type`, `msg`, `created_at`, `updated_at`) VALUES
(1, 'user', 'Inappropriate profile Picture', '2018-09-22 00:17:02', '2018-09-22 00:17:02'),
(2, 'question', 'Inappropriate question', '2018-09-22 00:17:02', '2018-09-22 00:17:02'),
(3, 'answer', 'Wrong Answer', '2018-09-22 00:17:02', '2018-09-22 00:17:02'),
(4, 'summary', 'Inappropriate summary', '2018-09-22 00:17:02', '2018-09-22 00:17:02'),
(5, 'bookmark', 'Inappropriate bookmark', '2018-09-22 00:17:02', '2018-09-22 00:17:02'),
(7, 'question', 'Stupid question', '2018-09-21 21:21:54', '2018-09-21 21:21:54');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'administrator', '2018-08-19 11:49:17', '2018-08-19 11:49:17'),
(2, 'User', 'User', '2018-08-19 11:58:31', '2018-08-19 11:58:31'),
(3, 'Editor', 'Editor', '2018-08-24 21:55:16', '2018-08-24 21:55:16');

-- --------------------------------------------------------

--
-- Table structure for table `role_menu`
--

CREATE TABLE `role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_menu`
--

INSERT INTO `role_menu` (`role_id`, `menu_id`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, NULL),
(1, 17, NULL, NULL),
(1, 18, NULL, NULL),
(1, 19, NULL, NULL),
(1, 20, NULL, NULL),
(1, 21, NULL, NULL),
(1, 22, NULL, NULL),
(1, 23, NULL, NULL),
(1, 24, NULL, NULL),
(1, 25, NULL, NULL),
(1, 26, NULL, NULL),
(1, 27, NULL, NULL),
(1, 28, NULL, NULL),
(1, 29, NULL, NULL),
(1, 30, NULL, NULL),
(1, 31, NULL, NULL),
(1, 32, NULL, NULL),
(1, 33, NULL, NULL),
(1, 34, NULL, NULL),
(1, 35, NULL, NULL),
(1, 36, NULL, NULL),
(1, 37, NULL, NULL),
(1, 38, NULL, NULL),
(1, 39, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 3, NULL, NULL),
(3, 3, NULL, NULL),
(3, 9, NULL, NULL),
(3, 10, NULL, NULL),
(3, 13, NULL, NULL),
(3, 14, NULL, NULL),
(3, 15, NULL, NULL),
(3, 16, NULL, NULL),
(4, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 2, NULL, NULL),
(1, 7, NULL, NULL),
(2, 7, NULL, NULL),
(2, 8, NULL, NULL),
(2, 9, NULL, NULL),
(3, 10, NULL, NULL),
(3, 11, NULL, NULL),
(2, 25, NULL, NULL),
(2, 3, NULL, NULL),
(2, 4, NULL, NULL),
(2, 4, NULL, NULL),
(2, 43, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `static_images`
--

CREATE TABLE `static_images` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `picture` text COLLATE utf8_unicode_ci NOT NULL,
  `font_color` enum('#d8d8d8','#150d0b') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `static_images`
--

INSERT INTO `static_images` (`id`, `name`, `picture`, `font_color`, `created_at`, `updated_at`) VALUES
(1, 'Big header', 'images/StaticImages/f534d6931e9ead8ce7ecb15619867e63.png', '#d8d8d8', '2018-10-04 11:36:55', '2018-10-04 11:49:26'),
(2, 'Main page subscribe', 'images/StaticImages/c2d5e841bd67b9e858178a4910dc6429.jpg', '#d8d8d8', '2018-10-04 11:45:44', '2018-10-04 11:49:13'),
(3, 'Small header', 'images/StaticImages/03794d8b9cef80d477d852b0b8999722.png', '#d8d8d8', '2018-10-04 11:49:03', '2018-10-04 11:49:03');

-- --------------------------------------------------------

--
-- Table structure for table `text_lectures`
--

CREATE TABLE `text_lectures` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `universities_id` int(11) NOT NULL,
  `colleges_id` int(11) NOT NULL,
  `majors_id` int(11) NOT NULL,
  `courses_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `text_lectures`
--

INSERT INTO `text_lectures` (`id`, `name`, `number`, `universities_id`, `colleges_id`, `majors_id`, `courses_id`, `created_at`, `updated_at`) VALUES
(1, 'introduction', 1, 9, 12, 11, 1, '2018-10-17 13:44:55', '2018-10-17 13:44:55'),
(2, 'second lec', 2, 9, 12, 11, 1, '2018-10-17 13:45:06', '2018-10-17 13:45:06'),
(3, 'fouth mannnnnnn', 4, 9, 12, 11, 1, '2018-10-17 14:01:59', '2018-10-17 14:01:59'),
(4, 'lec 1', 1, 9, 13, 15, 6, '2018-10-17 14:11:59', '2018-10-17 14:11:59');

-- --------------------------------------------------------

--
-- Table structure for table `text_sections`
--

CREATE TABLE `text_sections` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `section_number` int(11) DEFAULT NULL,
  `universities_id` int(11) NOT NULL,
  `colleges_id` int(11) NOT NULL,
  `majors_id` int(11) NOT NULL,
  `courses_id` int(11) NOT NULL,
  `text_lecture_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `text_sections`
--

INSERT INTO `text_sections` (`id`, `name`, `picture`, `text`, `section_number`, `universities_id`, `colleges_id`, `majors_id`, `courses_id`, `text_lecture_id`, `created_at`, `updated_at`) VALUES
(1, 'section 1', 'images/lecture/fd59337824f2906dcf1f72036833b633.jpg', '<p>The process of learning begins with observations or data, such as examples, direct experience, or instruction, in order to look for patterns in data and make better decisions in the future based on the examples that we provide. <strong>The primary aim ', 1, 9, 12, 11, 1, 1, '2018-10-17 17:29:36', '2018-10-17 17:29:36'),
(2, 'section 2', 'images/lecture/c917aa352952709c794f5216bc1b520b.jpg', '<p>The process of learning begins with observations or data, such as examples, direct experience, or instruction, in order to look for patterns in data and make better decisions in the future based on the examples that we provide. <strong>The primary aim ', 2, 9, 12, 11, 1, 1, '2018-10-17 17:30:10', '2018-10-17 17:30:10'),
(3, 'section 3', NULL, '<p>The process of learning begins with observations or data, such as examples, direct experience, or instruction, in order to look for patterns in data and make better decisions in the future based on the examples that we provide. <strong>The primary aim ', 3, 9, 12, 11, 1, 1, '2018-10-17 17:30:28', '2018-10-17 17:30:28'),
(4, 'section 1 baby', 'images/lecture/eed5661c018f4d537246c413e4bfd4e6.png', '<p>The process of learning begins with observations or data, such as examples, direct experience, or instruction, in order to look for patterns in data and make better decisions in the future based on the examples that we provide. <strong>The primary aim ', 1, 9, 12, 11, 1, 2, '2018-10-17 17:31:10', '2018-10-17 17:31:10'),
(5, 'one', 'images/lecture/03e14ef8cacfced1bd21bc56a368979b.jpg', '<p>The process of learning begins with observations or data, such as examples, direct experience, or instruction, in order to look for patterns in data and make better decisions in the future based on the examples that we provide. <strong>The primary aim ', 1, 9, 13, 15, 6, 4, '2018-10-17 17:34:42', '2018-10-17 17:34:42'),
(6, 'two', NULL, '<p>The process of learning begins with observations or data, such as examples, direct experience, or instruction, in order to look for patterns in data and make better decisions in the future based on the examples that we provide. <strong>The primary aim ', 2, 9, 13, 15, 6, 4, '2018-10-17 17:35:04', '2018-10-17 17:35:04');

-- --------------------------------------------------------

--
-- Table structure for table `universities`
--

CREATE TABLE `universities` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'university_picture.png',
  `cover_picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'university_cover.png',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `universities`
--

INSERT INTO `universities` (`id`, `name`, `description`, `picture`, `cover_picture`, `created_at`, `updated_at`) VALUES
(9, 'Damascus University', '<p>The University of Damascus is the largest and oldest university in Syria, located in the capital Damascus and has campuses in other Syrian cities. It was founded in 1923 through the merger of the School of Medicine (established 1903) and the Institute of Law (established 1913). Until 1958 it was named the Syrian University, but the name changed after the founding of the University of Aleppo. There are nine public universities and more than ten private ones in Syria. Damascus university was one of the most reputable universities in the Middle East before the war in Syria started in 2011</p>', 'images/universities/ee2758a000c420a17ed5901bdcb5e67d.jpg', 'university_cover.png', '2018-10-17 00:58:32', '2018-10-17 00:58:32'),
(10, 'Aleppo University', '<p>University of Aleppo is a public university located in Aleppo, Syria. It is the second largest university in Syria after the University of Damascus.</p>\r\n\r\n<p>During 2005-2006 the University had over 61,000 undergraduate students, over 1,500 postgraduate students and approximately 2,400 faculty members. The university has 25 faculties and 10 intermediate colleges.</p>', 'images/universities/d1c6959482205c65d3805f28d0999257.jpg', 'university_cover.png', '2018-10-17 01:00:44', '2018-10-17 01:00:44'),
(11, 'Tishreen University', '<p>Tishreen University, is a public university located in Latakia, Syria. It is the third largest university in Syria. It was established on 20 May 1971 at the University of Latakia before the name was changed to commemorate October War.</p>\r\n\r\n<p>In the beginning, the university only had 3 faculties, Arabic literature, science, and agriculture and an enrollment of 983 students during the 1970s.However that number largely grew throughout the years to reach more than 70,000 students, making Tishreen University the third largest in Syria, with the number of its faculties rising from 3 to 21; including Medicine, Pharmacy, Dentistry, Science, Nursing, Education, Agriculture, Law, History, Electrical and Technical Engineering and Arts, among others</p>', 'images/universities/b1b6684014b60b7a4443dcb5ce40c891.jpg', 'university_cover.png', '2018-10-17 01:02:52', '2018-10-17 01:02:52'),
(12, 'Al-Baath University', '<p>Al-Baath University, founded in 1979, is a public university located in the city of Homs, Syria, 180 km north of Damascus. It is Syria&#39;s fourth-largest university.</p>\r\n\r\n<p>The university was established in 1979, it was established by Presidential Decree No. 44 issued by Hafez al-Assad.[citation needed]</p>\r\n\r\n<p>Al-Baath University has 22 faculties, 5 intermediate institutes, 40,000 regular students, 20,000 students in open learning, 1310 high studies students and 622 faculty members. The library contains some 63,000 volumes (as of 2011).</p>', 'images/universities/73937114d9884be6c6c26c2eeb362f29.jpg', 'university_cover.png', '2018-10-17 01:07:08', '2018-10-17 01:09:40'),
(13, 'Syrian Virtual University', '<p>The Syrian Virtual University is a Syrian educational institution established by the Syrian Ministry of Higher Education. It provides virtual education (using the Internet) to students from around the world. It was established on 2 September 2002 and is the first virtual education institution in the region, and as of 2006, remains the only one. The goals of the SVU include offering education to those who want to learn but cannot afford to do so by going to a &quot;brick and mortar&quot; university. It is headquartered at the Ministry of Higher Education building, Damascus.</p>', 'images/universities/df652ea4018f2d342ac534d5d40252d0.jpg', 'university_cover.png', '2018-10-17 01:08:47', '2018-10-17 01:08:47');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` text COLLATE utf8_unicode_ci,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'images/Users/user.png',
  `first_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middle_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` enum('Male','Female') COLLATE utf8_unicode_ci DEFAULT NULL,
  `points` int(11) NOT NULL DEFAULT '50',
  `countries_id` int(11) DEFAULT NULL,
  `cities_id` int(11) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `year_of_study` enum('1','2','3','4','5','6') COLLATE utf8_unicode_ci DEFAULT NULL,
  `universities_id` int(11) DEFAULT NULL,
  `colleges_id` int(11) DEFAULT NULL,
  `majors_id` int(11) DEFAULT NULL,
  `verified` tinyint(4) NOT NULL DEFAULT '0',
  `email_token` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `activation_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `remember_token`, `picture`, `first_name`, `middle_name`, `last_name`, `mobile`, `gender`, `points`, `countries_id`, `cities_id`, `birthdate`, `year_of_study`, `universities_id`, `colleges_id`, `majors_id`, `verified`, `email_token`, `created_at`, `updated_at`, `activation_token`, `deleted_at`) VALUES
(1, 'admin', 'admin@admin.com', '$2y$10$1dpIyJyAxhFp5RMvENQgEe71sOAHtJlA8fjsZX/Mha6ikFpjLYKIG', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjY2MGU5Mjc4NTI3NmVlNDQyMTM1YTVhNjg2ODAxNTI0YjJlNmE5OWE2NTA5MmU4YjA2YzU5MmJhZTY0NDEyZWI5NzY4MGFkOGNlMDAyMjQwIn0.eyJhdWQiOiIxIiwianRpIjoiNjYwZTkyNzg1Mjc2ZWU0NDIxMzVhNWE2ODY4MDE1MjRiMmU2YTk5YTY1MDkyZThiMDZjNTkyYmFlNjQ0MTJlYjk3NjgwYWQ4Y2UwMDIyNDAiLCJpYXQiOjE1Mzk2NDIwODcsIm5iZiI6MTUzOTY0MjA4NywiZXhwIjoxNTcxMTc4MDg3LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.OI8T7XzO1Gv-0K0kTkocO-pXWnpiP73PHwGZHiZ-_eFTHO3bM9y5j2C5kFuClUQxCdMxy9gwZ59RkB9k87rQw_kwIqEzylNkwLF_GvlpvjpAfA6S5ro34lK03RJ36sg4dc0SIOErvlD--XsshUklwtckawL4m6U2GQKAy8PNT_EZcaL5_LG41AeueY0VAQUL50jaPuDg1YIn7BhEexzuy6rAr7pxXiEl2L85rc3Qhd35T7B8sK9EAxPAkzI6F65I_lsB7RqGZ7rz3fRLizkrPzh-SbbLiX7NYPhTli_XdqRjvck923Oqn9MKcjnQzSDgOtr_6fClTS-iypK_t5dzTuJAid6loNZ5IBYxeWlJNWf6aoOxF6TBRbvfLM5UVabmG5JFkwhNCUYr0bVGdfU63bjtAMNJ9h8n4nW3s5SBLnEBv9csAv3y0-VockDglTTR-YTyfDO3sIwv0oubnPpJLB18yB69tb1qs_tBmVr6ln_KrXNBxHH5AgC2JnNA4Ww6vxyZFxD-uDj08QBtBC-iFx-w8FT7UG59-UHkLDl_YuMKFTMTJYifFnsk8hG1s3C_CwCeU4ZtlsUnIVQhSYdoGYfMRS70hkCXyP00-qEmeXJ-2F-ToNNREyiQutIlsnGOQ0Sffi3oofgsoQAN5WRF5RZimqJs1hPlQzpk_AS8rrw', 'images/Users/b6e05557fdf7ae1fe1134f9462eb52c0.jpg', 'aoso', 'Mazen', 'Albaghdady', '00963 991235652', 'Male', 1000000, 1, 1, '1995-11-27', '5', 9, 12, 11, 1, NULL, '2018-07-31 21:00:00', '2018-10-17 17:55:52', '', NULL),
(2, 'Aoso', 'baghdady.usama33@gmail.com', '$2y$10$YdPz/GTNgz2WSfzHfp4XAuzenbEnsRmG48VBbqX1RQbK0BTHaiHza', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjUxNGQxNDgzMWNhYmVmMTZiNTk0NmVlNGE5ZDg1OGRjZGEyZjA0ZGI2MzdiY2Y0ODcxNjc2Y2ZlZmYzOGVmNzI1ZTIxNzg2MzM5MzI4YTZmIn0.eyJhdWQiOiIxIiwianRpIjoiNTE0ZDE0ODMxY2FiZWYxNmI1OTQ2ZWU0YTlkODU4ZGNkYTJmMDRkYjYzN2JjZjQ4NzE2NzZjZmVmZjM4ZWY3MjVlMjE3ODYzMzkzMjhhNmYiLCJpYXQiOjE1Mzk3Mjg5ODksIm5iZiI6MTUzOTcyODk4OSwiZXhwIjoxNTcxMjY0OTg5LCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.nBFQhCpE8Cka1kzQihaPeOW2HtrbqMTgeGmzmzEalj2hgAnApT-Fdy8XkNRyr01QwMAVRxCealvyLGlkrxmbexQTjOcbz9gT7x7uooZhWaH3CjM-RBrOa5af7hV54AqS8L1-M0LLd7k_JAoyQBs7_byOiNVb51Kz81SCreNlPw9D2Ghb8OFkClL-qJ2ZKcG9dcDkBErYUGkc89sTYmFg3SpZE7gmlpwMo3vKH7W8v2DOG9wXq1Iv4g8hctK0qGPCbT3o-kC4wnGwgOaHDzJZ-aSUPLPESzRdNF5H5auDzu2L4YJonDXvWhHy0ysBDfbGK1C_ytGhhK8BHbvDEMf0UW99X4rC24s-kHhufs8mqsfSdXZ5tqRyAfegse6lgqS_PdMWXGIbWMa_hoWqKA5Q8kWbIuulpdPFGMFy17gFbD7O8_x6IA-ZJ40plmC8YGhO3x-pOnj7l-svXxGFV0zHK0jzl5P33XxuSMUQQWk7pntfySSYRonqd2F0OdsUzceG7Roqono6S1EziefUgUNBM53FBZzcYkk0zbxalIBcPJ16BuRJqOyS7fMsU4eEiBeNMflN3__rsZApINzLRSboGmdO_n4vhYvHauOOJJRYA5JDQah6_X-RX2_AuitgDsxAQe46OmtJBq893AjZOEMaOCXYxzvTuUFJpqRrM8HXCro', 'images/Users/7c5edbfb2190722fcb875fb54268293b.jpg', 'oooooo', 'dasdsad', 'asdsadsad', '00963 991235652', 'Male', 4941, 1, 1, '2018-10-11', '2', 9, 12, 11, 1, NULL, '2018-08-12 21:00:00', '2018-10-17 17:55:01', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_permissions`
--

CREATE TABLE `user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_permissions`
--

INSERT INTO `user_permissions` (`user_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(2, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_points`
--

CREATE TABLE `user_points` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `date` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_progress`
--

CREATE TABLE `user_progress` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `lecture_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lecture_type` enum('text','video') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_visits`
--

CREATE TABLE `user_visits` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `date` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `video_lectures`
--

CREATE TABLE `video_lectures` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` text COLLATE utf8_unicode_ci,
  `number` int(11) DEFAULT NULL,
  `universities_id` int(11) NOT NULL,
  `colleges_id` int(11) NOT NULL,
  `majors_id` int(11) NOT NULL,
  `courses_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `video_lectures`
--

INSERT INTO `video_lectures` (`id`, `name`, `video`, `number`, `universities_id`, `colleges_id`, `majors_id`, `courses_id`, `created_at`, `updated_at`) VALUES
(1, 'lec three', 'lectures/videos/7da01354fac0088c2d2740225aaf3cdd.mp4', 3, 9, 12, 11, 1, '2018-10-17 14:00:46', '2018-10-17 14:00:46'),
(2, 'final baby', 'lectures/videos/5dd6949c5f67964e0916a5969165baac.MKV', 5, 9, 12, 11, 1, '2018-10-17 14:08:15', '2018-10-17 14:08:15'),
(3, 'now', 'lectures/videos/d80cf01f7cdf72b6f3dff8bb92a58b84.MKV', 6, 9, 12, 11, 1, '2018-10-17 14:10:33', '2018-10-17 14:10:33'),
(4, 'civil 2', 'lectures/videos/fcdd4d336b5b8b2a7d2cef1ede65f900.mp4', 2, 9, 13, 15, 6, '2018-10-17 14:12:27', '2018-10-17 14:12:27');

-- --------------------------------------------------------

--
-- Table structure for table `video_sections`
--

CREATE TABLE `video_sections` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `section_start` float DEFAULT NULL,
  `section_end` float DEFAULT NULL,
  `section_number` int(11) DEFAULT NULL,
  `universities_id` int(11) NOT NULL,
  `colleges_id` int(11) NOT NULL,
  `majors_id` int(11) NOT NULL,
  `courses_id` int(11) NOT NULL,
  `video_lecture_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `video_sections`
--

INSERT INTO `video_sections` (`id`, `name`, `section_start`, `section_end`, `section_number`, `universities_id`, `colleges_id`, `majors_id`, `courses_id`, `video_lecture_id`, `created_at`, `updated_at`) VALUES
(1, 'soso', 0, 1.2, 1, 9, 12, 11, 1, 1, '2018-10-17 17:32:28', '2018-10-17 17:32:28'),
(2, 'sec 2', 1.2, 4, 2, 9, 12, 11, 1, 1, '2018-10-17 17:32:48', '2018-10-17 17:32:48'),
(3, 'three', 4, 4.2, 3, 9, 12, 11, 1, 1, '2018-10-17 17:33:09', '2018-10-17 17:33:09'),
(4, 'one', 0, 2, 1, 9, 12, 11, 1, 2, '2018-10-17 17:33:38', '2018-10-17 17:33:38'),
(5, 'two', 2, 3, 2, 9, 12, 11, 1, 2, '2018-10-17 17:33:53', '2018-10-17 17:33:53'),
(6, 'one', 0, 1, 1, 9, 13, 15, 6, 4, '2018-10-17 17:35:28', '2018-10-17 17:35:28'),
(7, 'two', 1, 2, 2, 9, 13, 15, 6, 4, '2018-10-17 17:35:42', '2018-10-17 17:35:42');

-- --------------------------------------------------------

--
-- Table structure for table `vote_text_answers`
--

CREATE TABLE `vote_text_answers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lecture_text_answers_id` int(11) NOT NULL,
  `vote_type` enum('up','down') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vote_text_bookmarks`
--

CREATE TABLE `vote_text_bookmarks` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lecture_text_bookmarks_id` int(11) NOT NULL,
  `vote_type` enum('up','down') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vote_text_questions`
--

CREATE TABLE `vote_text_questions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lecture_text_questions_id` int(11) NOT NULL,
  `vote_type` enum('up','down') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vote_text_summaries`
--

CREATE TABLE `vote_text_summaries` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lecture_text_summaries_id` int(11) NOT NULL,
  `vote_type` enum('up','down') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vote_video_answers`
--

CREATE TABLE `vote_video_answers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lecture_video_answers_id` int(11) NOT NULL,
  `vote_type` enum('up','down') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vote_video_bookmarks`
--

CREATE TABLE `vote_video_bookmarks` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lecture_video_bookmarks_id` int(11) NOT NULL,
  `vote_type` enum('up','down') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vote_video_questions`
--

CREATE TABLE `vote_video_questions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lecture_video_questions_id` int(11) NOT NULL,
  `vote_type` enum('up','down') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vote_video_summaries`
--

CREATE TABLE `vote_video_summaries` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lecture_video_summaries_id` int(11) NOT NULL,
  `vote_type` enum('up','down') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `badges`
--
ALTER TABLE `badges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `badge_user`
--
ALTER TABLE `badge_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colleges`
--
ALTER TABLE `colleges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `university_id` (`universities_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `facility_id` (`colleges_id`);

--
-- Indexes for table `courses_rating`
--
ALTER TABLE `courses_rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `lecture_text_answers`
--
ALTER TABLE `lecture_text_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `text_question_id` (`lecture_text_questions_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `lecture_text_bookmarks`
--
ALTER TABLE `lecture_text_bookmarks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `text_section_id` (`text_section_id`);

--
-- Indexes for table `lecture_text_questions`
--
ALTER TABLE `lecture_text_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `text_section_id` (`text_sections_id`);

--
-- Indexes for table `lecture_text_summaries`
--
ALTER TABLE `lecture_text_summaries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `text_lecture_id` (`text_lecture_id`);

--
-- Indexes for table `lecture_video_answers`
--
ALTER TABLE `lecture_video_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lecture_video_bookmarks`
--
ALTER TABLE `lecture_video_bookmarks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lecture_video_questions`
--
ALTER TABLE `lecture_video_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lecture_video_summaries`
--
ALTER TABLE `lecture_video_summaries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `limitations`
--
ALTER TABLE `limitations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `limitations_offers`
--
ALTER TABLE `limitations_offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `majors`
--
ALTER TABLE `majors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `creator_user_id` (`user_id`);

--
-- Indexes for table `notification_type`
--
ALTER TABLE `notification_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_users`
--
ALTER TABLE `notification_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notification_id` (`notifications_id`),
  ADD KEY `recipient_user_id` (`user_id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers_period`
--
ALTER TABLE `offers_period`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers_user`
--
ALTER TABLE `offers_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `operation_log`
--
ALTER TABLE `operation_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_operation_log_user_id_index` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `periods`
--
ALTER TABLE `periods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_permissions_name_unique` (`name`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports_msg`
--
ALTER TABLE `reports_msg`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_roles_name_unique` (`name`);

--
-- Indexes for table `role_menu`
--
ALTER TABLE `role_menu`
  ADD KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`);

--
-- Indexes for table `static_images`
--
ALTER TABLE `static_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `text_lectures`
--
ALTER TABLE `text_lectures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `text_sections`
--
ALTER TABLE `text_sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `text_lecture_id` (`text_lecture_id`);

--
-- Indexes for table `universities`
--
ALTER TABLE `universities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_permissions`
--
ALTER TABLE `user_permissions`
  ADD KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`);

--
-- Indexes for table `user_points`
--
ALTER TABLE `user_points`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_progress`
--
ALTER TABLE `user_progress`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_visits`
--
ALTER TABLE `user_visits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video_lectures`
--
ALTER TABLE `video_lectures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_id` (`courses_id`);

--
-- Indexes for table `video_sections`
--
ALTER TABLE `video_sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `video_lecture_id` (`video_lecture_id`);

--
-- Indexes for table `vote_text_answers`
--
ALTER TABLE `vote_text_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vote_text_bookmarks`
--
ALTER TABLE `vote_text_bookmarks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vote_text_questions`
--
ALTER TABLE `vote_text_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vote_text_summaries`
--
ALTER TABLE `vote_text_summaries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vote_video_answers`
--
ALTER TABLE `vote_video_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vote_video_bookmarks`
--
ALTER TABLE `vote_video_bookmarks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vote_video_questions`
--
ALTER TABLE `vote_video_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vote_video_summaries`
--
ALTER TABLE `vote_video_summaries`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `badges`
--
ALTER TABLE `badges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `badge_user`
--
ALTER TABLE `badge_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `colleges`
--
ALTER TABLE `colleges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `courses_rating`
--
ALTER TABLE `courses_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lecture_text_answers`
--
ALTER TABLE `lecture_text_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lecture_text_bookmarks`
--
ALTER TABLE `lecture_text_bookmarks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lecture_text_questions`
--
ALTER TABLE `lecture_text_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lecture_text_summaries`
--
ALTER TABLE `lecture_text_summaries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lecture_video_answers`
--
ALTER TABLE `lecture_video_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lecture_video_bookmarks`
--
ALTER TABLE `lecture_video_bookmarks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lecture_video_questions`
--
ALTER TABLE `lecture_video_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lecture_video_summaries`
--
ALTER TABLE `lecture_video_summaries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `limitations`
--
ALTER TABLE `limitations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `limitations_offers`
--
ALTER TABLE `limitations_offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `majors`
--
ALTER TABLE `majors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification_type`
--
ALTER TABLE `notification_type`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `notification_users`
--
ALTER TABLE `notification_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=306;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `offers_period`
--
ALTER TABLE `offers_period`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `offers_user`
--
ALTER TABLE `offers_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `operation_log`
--
ALTER TABLE `operation_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=502;

--
-- AUTO_INCREMENT for table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `periods`
--
ALTER TABLE `periods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reports_msg`
--
ALTER TABLE `reports_msg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `static_images`
--
ALTER TABLE `static_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `text_lectures`
--
ALTER TABLE `text_lectures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `text_sections`
--
ALTER TABLE `text_sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `universities`
--
ALTER TABLE `universities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_points`
--
ALTER TABLE `user_points`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_progress`
--
ALTER TABLE `user_progress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_visits`
--
ALTER TABLE `user_visits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `video_lectures`
--
ALTER TABLE `video_lectures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `video_sections`
--
ALTER TABLE `video_sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `vote_text_answers`
--
ALTER TABLE `vote_text_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vote_text_bookmarks`
--
ALTER TABLE `vote_text_bookmarks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vote_text_questions`
--
ALTER TABLE `vote_text_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vote_text_summaries`
--
ALTER TABLE `vote_text_summaries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vote_video_answers`
--
ALTER TABLE `vote_video_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vote_video_bookmarks`
--
ALTER TABLE `vote_video_bookmarks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vote_video_questions`
--
ALTER TABLE `vote_video_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vote_video_summaries`
--
ALTER TABLE `vote_video_summaries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
