<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;


class QuestionAndAnswers implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $question_type;
    public $question_id;
    public $data;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($question_type,$question_id,$data)
    {
        $this->question_type = $question_type;
        $this->question_id = $question_id;
        $this->data = $data;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('QuestionAndAnswers-channel.'.$this->question_type.'.'.$this->question_id);
    }
}
