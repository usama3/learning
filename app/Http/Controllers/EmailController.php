<?php

namespace App\Http\Controllers;

use App\Mail\ReportMail;
use App\Models\Colleges;
use App\Models\Course;
use App\Models\Reports_msg;
use App\Models\Universities;
use App\Models\User;
use Encore\Admin\Facades\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;
use DB;
class EmailController extends Controller
{

    public static function SendReportMail($user_id,$msg_id,$url)
    {
        $by_user = User::find($user_id)->username;
        $msg = Reports_msg::find($msg_id)->msg;
        $data = ['message' => $msg,
            'url' => $url,
            'by_user' => $by_user];

        Mail::to('baghdady.usama@gmail.com')->send(new ReportMail($data));

    }

}
