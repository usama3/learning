<?php

namespace App\Http\Controllers;

use App\Events\QuestionAndAnswers;
use App\Models\Colleges;
use App\Models\Course;
use App\Models\Lecture_text_answers;
use App\Models\Lecture_text_questions;
use App\Models\Lecture_video_answers;
use App\Models\Lecture_video_questions;
use App\Models\Text_lecture;
use App\Models\Text_section;
use App\Models\Universities;
use App\Models\Video_lecture;
use App\Models\Video_section;
use Encore\Admin\Facades\Admin;
use App\Models\User;
use Validator;
use DB;
use Request;


class AnswersController extends Controller
{
    public function add_answer(\Illuminate\Http\Request $request){

        $validator = Validator::make($request->all(), [
            'text'=>'required|min:1|max:10000',
//            'picture'=>'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->ApiResponse(null,$errors->first(),422);
        }

        $transaction =  DB::transaction(function() use($request){

            $token = Request::header('token');
            $user = User::where('remember_token','=',$token)->first();

            if($request->type == 'text') {

                $id = DB::table('lecture_text_answers')->insertGetId([
                    'text' => $request->post('text'),
                    'user_id' => $user->id,
                    'picture' => $request->post('picture'),
                    'lecture_text_questions_id' => $request->post('question_id'),
                ]);

                $answer = Lecture_text_answers::with('user')
                          ->where('id','=',$id)->first();

                $socket_data = [
                    'type' => 'add_answer',
                    'by_user' => $user->id,
                    'data' => $answer
                ];
                event(new QuestionAndAnswers('text',$answer->lecture_text_questions_id,$socket_data));

            }
            else{

                $id = DB::table('lecture_video_answers')->insertGetId([
                    'text' => $request->post('text'),
                    'user_id' => $user->id,
                    'picture' => $request->post('picture'),
                    'lecture_video_questions_id' => $request->post('question_id'),
                ]);
                $answer = Lecture_video_answers::with('user')
                    ->where('id','=',$id)->first();

                $socket_data = [
                    'type' => 'add_answer',
                    'by_user' => $user->id,
                    'data' => $answer
                ];

                event(new QuestionAndAnswers('video',$answer->lecture_video_questions_id,$socket_data));

            }
            $c1 = Lecture_text_answers::where('user_id','=',$user->id)->count();
            $c2 = Lecture_video_answers::where('user_id','=',$user->id)->count();

            if($c1 + $c2 == 50)
                app('App\Http\Controllers\BadgesController')->Silver_Refiner($user->id);

            app('App\Http\Controllers\NotificationsController')->QuestionAnswered($user->id,$request->post('question_id'),$request->type,$request->course_id);

            if(empty($id)) {
                $errors = 'Failed to insert row in database.';
                return $this->ApiResponse(null, $errors, 17);
            }
            else
                return $this->ApiResponse($answer, null, 200);

        });
        return $transaction;


    }

    public function edit_answer(\Illuminate\Http\Request $request){

        $validator = Validator::make($request->all(), [
            'text'=>'required|min:1|max:10000',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->ApiResponse(null,$errors->first(),422);
        }

        $transaction =  DB::transaction(function() use($request) {

            if($request->type == 'text')
                $table = 'lecture_text_answers';
            else
                $table = 'lecture_video_answers';


            $id = DB::table($table)
                ->where('id', '=', $request->post('id'))
                ->update([
                    'text' => $request->post('text'),
                    'picture' => $request->post('picture'),
                ]);

            if($request->type == 'text')
                $answer = Lecture_text_answers::where('id','=',$request->post('id'))->first();
            else
                $answer = Lecture_video_answers::where('id','=',$request->post('id'))->first();


            $socket_data = [
                'type' => 'edit_answer',
                'data' => $answer
            ];

            if($request->type == 'text')
                event(new QuestionAndAnswers($request->type,$answer->lecture_text_questions_id,$socket_data));
            else
                event(new QuestionAndAnswers($request->type,$answer->lecture_video_questions_id,$socket_data));


            if (empty($id)) {
                $errors = 'Failed to update row in database.';
                return $this->ApiResponse(null, $errors, 17);
            } else
                return $this->ApiResponse($request->all(), null, 200);
        });

        return $transaction;

}

    public function delete_answer($type,$answer_id){

        $transaction =  DB::transaction(function() use($type,$answer_id) {

            if ($type == 'text')
                $table = 'lecture_text_answers';
            else
                $table = 'lecture_video_answers';

            $id = DB::table($table)
                ->where([
                    ['id', '=', $answer_id],
                ])->delete();

            if (empty($id)) {
                $errors = 'Failed to delete row in database.';
                return $this->ApiResponse(null, $errors, 17);
            } else
                return $this->ApiResponse(array(['type' => $type, 'answer_id' => $answer_id]), null, 200);
        });

        return $transaction;

    }


    public function mark_answer(\Illuminate\Http\Request $request){

        $transaction =  DB::transaction(function() use($request) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            if ($request->type == 'text') {
                $table1 = 'lecture_text_answers';
                $table2 = 'lecture_text_questions';
                $col = 'lecture_text_questions_id';
                $answerd_user_id = Lecture_text_answers::find($request->post('answer_id'))->user_id;

            } else {
                $table1 = 'lecture_video_answers';
                $table2 = 'lecture_video_questions';
                $col = 'lecture_video_questions_id';
                $answerd_user_id = Lecture_video_answers::find($request->post('answer_id'))->user_id;

            }

            $check = DB::table($table1)
                ->where([
                    [$col, '=', $request->post('question_id')],
                    ['marked', '=', 1]
                ])
                ->get();


            if ($check->count() == 0) {
                $id = DB::table($table1)
                    ->where('id', '=', $request->post('answer_id'))
                    ->update([
                        'marked' => 1,
                    ]);

                $points = DB::table($table2)
                    ->where('id', '=', $request->post('question_id'))
                    ->select('bounty')->get()[0]->bounty;


                DB::table('users')
                    ->where('id', '=', $answerd_user_id)
                    ->increment('points', $points);

                app('App\Http\Controllers\BadgesController')->add_points_and_date($user->id, $answerd_user_id);


                if (!empty($id) && $points >= 0)
                    app('App\Http\Controllers\BadgesController')->Bronze_Benefactor($user->id);

                if (!empty($id)) {
                    app('App\Http\Controllers\BadgesController')->Bronze_Rare($user->id);
                    app('App\Http\Controllers\NotificationsController')->AnswerMarked($user->id, $answerd_user_id, $request->post('answer_id'), $request->course_id,$request->type);

                }

                if (empty($id)) {
                    $errors = 'Failed to update row in database.';
                    return $this->ApiResponse(null, $errors, 17);
                } else
                    return $this->ApiResponse($request->all(), null, 200);

            }
            $errors = "You can't mark more than one answer.";
            return $this->ApiResponse(null, $errors, 17);
        });
        return $transaction;


    }

    public function un_mark_answer(\Illuminate\Http\Request $request){

        $transaction =  DB::transaction(function() use($request) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            if ($request->type == 'text') {
                $table1 = 'lecture_text_answers';
                $table2 = 'lecture_text_questions';
            } else {
                $table1 = 'lecture_video_answers';
                $table2 = 'lecture_video_questions';
            }

            $id = DB::table($table1)
                ->where('id', '=', $request->post('answer_id'))
                ->update([
                    'marked' => 0,
                ]);

            $points = DB::table($table2)
                ->where('id', '=', $request->post('question_id'))
                ->select('bounty')->get()[0]->bounty;

            DB::table('users')
                ->where('id', '=', $user->id)
                ->increment('points', -$points);

            if (empty($id)) {
                $errors = 'Failed to update row in database.';
                return $this->ApiResponse(null, $errors, 17);
            } else
                return $this->ApiResponse($request->all(), null, 200);
        });
        return $transaction;

    }


}