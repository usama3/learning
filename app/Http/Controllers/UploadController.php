<?php

namespace App\Http\Controllers;

use Validator;
use DB;
use Request;
use Image;


class UploadController extends Controller
{
    public function upload_pic(\Illuminate\Http\Request $request,$type){
        $transaction =  DB::transaction(function() use($request,$type) {

            $files = $request->file('picture');

            if ($request->hasFile('picture')) {
                $final_paths = array();
                foreach ($files as $file) {

                    $validator = Validator::make($request->all(), [
                        'picture.*' => 'image|required|mimes:jpeg,png,jpg',
                        'between.*' => 'min:10,max:170'
                    ]);

                    if ($validator->fails()) {
                        $errors = "Picture must have the following rules:\r\n 1- size:between:10KB and 5MB\r\n 2- extinction:jpeg,png,jpg";
                        return $this->ApiResponse(null, $errors, 422);
                    }


                    $originalImage = $file;
                    $Image = Image::make($originalImage);
//        $Image->resize(320, 240);
                    $extra = '';

                    if ($type == "text_question" || $type == "video_question") {
                        $originalPath = public_path();
                        $extra = '/uploads/images/Questions/' . time() . $originalImage->getClientOriginalName();
                        $path = $originalPath . $extra;
                        $Image->save($path);
                    }
                    if ($type == "text_answer" || $type == "video_answer") {
                        $originalPath = public_path();
                        $extra = '/uploads/images/Answers/' . time() . $originalImage->getClientOriginalName();
                        $path = $originalPath . $extra;
                        $Image->save($path);

                    }
                    if ($type == "profile") {
                        $originalPath = public_path();
                        $extra = '/uploads/images/Users/' . time() . $originalImage->getClientOriginalName();
                        $path = $originalPath . $extra;
                        $Image->save($path);
                    }
                    
                    array_push($final_paths, $extra);

                }
                return $this->ApiResponse($final_paths, null, 200);


            }
        });
        return $transaction;

    }
}
