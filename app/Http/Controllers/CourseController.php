<?php

namespace App\Http\Controllers;

use App\Http\Middleware\Offer;
use App\Models\Colleges;
use App\Models\Course;
use App\Models\Lecture_text_answers;
use App\Models\Lecture_text_questions;
use App\Models\Lecture_video_answers;
use App\Models\Lecture_video_questions;
use App\Models\Offers;
use App\Models\Text_lecture;
use App\Models\Text_section;
use App\Models\Universities;
use App\Models\User;
use App\Models\Video_lecture;
use App\Models\Video_section;
use Carbon\Carbon;
use Encore\Admin\Facades\Admin;
use Request;
use Validator;
use DB;

class CourseController extends Controller
{
    public function index($course_id)
    {
        $transaction =  DB::transaction(function() use($course_id) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            $user_role = User::with('roles')
                ->where('users.id', '=', $user->id)
                ->get();

            $admin = false;
            foreach ($user_role[0]->roles as $role)
                if ($role->name == 'Administrator')
                    $admin = true;

            $limitations = User::with('offers.limitations')
                ->where('users.id', '=', $user->id)
                ->get();

            $query = Course::with('colleges', 'universities', 'majors')
                ->where('courses.id', '=', $course_id)
                ->get();

            $text_lectures = Text_lecture::with('text_sections')
                ->where('courses_id', '=', $course_id)
                ->get();

            $video_lectures = Video_lecture::with('video_sections')
                ->where('courses_id', '=', $course_id)
                ->get();

            $progress = app('App\Http\Controllers\UserProgressController')->user_course_progress($course_id);

            $all = array();
            foreach ($text_lectures as $ob)
                array_push($all, $ob);
            foreach ($video_lectures as $ob)
                array_push($all, $ob);

            usort($all, function ($a, $b) {
                return $a->number - $b->number;
            });
            $limits = [];
            if (!$admin)
                if (!empty($limitations[0]->offers[0]))
                    foreach ($limitations[0]->offers[0]->limitations as $limt)
                        $limits[$limt->name] = true;
                else
                    $limits = [];
            else
                $limits = 'Admin';

            $next = new \stdClass();
            if ($progress->count() != 0) {
                $i = 0;
                $last = $progress[$progress->count() - 1];
                foreach ($all as $lec) {

                    if (($lec->id == $last->lecture_id)) {
                        if ((isset($lec->text_sections) && ('text' == $last->lecture_type) || (isset($lec->video_sections) && ('video' == $last->lecture_type)))) {
                            $j = $i + 1;
                            if ($all[$j]) {
                                $next->next_id = $all[$j]->id;
                                if (isset($all[$j]->text_sections))
                                    $next->next_type = 'text';
                                else
                                    $next->next_type = 'video';

                            } else
                                $next = null;
                        }

                    }
                    if (($lec->id == $last->lecture_id) && isset($lec->text_sections)) {

                    }
                    $i++;
                }

            } else {
                $next->next_id = $all[0]->id;
                if (isset($all[0]->text_sections))
                    $next->next_type = 'text';
                else
                    $next->next_type = 'video';
            }


            $ret = new \stdClass();
            $ret->course = $query;
            $ret->lectures = $all;
            $ret->limitations = $limits;
            $ret->progress = $progress;

            if (!empty((array)$next)) {
                $ret->course[0]->next_id = $next->next_id;
                $ret->course[0]->next_type = $next->next_type;
            }
            else{
                $ret->course[0]->next_id = null;
                $ret->course[0]->next_type = null;
            }

            return $this->ApiResponse($ret, null, 200);
        });

        return $transaction;

    }

    public function course_info($course_id){
        $transaction =  DB::transaction(function() use($course_id) {

            $token = Request::header('token');
            if ($token != "") {
                $user = User::where('remember_token', '=', $token)->first();

                if (!empty($user)) {

                    $all = new \stdClass();
                    $course = Course::with('colleges', 'universities', 'majors')
                        ->where('id', '=', $course_id)->get();

                    $user_rate = DB::table('courses_rating')
                        ->where([
                            ['user_id', '=', $user->id],
                            ['course_id', '=', $course_id],
                        ])
                        ->first();

                    $all->course = $course;
                    $all->can_access = $this->can_access();
                    if($user_rate)
                        $all->user_rate = $user_rate->rate;
                    else
                        $all->user_rate = null;

                } else {
                    $errors = "Token doesn't belong to user!";
                    return $this->ApiResponse(null, $errors, 404);
                }
            } else {

                $all = new \stdClass();
                $course = Course::with('colleges', 'universities', 'majors')
                    ->where('id', '=', $course_id)->get();
                $all->course = $course;
                $all->can_access = 'null';
                $all->user_rate = 'null';

            }

            return $this->ApiResponse($all, null, 200);
        });
        return $transaction;
    }

    public function rate_course($course_id,\Illuminate\Http\Request $request){

        $transaction =  DB::transaction(function() use($request,$course_id) {

            $validator = Validator::make($request->all(), [
                'rate' => 'required|numeric|between:0.0,5.0',
            ]);

            if ($validator->fails()) {
                $errors = $validator->errors();
                return $this->ApiResponse(null, $errors->first(), 422);
            }

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            $check = DB::table('courses_rating')
                ->where([
                    ['user_id', '=', $user->id],
                    ['course_id', '=', $course_id],
                ])
                ->get();


            if ($check->count() == 0) {
                $id = DB::table('courses_rating')
                    ->insertGetId([
                        'user_id' => $user->id,
                        'course_id' => $course_id,
                        'rate' => $request->post('rate'),

                    ]);

            } else {

                $id = DB::table('courses_rating')
                    ->where([
                        ['user_id', '=', $user->id],
                        ['course_id', '=', $course_id],
                    ])
                    ->update([
                        'rate' => $request->post('rate'),
                    ]);
            }

            $rate = DB::table('courses_rating')
                ->where('course_id', '=', $course_id)
                ->avg('rate');

            $id2 = DB::table('courses')
                ->where([
                    ['id', '=', $course_id],
                ])
                ->update([
                    'rating' => $rate,
                ]);

            if (empty($id) || empty($id2)) {
                $errors = 'Failed to insert row in database.';
                return $this->ApiResponse(null, $errors, 17);
            } else
                return $this->ApiResponse($request->all(), null, 200);
        });
        return $transaction;
    }

    private function can_access()
    {
        $transaction =  DB::transaction(function() {

            $course_id = Request::segment(3);
            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            if ($user) {
                $user_offer = User::with('offers', 'offers.periods')
                    ->where('id', '=', $user->id)
                    ->first();

                if (!$user_offer->offers->isEmpty()) {
                    if (($user_offer->offers[0]->universities_id == 0 && $user_offer->offers[0]->colleges_id == 0) ||
                        ($user_offer->offers[0]->universities_id == 0 && $user_offer->offers[0]->colleges_id == $user->colleges_id) ||
                        ($user_offer->offers[0]->universities_id == $user->universities_id && $user_offer[0]->offers->colleges_id == 0) ||
                        ($user_offer->offers[0]->universities_id == $user->universities_id && $user_offer[0]->offers->colleges_id == $user->colleges_id)
                    ) {

                        $current_time = new \DateTime(Carbon::now()->toDateTimeString());
                        $period = false;
                        foreach ($user_offer->offers[0]->periods as $period) {
                            $start = new \DateTime($period->start_date);
                            $end = new \DateTime($period->end_date);
                            if (((strtotime($current_time->format('y-m-d')) - strtotime($end->format('y-m-d'))) <= 0) &&
                                ((strtotime($current_time->format('y-m-d')) - strtotime($start->format('y-m-d'))) >= 0)
                            ) {
                                $period = true;
                            }
                        }

                        if ($period == true) {

                            $course = Course::where('id', '=', $course_id)->first();
                            $course_year = $course->year;
                            $current_year = $user->year_of_study;
                            $previous_year = $user->year_of_study - 1;
                            $next_year = $user->year_of_study + 1;
                            $allowed_years = (explode(',', $user_offer->offers[0]->years));

                            if (($current_year == $course_year && in_array('current', $allowed_years)) ||
                                ($previous_year == $course_year && in_array('previous', $allowed_years)) ||
                                ($next_year == $course_year && in_array('next', $allowed_years)) ||
                                in_array('all', $allowed_years)
                            ) {

                                $course_semester = $course->semester;
                                $allowed_semesters = (explode(',', $user_offer->offers[0]->semesters));
                                if ((in_array($course_semester, $allowed_semesters))) {
                                    return 1;
                                } else
                                    return 0;
                            } else
                                return 0;
                        } else
                            return 0;
                    } else
                        return 0;
                } else
                    return 0;
            } else
                return 0;
        });
        return $transaction;
    }






}
















