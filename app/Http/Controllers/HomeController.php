<?php

namespace App\Http\Controllers;

use App\Models\Colleges;
use App\Models\Course;
use App\Models\Universities;
use App\Models\User;
use Carbon\Carbon;
use Encore\Admin\Facades\Admin;
use Validator;
use DB;
use Request;
class HomeController extends Controller
{
    private function user_visit($user_id){

        $now = Carbon::now('America/Boise')->subDay()->toDateString();

        $check = DB::table('user_visits')->where([
            ['user_id','=', $user_id],
            ['date' ,'=', $now],
        ])
            ->get();

        if($check->count() == 0) {
            DB::table('user_visits')->insert([
                'user_id' => $user_id,
                'date' => $now,
            ]);
        }
    }

    public function index()
    {
        $token = Request::header('token');
        if($token != "") {
            $user = User::where('remember_token', '=', $token)->first();
            if(!empty($user)) {
                $this->user_visit($user->id);
                app('App\Http\Controllers\BadgesController')->Silver_Enthusiast($user->id);
                app('App\Http\Controllers\BadgesController')->Gold_Fanatic($user->id);
            }
        }
        $query = Universities::with('colleges')
            ->get();

        if(count($query) == 0)
            return redirect('/');

        return $this->ApiResponse($query,null,200);


    }
    public function college($college_id)
    {
        $query = Colleges::with('majors','universities')
            ->where('colleges.id','=',$college_id)
            ->get();

        if(count($query) == 0)
            return redirect('/');

        return $this->ApiResponse($query,null,200);
    }

    public function college_and_uni($college_id)
    {
        $query = Colleges::with('course','universities')
            ->where('colleges.id','=',$college_id)
            ->get();

        if(count($query) == 0)
            return redirect('/');

        return $this->ApiResponse($query,null,200);
    }


    public function college_year($college_id,$year)
    {

        $query = DB::table('colleges')
            ->join('courses','courses.colleges_id','colleges.id')
            ->join('universities','universities.id','colleges.universities_id')
            ->where('colleges.id','=',$college_id)
            ->where('courses.year','=',$year)
            ->select('colleges.id','colleges.name','colleges.picture','colleges.description',
                'universities.name as university_name','universities.picture as university_picture',
                'courses.name as course_name','courses.picture as course_picture','courses.year','courses.semester'
                ,'courses.description as course_description','courses.rating','courses.professor')
            ->get();


        if($query->count() != 0 )
            return $this->ApiResponse($query,null,200);
        else{
            $errors = "College or year doesn't exist";
            return $this->ApiResponse(null, $errors, 404);
        }
    }



}
