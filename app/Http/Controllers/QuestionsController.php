<?php

namespace App\Http\Controllers;

use App\Events\QuestionAndAnswers;
use App\Events\Questions;
use App\Models\Colleges;
use App\Models\Course;
use App\Models\Lecture_text_answers;
use App\Models\Lecture_text_questions;
use App\Models\Lecture_video_answers;
use App\Models\Lecture_video_questions;
use App\Models\Text_lecture;
use App\Models\Text_section;
use App\Models\Universities;
use App\Models\User;
use App\Models\Video_lecture;
use App\Models\Video_section;
use Encore\Admin\Facades\Admin;
use Illuminate\Support\Facades\Storage;
use Validator;
use DB;
use Request;

class QuestionsController extends Controller
{
    public function section_questions($type,$section_id){
        $transaction =  DB::transaction(function() use($type,$section_id) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            if($type == "text") {
                $questions = DB::table('lecture_text_questions')
                    ->join('users', 'users.id', 'lecture_text_questions.user_id')
                    ->leftjoin("vote_text_questions", function ($join) {
                        $join->on("vote_text_questions.user_id", "=", "users.id")
                            ->on("vote_text_questions.id", "=", "lecture_text_questions.id");
                    })
                    ->where([
                        ['lecture_text_questions.text_sections_id', '=', $section_id],
                    ])
                    ->select("lecture_text_questions.*", 'users.id as user_id','users.picture','users.username',
                        'vote_text_questions.vote_type')
                    ->get();
            }
            else{
                $questions = DB::table('lecture_video_questions')
                    ->join('users', 'users.id', 'lecture_video_questions.user_id')
                    ->leftjoin("vote_video_questions", function ($join) {
                        $join->on("vote_video_questions.user_id", "=", "users.id")
                            ->on("vote_video_questions.id", "=", "lecture_video_questions.id");
                    })
                    ->where([
                        ['lecture_video_questions.video_sections_id', '=', $section_id],
                    ])
                    ->select("lecture_video_questions.*", 'users.id as user_id','users.picture','users.username',
                        'vote_video_questions.vote_type')
                    ->get();
            }
            return $this->ApiResponse($questions, null, 200);
        });
        return $transaction;
    }



    public function questions($course_id)
    {
        $transaction =  DB::transaction(function() use($course_id) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();
            $text_lectures_questions = lecture_text_questions::with(['text_sections.text_lecture', 'user', 'text_answers', 'text_answers.user'
                , 'vote' => function ($query) use ($user) {
                    $query->where('user_id', '=', $user->id);
                }, 'text_answers.vote' => function ($query) use ($user) {
                    $query->where('user_id', '=', $user->id);
                }
            ])
                ->orderBy('created_at', 'desc')
                ->where('course_id', '=', $course_id)
                ->get();

            $video_lectures_questions = lecture_video_questions::with(['video_sections.video_lecture', 'user', 'video_answers', 'video_answers.user'
                , 'vote' => function ($query) use ($user) {
                    $query->where('user_id', '=', $user->id);
                } , 'video_answers.vote' => function ($query) use ($user)  {
                    $query->where('user_id', '=', $user->id);
                }   ])
                ->orderBy('created_at', 'desc')
                ->where('course_id', '=', $course_id)
                ->get();


            $all = array();
            foreach ($text_lectures_questions as $ob)
                array_push($all, $ob);
            foreach ($video_lectures_questions as $ob)
                array_push($all, $ob);

            usort($all, function ($a, $b) {
                return strtotime($b->created_at) - strtotime($a->created_at);
            });

            return $this->ApiResponse($all, null, 200);
        });
        return $transaction;

    }

    public function question_and_answers($course_id,$type,$question_id)
    {
        $transaction =  DB::transaction(function() use($course_id,$type,$question_id) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();
            $marked = 0;

            $all = new \stdClass();
            if ($type == 'text') {
                $question = lecture_text_questions::with(['text_sections.text_lecture', 'user', 'text_answers', 'text_answers.user'
                    , 'vote' => function ($query) use ($user) {
                        $query->where('user_id', '=', $user->id);
                    }
                    , 'text_answers.vote' => function ($query) use ($user) {
                        $query->where('user_id', '=', $user->id);
                    }
                ])
                    ->orderBy('created_at', 'desc')
                    ->where('id', '=', $question_id)
                    ->get();

            } else {
                $question = lecture_video_questions::with(['video_sections.video_lecture', 'user', 'video_answers', 'video_answers.user'
                    , 'vote' => function ($query)use ($user)  {
                        $query->where('user_id', '=', $user->id);
                    }
                    , 'video_answers.vote' => function ($query) use ($user)  {
                        $query->where('user_id', '=', $user->id);
                    }
                ])
                    ->orderBy('created_at', 'desc')
                    ->where('id', '=', $question_id)
                    ->get();


            }
            return $this->ApiResponse($question, null, 200);
        });
        return $transaction;
    }

    public function socket_question_and_answers($course_id,$type,$question_id)
    {
        $transaction =  DB::transaction(function() use($course_id,$type,$question_id) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();
            $marked = 0;

            $all = new \stdClass();
            if ($type == 'text') {
                $question = lecture_text_questions::with(['text_sections.text_lecture', 'user', 'text_answers', 'text_answers.user'
                    , 'vote' => function ($query) use ($user) {
                        $query->where('user_id', '=', $user->id);
                    }
                    , 'text_answers.vote' => function ($query) use ($user) {
                        $query->where('user_id', '=', $user->id);
                    }
                ])
                    ->orderBy('created_at', 'desc')
                    ->where('id', '=', $question_id)
                    ->get();

            } else {
                $question = lecture_video_questions::with(['video_sections.video_lecture', 'user', 'video_answers', 'video_answers.user'
                    , 'vote' => function ($query)use ($user)  {
                        $query->where('user_id', '=', $user->id);
                    }
                    , 'video_answers.vote' => function ($query) use ($user)  {
                        $query->where('user_id', '=', $user->id);
                    }
                ])
                    ->orderBy('created_at', 'desc')
                    ->where('id', '=', $question_id)
                    ->get();


            }
            return $question;
        });
        return $transaction;
    }




    public function add_question(\Illuminate\Http\Request $request){
        $validator = Validator::make($request->all(), [
            'title' => 'required|min:1|max:100',
            'text' => 'required|min:1|max:10000',
//            'picture'=>'image|required',
        ]);


        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->ApiResponse(null, $errors->first(), 422);
        }
        $transaction =  DB::transaction(function() use($request) {


            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            if ($request->type == 'text') {

                $id = DB::table('lecture_text_questions')->insertGetId([
                    'title' => $request->post('title'),
                    'text' => $request->post('text'),
                    'picture' => $request->post('picture'),
                    'user_id' => $user->id,
                    'course_id' => $request->post('course_id'),
                    'text_lecture_id' => $request->post('lecture_id'),
                    'text_sections_id' => $request->post('section_id'),
                ]);


            } else {

                $id = DB::table('lecture_video_questions')->insertGetId([
                    'title' => $request->post('title'),
                    'text' => $request->post('text'),
                    'picture' => $request->post('picture'),
                    'user_id' => $user->id,
                    'course_id' => $request->post('course_id'),
                    'video_lecture_id' => $request->post('lecture_id'),
                    'video_sections_id' => $request->post('section_id'),
                ]);

            }

            $data = $this->socket_question_and_answers($request->course_id,$request->type,$id);
            $socket_data = [
                'type' => 'add_question',
                'by_user' => $user->id,
                'data' => $data
            ];

            event(new Questions($request->post('course_id'),$socket_data));


            app('App\Http\Controllers\NotificationsController')->NewQuestion($id, $request->type);
            if (empty($id)) {
                $errors = 'Failed to insert row in database.';
                return $this->ApiResponse(null, $errors, 17);
            } else
                return $this->ApiResponse($data, null, 200);
        });
        return $transaction;


    }

    public function edit_question(\Illuminate\Http\Request $request){

        $validator = Validator::make($request->all(), [
            'text'=>'required|min:1|max:10000',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->ApiResponse(null,$errors->first(),422);
        }
        $transaction =  DB::transaction(function() use($request) {

            if ($request->type == 'text')
                $table = 'lecture_text_questions';
            else
                $table = 'lecture_video_questions';

            $id = DB::table($table)
                ->where('id', '=', $request->post('id'))
                ->update([
                    'title' => $request->post('title'),
                    'text' => $request->post('text'),
                    'picture' => $request->post('picture'),
                ]);


            if($request->type == 'text')
                $question = Lecture_text_questions::where('id','=',$request->post('id'))->first();
            else
                $question = Lecture_video_questions::where('id','=',$request->post('id'))->first();


            $socket_data = [
                'type' => 'edit_question',
                'data' => $question
            ];

            if($request->type == 'text') {
                event(new Questions($request->post('course_id'),$socket_data));
                event(new QuestionAndAnswers($request->type, $request->post('id'), $socket_data));
            }
            else {
                event(new Questions($request->post('course_id'),$socket_data));
                event(new QuestionAndAnswers($request->type, $request->post('id'), $socket_data));
            }


            if (empty($id)) {
                $errors = 'Failed to update row in database.';
                return $this->ApiResponse(null, $errors, 17);
            } else
                return $this->ApiResponse($question, null, 200);
        });
        return $transaction;

    }

    public function delete_question($type,$question_id){
        $transaction =  DB::transaction(function() use($type,$question_id) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            if ($type == 'text')
                $table = 'lecture_text_questions';
            else
                $table = 'lecture_video_questions';


            if ($type == 'text') {

                $id1 = DB::table('lecture_text_questions')
                    ->where([
                        ['id', '=', $question_id],
                    ])->delete();

                $id2 = DB::table('lecture_text_answers')
                    ->where([
                        ['lecture_text_questions_id', '=', $question_id],
                    ])->delete();

            } else {
                $id1 = DB::table('lecture_video_questions')
                    ->where([
                        ['id', '=', $question_id],
                    ])->delete();

                $id2 = DB::table('lecture_video_answers')
                    ->where([
                        ['lecture_video_questions_id', '=', $question_id],
                    ])->delete();

            }

            if (empty($id1) && empty($id2)) {
                $errors = 'Failed to delete row in database.';
                return $this->ApiResponse(null, $errors, 17);
            } else
                return $this->ApiResponse(array(['type' => $type, 'question_id' => $question_id]), null, 200);
        });
        return $transaction;
    }

    public function add_bounty(\Illuminate\Http\Request $request){

        $validator = Validator::make($request->all(), [
            'bounty'=>'required|min:1|numeric',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->ApiResponse(null,$errors->first(),422);
        }
        $transaction =  DB::transaction(function() use($request) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            if ($user->points >= $request->post('bounty')) {

                DB::table('users')
                    ->where('id', '=', $user->id)
                    ->increment('points', -$request->post('bounty'));

                if ($request->type == 'text') {
                    $id = DB::table('lecture_text_questions')
                        ->where('id', '=', $request->post('id'))
                        ->update([
                            'bounty' => $request->post('bounty'),
                        ]);

                } else {

                    $id = DB::table('lecture_video_questions')
                        ->where('id', '=', $request->post('id'))
                        ->update([
                            'bounty' => $request->post('bounty'),
                        ]);
                }

                if (!empty($id)) {
                    app('App\Http\Controllers\BadgesController')->Bronze_Promoter($user->id);
                    app('App\Http\Controllers\NotificationsController')->QuestionBounty($id, $request->type);

                }

                if($request->type == 'text')
                    $question = Lecture_text_questions::where('id','=',$request->post('id'))->first();
                else
                    $question = Lecture_video_questions::where('id','=',$request->post('id'))->first();

                $socket_data = [
                    'type' => 'edit_question',
                    'by_user' => $user->id,
                    'data' => $question
                ];


                if (empty($id)) {
                    $errors = 'Failed to update row in database.';
                    return $this->ApiResponse(null, $errors, 17);
                } else {
                    event(new Questions($request->post('course_id'),$socket_data));
                    return $this->ApiResponse($request->all(), null, 200);
                }
            } else {
                $errors = 'No ` Points';
                return $this->ApiResponse(null, $errors, 112);
            }
        });
        return $transaction;
    }

    public function cancel_bounty(\Illuminate\Http\Request $request){

        $transaction =  DB::transaction(function() use($request) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            DB::table('users')
                ->where('id', '=', $user->id)
                ->increment('points', $request->post('bounty'));

            if ($request->type == 'text') {

                $id = DB::table('lecture_text_questions')
                    ->where('id', '=', $request->post('id'))
                    ->update([
                        'bounty' => 0,
                    ]);

            } else {

                $id = DB::table('lecture_video_questions')
                    ->where('id', '=', $request->post('id'))
                    ->update([
                        'bounty' => 0,
                    ]);
            }

            if($request->type == 'text')
                $question = Lecture_text_questions::where('id','=',$request->post('id'))->first();
            else
                $question = Lecture_video_questions::where('id','=',$request->post('id'))->first();

            $socket_data = [
                'type' => 'edit_question',
                'by_user' => $user->id,
                'data' => $question
            ];



            if (empty($id)) {
                $errors = 'Failed to update row in database.';
                return $this->ApiResponse(null, $errors, 17);
            } else{
                event(new Questions($request->post('course_id'),$socket_data));
                return $this->ApiResponse($request->all(), null, 200);
            }
        });
        return $transaction;

    }


}