<?php
namespace App\Http\Controllers\Auth;
use App\Models\Role;
use App\Traits\ApiResponseTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Jobs\SendVerificationEmail;
use App\Notifications\SignupActivate;
use DB;

class AuthController extends Controller
{
    use ApiResponseTrait;
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    //TOFIX
    public function register(Request $request)
    {
        $hash = str_random(64);
        $data = $request->all();
        $user = User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'picture' => $data['picture'],
            'points' => 100,
            'first_name' => $data['first_name'],
            'middle_name' => $data['middle_name'],
            'last_name' => $data['last_name'],
//            'mobile' => $data['mobile'],
//            'gender' => $data['gender'],
//            'birthdate' => $data['birthdate'],
            'year_of_study' => $data['year_of_study'],
            'countries_id' => $data['countries_id'],
            'cities_id' => $data['cities_id'],
            'universities_id' => $data['universities_id'],
            'colleges_id' => $data['colleges_id'],
            'majors_id' => $data['majors_id'],
        ]);
        User::where('id','=',$user->id)->update(['activation_token'=> $hash]);

//        $user->notify(new SignupActivate($hash));
        $this->dispatch(new SendVerificationEmail($user,$hash));

//        dispatch(new SendVerificationEmail($user));

        return $this->ApiResponse($data, null, 200);

    }

    public function signupActivate($token)
    {
        $user = User::where('activation_token', $token)->first();
        $role = Role::where('name', 'User')->first();
        if (!$user)
            return $this->ApiResponse(null, 'This activation token is invalid.', 404);

        $user->verified = true;
        $user->activation_token = '';
        $user->save();

        DB::table('role_user')->insert([
            'user_id' => $user->id,
            'role_id' => $role->id,
        ]);

        return $this->ApiResponse($user, null, 200);
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);

        $token->save();
        $tok = $tokenResult->accessToken;
        User::where('email','=',$request->email)->update(['remember_token' => $tok]);

        $data =  [
            'user_id' => $user->id,
            'access_token' => $tok,
            'token_type' => 'Bearer',
            'username' =>$user->username,
            'picture' => $user->picture,
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ];
        return $this->ApiResponse($data, null, 200);

    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }
}