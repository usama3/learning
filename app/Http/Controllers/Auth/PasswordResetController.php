<?php
namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponseTrait;
use Carbon\Carbon;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\Models\User;
use App\Models\PasswordReset;
use Request;

class PasswordResetController extends Controller
{
    use ApiResponseTrait;
    /**
     * Create token password reset
     *
     * @param  [string] email
     * @return [string] message
     */
    public function create(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
        ]);
        $user = User::where('email', $request->email)->first();
        if (!$user)
            return $this->ApiResponse(null, "We can't find a user with that e-mail address.", 404);

        $tt = str_random(60);
        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => $tt
             ]
        );
        $user->notify(new PasswordResetRequest($tt));

        return $this->ApiResponse('We have e-mailed your password reset link!', null, 200);

    }
    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return [string] message
     * @return [json] passwordReset object
     */
    public function find($token)
    {
        $passwordReset = PasswordReset::where('token', $token)
            ->first();
        if (!$passwordReset)
            return $this->ApiResponse(null, 'This password reset token is invalid.', 404);

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return $this->ApiResponse(null, 'This password reset token is invalid.', 404);

        }

        return $this->ApiResponse($passwordReset, null, 200);

    }
     /**
     * Reset password
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
     * @return [string] message
     * @return [json] user object
     */
    public function reset(\Illuminate\Http\Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string|confirmed',
        ]);
        $passwordReset = PasswordReset::where([
            ['token', Request::header('token')],
            ['email', $request->email]
        ])->first();

        if (!$passwordReset)
            return $this->ApiResponse(null, 'This password reset token is invalid.', 404);

        $user = User::where('email', $passwordReset->email)->first();
        if (!$user)
            return $this->ApiResponse(null,"We can't find a user with that e-mail address.", 200);

        $user->password = bcrypt($request->password);
        $user->save();
        $passwordReset->delete();
        $user->notify(new PasswordResetSuccess($passwordReset));

        return $this->ApiResponse($user, null, 200);

    }
}