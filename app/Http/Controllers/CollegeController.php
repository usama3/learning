<?php

namespace App\Http\Controllers;

use App\Models\Colleges;
use App\Models\Course;
use App\Models\Majors;
use App\Models\Universities;
use Encore\Admin\Facades\Admin;
use Illuminate\Http\Request;
use Validator;
use DB;

class CollegeController extends Controller
{
    public function college($college_id)
    {
        $transaction =  DB::transaction(function() use($college_id) {

            $query = Colleges::with('universities')
                ->where('colleges.id', '=', $college_id)
                ->get();

            $query[0]->years = $this->college_years($college_id);

            return $this->ApiResponse($query, null, 200);
        });
        return $transaction;
    }


    public function courses_by_majors($college_id,$years_array,$majors_array){

        if ($majors_array == '_')
            $courses = DB::table('majors')
                ->join('courses','courses.majors_id','majors.id')
                ->whereIn('courses.year',explode(',',$years_array))
                ->select('courses.*','majors.name as major_name')
                ->get();
        else
            $courses = DB::table('majors')
                ->join('courses','courses.majors_id','majors.id')
                ->whereIn('courses.year',explode(',',$years_array))
                ->whereIn('majors.id',explode(',',$majors_array))
                ->select('courses.*','majors.name as major_name')
                ->get();

        return $this->ApiResponse($courses,null,200);
    }


    private function college_years($college_id){
        $transaction =  DB::transaction(function() use($college_id) {

            $query = Majors::where('colleges_id', '=', $college_id)
                ->get();

            $years_and_majors = [];

            foreach ($query as $q) {
                $years = explode(',', $q->years);

                foreach ($years as $year) {
                    if (isset($years_and_majors[$year]))
                        array_push($years_and_majors[$year], ['id' => $q->id, 'name' => $q->name]);
                    else
                        $years_and_majors[$year] = [['id' => $q->id, 'name' => $q->name]];

                }
            }
            ksort($years_and_majors);

            $all = array();

            foreach ($years_and_majors as $key => $value)
                array_push($all, ['year' => $key, 'majors' => $value]);

            return $all;
        });

        return $transaction;

    }
}
