<?php

namespace App\Http\Controllers;

use App\Events\Bookmarks;
use App\Models\Colleges;
use App\Models\Course;
use App\Models\Lecture_text_answers;
use App\Models\Lecture_text_bookmarks;
use App\Models\Lecture_text_questions;
use App\Models\Lecture_video_answers;
use App\Models\Lecture_video_bookmarks;
use App\Models\Lecture_video_questions;
use App\Models\Text_lecture;
use App\Models\Text_section;
use App\Models\Universities;
use App\Models\User;
use App\Models\Video_lecture;
use App\Models\Video_section;
use Encore\Admin\Facades\Admin;
use Request;
use Validator;
use DB;

class BookmarksController extends Controller
{
    public function section_bookmarks($type,$section_id){
        $transaction =  DB::transaction(function() use($type,$section_id) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            if($type == "text") {
                $bookmarks = DB::table('lecture_text_bookmarks')
                    ->join('users', 'users.id', 'lecture_text_bookmarks.user_id')
                    ->leftjoin("vote_text_bookmarks", function ($join) {
                        $join->on("vote_text_bookmarks.user_id", "=", "users.id")
                            ->on("vote_text_bookmarks.id", "=", "lecture_text_bookmarks.id");
                    })
                    ->where([
                        ['lecture_text_bookmarks.private', '=', 0],
                        ['lecture_text_bookmarks.text_section_id', '=', $section_id],
                    ])
                    ->orWhere([
                        ['lecture_text_bookmarks.private', '=', 1],
                        ['lecture_text_bookmarks.user_id', '=', $user->id],
                        ['lecture_text_bookmarks.text_section_id', '=', $section_id],
                    ])
                    ->select("lecture_text_bookmarks.*", 'users.id as user_id','users.picture','users.username',
                        'vote_text_bookmarks.vote_type')
                    ->get();
            }
            else{
                $bookmarks = DB::table('lecture_video_bookmarks')
                    ->join('users', 'users.id', 'lecture_video_bookmarks.user_id')
                    ->leftjoin("vote_video_bookmarks", function ($join) {
                        $join->on("vote_video_bookmarks.user_id", "=", "users.id")
                            ->on("vote_video_bookmarks.id", "=", "lecture_video_bookmarks.id");
                    })
                    ->where([
                        ['lecture_video_bookmarks.private', '=', 0],
                        ['lecture_video_bookmarks.video_section_id', '=', $section_id],
                    ])
                    ->orWhere([
                        ['lecture_video_bookmarks.private', '=', 1],
                        ['lecture_video_bookmarks.user_id', '=', $user->id],
                        ['lecture_video_bookmarks.video_section_id', '=', $section_id],
                    ])
                    ->select("lecture_video_bookmarks.*", 'users.id as user_id','users.picture','users.username',
                        'vote_video_bookmarks.vote_type')
                    ->get();
            }
            return $this->ApiResponse($bookmarks, null, 200);
        });
        return $transaction;
    }



    public function get_bookmark($type,$bookmark_id)
    {
        $transaction =  DB::transaction(function() use($type,$bookmark_id) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            if ($user) {
                $bookmark = [];
                if ($type == 'text') {
                    $bookmark = Lecture_text_bookmarks::with(['user', 'vote' => function ($query) use ($user) {
                        $query->where('user_id', '=', $user->id);
                    }])
                        ->where('id', '=', $bookmark_id)
                        ->get();
                } else {
                    $bookmark = Lecture_video_bookmarks::with(['user', 'vote' => function ($query) use ($user) {
                        $query->where('user_id', '=', $user->id);
                    }])
                        ->where('id', '=', $bookmark_id)
                        ->get();
                }

                if (!empty($bookmark[0])) {
                    if ($bookmark[0]->private == 0 || $bookmark[0]->user->id == $user->id)
                        return $this->ApiResponse($bookmark, null, 200);
                    else
                        return $this->ApiResponse(null, 'private', 403);
                } else
                    return $this->ApiResponse(null, 'Not found', 404);

            } else {
                $errors = "Token doesn't belong to user";
                return $this->ApiResponse(null, $errors, 422);
            }
        });
        return $transaction;

    }

    public function socket_bookmark($type,$bookmark_id,$lecture_id,$section_id)
    {
        $transaction =  DB::transaction(function() use($type,$bookmark_id,$lecture_id,$section_id) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            if ($user) {
                $bookmark = [];
                if ($type == 'text') {
                    $bookmark = Text_lecture::with(['text_sections.lecture_text_bookmarks.user',
                        'text_sections' =>  function ($query)use($section_id) {
                        $query->where([
                            ['id', '=', $section_id]
                        ]);
                    },
                        'text_sections.lecture_text_bookmarks' => function ($query)use($bookmark_id) {
                        $query->where([
                            ['private', '=', 0],
                            ['id', '=', $bookmark_id]
                        ]);
                    }])
                        ->where('id','=',$lecture_id)
                        ->get();


                } else {
                    $bookmark = Video_lecture::with(['video_sections.lecture_video_bookmarks.user',
                        'video_sections' =>  function ($query)use($section_id) {
                            $query->where([
                                ['id', '=', $section_id]
                            ]);
                        },
                        'video_sections.lecture_video_bookmarks' => function ($query)use($bookmark_id) {
                            $query->where([
                                ['private', '=', 0],
                                ['id', '=', $bookmark_id]
                            ]);
                        }])
                        ->where('id','=',$lecture_id)
                        ->get();
                }

                if (!empty($bookmark[0])) {
                    if ($bookmark[0]->private == 0 || $bookmark[0]->user->id == $user->id)
                        return $bookmark;
                    else
                        return [];
                } else
                    return [];

            } else {
                $errors = "Token doesn't belong to user";
                return $errors;
            }
        });
        return $transaction;

    }


    public function all_bookmarks($course_id)
    {
        $transaction =  DB::transaction(function() use($course_id) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            $text_summeries = Text_lecture::with(['text_sections', 'text_sections.lecture_text_bookmarks.user', 'text_sections.lecture_text_bookmarks' => function ($query) {
                $query->where('private', '=', 0);
            }, 'text_sections.lecture_text_bookmarks.vote' => function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            }])
                ->where('courses_id', '=', $course_id)
                ->get();


            $video_summeries = Video_lecture::with(['video_sections', 'video_sections.lecture_video_bookmarks.user', 'video_sections.lecture_video_bookmarks' => function ($query) {
                $query->where('private', '=', 0);
            }, 'video_sections.lecture_video_bookmarks.vote' => function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            }])
                ->where('courses_id', '=', $course_id)
                ->get();

            $all = array();
            foreach ($text_summeries as $ob)
                array_push($all, $ob);
            foreach ($video_summeries as $ob)
                array_push($all, $ob);

            usort($all, function ($a, $b) {
                return $a->number - $b->number;
            });

            return $this->ApiResponse($all, null, 200);

        });
        return $transaction;
    }

    public function my_bookmarks($course_id)
    {
        $transaction =  DB::transaction(function() use($course_id) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();


            $text_summeries = Text_lecture::with(['text_sections', 'text_sections.lecture_text_bookmarks.user', 'text_sections.lecture_text_bookmarks' => function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            }, 'text_sections.lecture_text_bookmarks.vote' => function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            }])
                ->where('courses_id', '=', $course_id)
                ->get();

            $video_summeries = Video_lecture::with(['video_sections', 'video_sections.lecture_video_bookmarks.user', 'video_sections.lecture_video_bookmarks' => function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            }, 'video_sections.lecture_video_bookmarks.vote' => function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            }])
                ->where('courses_id', '=', $course_id)
                ->get();


            $all = array();
            foreach ($text_summeries as $ob)
                array_push($all, $ob);
            foreach ($video_summeries as $ob)
                array_push($all, $ob);

            usort($all, function ($a, $b) {
                return $a->number - $b->number;
            });

            return $this->ApiResponse($all, null, 200);
        });
        return $transaction;
    }



    public function add_bookmark(\Illuminate\Http\Request $request){

        $validator = Validator::make($request->all(), [
            'text'=>'required|min:1|max:99999',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->ApiResponse(null,$errors->first(),422);
        }
        $transaction =  DB::transaction(function() use($request) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            if ($request->type == 'text') {

                $id = DB::table('lecture_text_bookmarks')->insertGetId([
                    'text' => $request->post('text'),
                    'private' => $request->post('private'),
                    'user_id' => $user->id,
                    'text_section_id' => $request->post('section_id'),
                ]);

                if (empty($id)) {
                    $errors = 'Failed to insert row in database.';
                    return $this->ApiResponse(null, $errors, 17);
                }


            } else {

                $id = DB::table('lecture_video_bookmarks')->insertGetId([
                    'text' => $request->post('text'),
                    'private' => $request->post('private'),
                    'user_id' => $user->id,
                    'video_section_id' => $request->post('section_id'),
                ]);
                if (empty($id)) {
                    $errors = 'Failed to insert row in database.';
                    return $this->ApiResponse(null, $errors, 17);
                }

            }
            $data = $this->socket_bookmark($request->type,$id, $request->post('lecture_id'),$request->post('section_id'));
            $socket_data = [
                'type' => 'add_bookmark',
                'by_user' => $user->id,
                'data' => $data
            ];
            if($request->post('private') == 0)
                 event(new Bookmarks($request->post('course_id'),$socket_data));

            return $this->ApiResponse($data, null, 200);
        });
        return $transaction;

    }

    public function edit_bookmark(\Illuminate\Http\Request $request){

        $validator = Validator::make($request->all(), [
            'text'=>'required|min:1|max:99999',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->ApiResponse(null,$errors->first(),422);
        }

        $transaction =  DB::transaction(function() use($request) {

            if ($request->type == 'text') {

                $id = DB::table('lecture_text_bookmarks')
                    ->where('id', '=', $request->post('id'))
                    ->update([
                        'text' => $request->post('text'),
                        'private' => $request->post('private'),
                    ]);

            } else {

                $id = DB::table('lecture_video_bookmarks')
                    ->where('id', '=', $request->post('id'))
                    ->update([
                        'text' => $request->post('text'),
                        'private' => $request->post('private'),
                    ]);


            }

            if($request->type == 'text')
                $data = Lecture_text_bookmarks::where('id','=',$request->post('id'))->first();
            else
                $data = Lecture_video_bookmarks::where('id','=',$request->post('id'))->first();

            $socket_data = [
                'type' => 'edit_bookmark',
                'data' => $data
            ];

            if($request->post('private') == 0)
                 event(new Bookmarks($request->post('course_id'),$socket_data));


            if (empty($id)) {
                $errors = 'Failed to update row in database.';
                return $this->ApiResponse(null, $errors, 17);
            } else
                return $this->ApiResponse($data, null, 200);

        });
        return $transaction;
    }

    public function delete_bookmark($type,$bookmark_id){

        $transaction =  DB::transaction(function() use($type,$bookmark_id) {

            if ($type == 'text') {

                $id = DB::table('lecture_text_bookmarks')
                    ->where([
                        ['id', '=', $bookmark_id],
                    ])->delete();

            } else {
                $id = DB::table('lecture_video_bookmarks')
                    ->where([
                        ['id', '=', $bookmark_id],
                    ])->delete();

            }

            if (empty($id)) {
                $errors = 'Failed to delete row in database.';
                return $this->ApiResponse(null, $errors, 17);
            } else
                return $this->ApiResponse(array(['type' => $type, 'bookmark_id' => $bookmark_id]), null, 200);

        });
        return $transaction;

    }



}