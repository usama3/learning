<?php

namespace App\Http\Controllers;

use App\Events\Summaries;
use App\Models\Colleges;
use App\Models\Course;
use App\Models\Lecture_text_answers;
use App\Models\Lecture_text_questions;
use App\Models\Lecture_text_summaries;
use App\Models\Lecture_video_answers;
use App\Models\Lecture_video_questions;
use App\Models\Lecture_video_summaries;
use App\Models\Offers;
use App\Models\Text_lecture;
use App\Models\Text_section;
use App\Models\Universities;
use App\Models\User;
use App\Models\Video_lecture;
use App\Models\Video_section;
use Carbon\Carbon;
use Encore\Admin\Facades\Admin;
use Request;
use Validator;
use DB;

class SummariesController extends Controller
{
    public function section_summary($type,$lecture_id){
        $transaction =  DB::transaction(function() use($type,$lecture_id) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            if($type == "text") {
                $summeries = DB::table('lecture_text_summaries')
                    ->join('users', 'users.id', 'lecture_text_summaries.user_id')
                    ->leftjoin("vote_text_summaries", function ($join) {
                        $join->on("vote_text_summaries.user_id", "=", "users.id")
                            ->on("vote_text_summaries.id", "=", "lecture_text_summaries.id");
                    })
                    ->where([
                        ['lecture_text_summaries.private', '=', 0],
                        ['lecture_text_summaries.text_lecture_id', '=', $lecture_id],
                    ])
                    ->orWhere([
                        ['lecture_text_summaries.private', '=', 1],
                        ['lecture_text_summaries.user_id', '=', $user->id],
                        ['lecture_text_summaries.text_lecture_id', '=', $lecture_id],
                    ])
                    ->select("lecture_text_summaries.*", 'users.id as user_id','users.picture','users.username',
                        'vote_text_summaries.vote_type')
                    ->get();
            }
            else{
                $summeries = DB::table('lecture_video_summaries')
                    ->join('users', 'users.id', 'lecture_video_summaries.user_id')
                    ->leftjoin("vote_video_summaries", function ($join) {
                        $join->on("vote_video_summaries.user_id", "=", "users.id")
                            ->on("vote_video_summaries.id", "=", "lecture_video_summaries.id");
                    })
                    ->where([
                        ['lecture_video_summaries.private', '=', 0],
                        ['lecture_video_summaries.video_lecture_id', '=', $lecture_id],
                    ])
                    ->orWhere([
                        ['lecture_video_summaries.private', '=', 1],
                        ['lecture_video_summaries.user_id', '=', $user->id],
                        ['lecture_video_summaries.video_lecture_id', '=', $lecture_id],
                    ])
                    ->select("lecture_video_summaries.*", 'users.id as user_id', 'users.picture','users.username',
                        'vote_video_summaries.vote_type')
                    ->get();
            }
            return $this->ApiResponse($summeries, null, 200);
        });
        return $transaction;
    }

    public function get_summary($type,$summary_id)
    {
        $transaction =  DB::transaction(function() use($type,$summary_id) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            if ($user) {

                $summary = [];
                if ($type == 'text') {
                    $summary = Lecture_text_summaries::with(['user', 'vote' => function ($query) use ($user) {
                        $query->where('user_id', '=', $user->id);
                    }])
                        ->where('id', '=', $summary_id)
                        ->get();
                } else {
                    $summary = Lecture_video_summaries::with(['user', 'vote' => function ($query) use ($user) {
                        $query->where('user_id', '=', $user->id);
                    }])
                        ->where('id', '=', $summary_id)
                        ->get();
                }
                if (!empty($summary[0])) {
                    if ($summary[0]->private == 0 || $summary[0]->user->id == $user->id)
                        return $this->ApiResponse($summary, null, 200);
                    else
                        return $this->ApiResponse(null, 'private', 403);
                } else
                    return $this->ApiResponse(null, 'Not found', 404);
            } else {
                $errors = "Token doesn't belong to user";
                return $this->ApiResponse(null, $errors, 422);
            }
        });
        return $transaction;

    }

    public function socket_summary($type,$summary_id,$lecture_id)
    {
        $transaction =  DB::transaction(function() use($type,$summary_id,$lecture_id) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            if ($user) {

                $summary = [];
                if ($type == 'text') {

                    $summary = Text_lecture::with(['lecture_text_summaries.user', 'lecture_text_summaries'
                    => function ($query) use($summary_id) {
                        $query->where([
                            ['private', '=', 0],
                            ['id', '=', $summary_id]
                        ]);
                    }])
                        ->where('id','=',$lecture_id)
                        ->get();

                } else {
                    $summary = Video_lecture::with(['lecture_video_summaries.user', 'lecture_video_summaries'
                    => function ($query) use($summary_id) {
                            $query->where([
                                ['private', '=', 0],
                                ['id', '=', $summary_id]
                            ]);
                        }])
                        ->where('id','=',$lecture_id)
                        ->get();
                }

                if (!empty($summary[0])) {
                    if ($summary[0]->private == 0 || $summary[0]->user->id == $user->id)
                        return $summary;
                    else
                        return [];
                } else
                    return [];
            } else {
                $errors = "Token doesn't belong to user";
                return $errors;
            }
        });
        return $transaction;

    }


    public function all_summaries($course_id)
    {
        $transaction =  DB::transaction(function() use($course_id) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            $text_summeries = Text_lecture::with(['lecture_text_summaries.user', 'lecture_text_summaries' => function ($query) {
                $query->where('private', '=', 0);
            }, 'lecture_text_summaries.vote' => function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            }])
                ->where('courses_id', '=', $course_id)
                ->get();


            $video_summeries = Video_lecture::with(['lecture_video_summaries.user', 'lecture_video_summaries' => function ($query) {
                $query->where('private', '=', 0);
            }, 'lecture_video_summaries.vote' => function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            }])
                ->where('courses_id', '=', $course_id)
                ->get();

            $all = array();
            foreach ($text_summeries as $ob)
                array_push($all, $ob);
            foreach ($video_summeries as $ob)
                array_push($all, $ob);

            usort($all, function ($a, $b) {
                return $a->number - $b->number;
            });


            return $this->ApiResponse($all, null, 200);
        });
        return $transaction;
    }

    public function my_summaries($course_id)
    {
        $transaction =  DB::transaction(function() use($course_id) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            $text_summeries = Text_lecture::with(['lecture_text_summaries.user', 'lecture_text_summaries' => function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            }, 'lecture_text_summaries.vote' => function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            }])
                ->where('courses_id', '=', $course_id)
                ->get();


            $video_summeries = Video_lecture::with(['lecture_video_summaries.user', 'lecture_video_summaries' => function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            }, 'lecture_video_summaries.vote' => function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            }])
                ->where('courses_id', '=', $course_id)
                ->get();


            $all = array();
            foreach ($text_summeries as $ob)
                array_push($all, $ob);
            foreach ($video_summeries as $ob)
                array_push($all, $ob);

            usort($all, function ($a, $b) {
                return $a->number - $b->number;
            });


            return $this->ApiResponse($all, null, 200);
        });
        return $transaction;

    }



    public function add_summery(\Illuminate\Http\Request $request){

        $validator = Validator::make($request->all(), [
            'text'=>'required|min:1|max:99999',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->ApiResponse(null,$errors->first(),422);
        }
        $transaction =  DB::transaction(function() use($request) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();


            if ($request->type == 'text') {

                $id = DB::table('lecture_text_summaries')->insertGetId([
                    'text' => $request->post('text'),
                    'private' => $request->post('private'),
                    'user_id' => $user->id,
                    'text_lecture_id' => $request->post('lecture_id'),
                ]);

                if (empty($id)) {
                    $errors = 'Failed to insert row in database.';
                    return $this->ApiResponse(null, $errors, 17);
                }


            } else {

                $id = DB::table('lecture_video_summaries')->insertGetId([
                    'text' => $request->post('text'),
                    'private' => $request->post('private'),
                    'user_id' => $user->id,
                    'video_lecture_id' => $request->post('lecture_id'),
                ]);

                if (empty($id)) {
                    $errors = 'Failed to insert row in database.';
                    return $this->ApiResponse(null, $errors, 17);
                }

            }

            $data = $this->socket_summary($request->type,$id,$request->post('lecture_id'));
            $socket_data = [
                'type' => 'add_summary',
                'by_user' => $user->id,
                'data' => $data
            ];
            
            if($request->post('private') == 0)
                 event(new Summaries($request->post('course_id'),$socket_data));

            return $this->ApiResponse($data, null, 200);
        });
        return $transaction;

    }

    public function edit_summery(\Illuminate\Http\Request $request){

        $validator = Validator::make($request->all(), [
            'text'=>'required|min:1|max:99999',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->ApiResponse(null,$errors->first(),422);
        }
        $transaction =  DB::transaction(function() use($request) {

            if ($request->type == 'text') {

                $id = DB::table('lecture_text_summaries')
                    ->where('id', '=', $request->post('id'))
                    ->update([
                        'text' => $request->post('text'),
                        'private' => $request->post('private'),
                    ]);

            } else {

                $id = DB::table('lecture_video_summaries')
                    ->where('id', '=', $request->post('id'))
                    ->update([
                        'text' => $request->post('text'),
                        'private' => $request->post('private'),
                    ]);
            }

            if($request->type == 'text')
                $data = Lecture_text_summaries::where('id','=',$request->post('id'))->first();
            else
                $data = Lecture_video_summaries::where('id','=',$request->post('id'))->first();

            $socket_data = [
                'type' => 'edit_summary',
                'data' => $data
            ];
            
            if($request->post('private') == 0)
                  event(new Summaries($request->post('course_id'),$socket_data));

            if (empty($id)) {
                $errors = 'Failed to update row in database.';
                return $this->ApiResponse(null, $errors, 17);
            } else
                return $this->ApiResponse($data, null, 200);
        });
        return $transaction;

    }

    public function delete_summery($type,$summary_id){
        $transaction =  DB::transaction(function() use($type,$summary_id) {

            if ($type == 'text') {
                $id = DB::table('lecture_text_summaries')
                    ->where([
                        ['id', '=', $summary_id],
                    ])->delete();
            } else {
                $id = DB::table('lecture_video_summaries')
                    ->where([
                        ['id', '=', $summary_id],
                    ])->delete();
            }
            if (empty($id)) {
                $errors = 'Failed to delete row in database.';
                return $this->ApiResponse(null, $errors, 17);
            } else
                return $this->ApiResponse(array(['type' => $type, 'summary_id' => $summary_id]), null, 200);
        });
        return $transaction;

    }





}