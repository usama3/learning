<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\User;
use Request;
use Validator;
use DB;


class UserProgressController extends Controller
{

    public function add_progress_lecture(\Illuminate\Http\Request $request){
        $transaction =  DB::transaction(function() use($request) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            if ($request->lecture_type == 'text')
                $type = 'text';
            else
                $type = 'video';

            $progress = DB::table('user_progress')
                ->where([
                    ['course_id', '=', $request->post('course_id')],
                    ['lecture_id', '=', $request->post('lecture_id')],
                    ['user_id', '=', $user->id],
                    ['lecture_type', '=', $type],

                ])
                ->get();

            if ($progress->count() == 0) {

                $id = DB::table('user_progress')->insertGetId([
                    'course_id' => $request->post('course_id'),
                    'lecture_id' => $request->post('lecture_id'),
                    'user_id' => $user->id,
                    'lecture_type' => $type,
                ]);

                if (empty($id)) {
                    $errors = 'Failed to update row in database.';
                    return $this->ApiResponse(null, $errors, 17);
                } else
                    return $this->ApiResponse($request->all(), null, 200);
            } else {
                $errors = 'Already Completed!';
                return $this->ApiResponse(null, $errors, 17);
            }
        });
        return $transaction;
    }

    public function user_course_progress($course_id){
        $transaction =  DB::transaction(function() use($course_id) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            $progress = DB::table('user_progress')
                ->where([
                    ['course_id', '=', $course_id],
                    ['user_id', '=', $user->id],
                ])
                ->get();


            return $progress;
        });
        return $transaction;

    }

    public function user_progress($user_id){
        $transaction =  DB::transaction(function() use($user_id) {

            $progress = DB::table('courses')
                ->join('user_progress', 'user_progress.course_id', 'courses.id')
                ->join('majors', 'majors.id', 'courses.majors_id')
                ->where([['user_progress.user_id', '=', $user_id],])
                ->groupBy('user_progress.course_id')
                ->select('courses.*', DB::raw("count(*) as progress"), 'majors.name as major_name')
                ->get();

            $i = 0;
            foreach ($progress as $p) {
                $course1 = DB::table('courses')
                    ->join('text_lectures', 'text_lectures.courses_id', 'courses.id')
                    ->where('courses.id', '=', $p->id)
                    ->get();

                $course2 = DB::table('courses')
                    ->join('video_lectures', 'video_lectures.courses_id', 'courses.id')
                    ->where('courses.id', '=', $p->id)
                    ->get();

                $progress[$i]->lectures_count = $course1->count() + $course2->count();
                $i++;
            }


            return $this->ApiResponse($progress, null, 200);
        });
        return $transaction;

    }

}
