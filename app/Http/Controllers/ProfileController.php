<?php
/**
 * Created by PhpStorm.
 * User: Usama
 * Date: 18/09/14
 * Time: 6:56 PM
 */

namespace App\Http\Controllers;
use App\Models\Cities;
use App\Models\Colleges;
use App\Models\Countries;
use App\Models\Lecture_text_questions;
use App\Models\Lecture_video_questions;
use App\Models\Majors;
use App\Models\Text_lecture;
use App\Models\Universities;
use App\Models\User;
use App\Models\Video_lecture;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Illuminate\Support\Facades\Hash;
use Request;
use Validator;
use DB;

class ProfileController extends Controller
{
    protected function index($user_id){
        $query = User::with('countries:id,name','cities:id,name','universities:id,name','colleges:id,name','majors:id,name')
                ->where('users.id','=',$user_id)
                ->get();

        $all = new \stdClass();
        $all->profile = $query;
        $all->privacy = null;

        return $this->ApiResponse($all,null,200);
    }

    public function statistics($user_id){

        $statistics = User::with(['universities' => function($query){
            $query->select('id','name');
        }, 'colleges' => function($query){
            $query->select('id','name');
        },'majors' => function($query){
            $query->select('id','name');
        }])
            ->where('id','=',$user_id)
            ->get();

        $all = new \stdClass();
        $all->statistics = $statistics;
        $all->my_summaries = $this->my_summaries($user_id);
        $all->my_bookmarks = $this->my_bookmarks($user_id);
        $all->my_questions = $this->my_questions($user_id);
        $all->my_answers = $this->my_answers($user_id);
        $all->my_badges = $this->my_badges($user_id);

        return $this->ApiResponse($all, null, 200);
    }

    public function badges($user_id){
        $all_badges = $this->all_badges();
        $my_badges = $this->my_badges($user_id);

        if($my_badges->count() > 0) {
            $i = 0;
            foreach ($all_badges as $all) {
                foreach ($my_badges as $my) {
                    if ($all->id == $my->badge_id) {
                        $all_badges[$i]->earned = true;
                        break;
                    }
                }
                $i++;
            }
        }

        $all = new \stdClass();
        $bronze = array();
        $silver = array();
        $gold = array();

        $j = 0;
        foreach ($all_badges as $badge){
            if(!property_exists($badge,'earned'))
                $all_badges[$j]->earned = false;

            if($badge->type == "bronze")
                array_push($bronze,$badge);
            else if($badge->type == "silver")
                array_push($silver,$badge);
            else
                array_push($gold,$badge);

            $j++;
        }
        $all->bronze = $bronze;
        $all->silver = $silver;
        $all->gold = $gold;

        return $this->ApiResponse($all, null, 200);
    }

    public function password_reset(\Illuminate\Http\Request $request){

        if($request->post('password')!=$request->post('confirmed_password')){
            $errors = [
                'status' => "error",
                'message' => "Password doesn't match"
            ];
            return $this->ApiResponse(null, $errors, 422);
        }

        $data['password'] = bcrypt($request['password']);
        $id = User::Where('email', '=', $request->post('email'))->update(['password' => $data['password']]);
        if($id)
            return $this->ApiResponse($data, null, 200);

        else{
            $errors = "Didn't update the row";
            return $this->ApiResponse(null, $errors, 17);
         }
    }

    public function change_password(\Illuminate\Http\Request $request){

        $user = User::where('remember_token','=',Request::header('token'))->get();

            if($request->post('password') != $request->post('confirmed_password')){
                $errors = "Password doesn't match";
                return $this->ApiResponse(null, $errors, 422);
            }
            if($request->_method)
                $data = $request->except(['token', '_method']);
            else
                $data = $request->except(['token']);

            $data['password'] = bcrypt($data['password']);
            $id = User::Where('id', '=', $user[0]->id)->update(['password' => $data['password']]);
            if($id)
                return $this->ApiResponse($data, null, 200);
            else{
                $errors = "Didn't update the row";
                return $this->ApiResponse(null, $errors, 17);
            }
    }

    protected function edit_profile(\Illuminate\Http\Request $request)
    {
        $user = User::where('remember_token','=',Request::header('token'))->get();

        if($request->_method)
                $data = $request->except(['token', '_method']);
            else
                $data = $request->except(['token']);

//            $data['password'] = bcrypt($data['password']);
            $id = User::Where('id', '=', $user[0]->id)->update($data);
            if($id)
                return $this->ApiResponse($data, null, 200);
            else{
                $errors = "Didn't update the row";
                return $this->ApiResponse(null, $errors, 17);
            }
    }

    public function countries($country_name){
        $query = Countries::where('name','like',$country_name."%")->get();
        return $this->ApiResponse($query, null, 200);

    }

    public function cities($country_id,$city_name){
        $query = Cities::where(
            [
                ['country_id','=',$country_id],
                ['name','like',$city_name."%"]
            ])
            ->get();

        return $this->ApiResponse($query, null, 200);
    }

    public function universities($university_name){
            $query = Universities::where('name','like',$university_name."%")->get();

        return $this->ApiResponse($query, null, 200);

    }
    public function colleges($university_id,$college_name){
        $query = Colleges::where(
            [
                ['universities_id','=',$university_id],
                ['name','like',$college_name."%"]
            ])
            ->get();

        return $this->ApiResponse($query, null, 200);
    }

    public function majors($user_year,$university_id,$college_id,$major_name){
        $query = Majors::where(
            [
                ['universities_id','=',$university_id],
                ['colleges_id','=',$college_id],
                ['name','like',$major_name."%"]
            ])
            ->get();
        $all =  array();

        foreach ($query as $q){
            $a = (explode(',',$q->years));
            if(in_array($user_year,$a))
                array_push($all,$q);
        }

        return $this->ApiResponse($all, null, 200);
    }

    private function my_summaries($user_id)
    {
        $transaction =  DB::transaction(function() use($user_id) {
            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            $text_summeries = Text_lecture::with(['lecture_text_summaries.user', 'lecture_text_summaries' => function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            }, 'lecture_text_summaries.vote' => function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            }])
                ->get();


            $video_summeries = Video_lecture::with(['lecture_video_summaries.user', 'lecture_video_summaries' => function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            }, 'lecture_video_summaries.vote' => function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            }])
                ->get();


            $all = array();
            foreach ($text_summeries as $ob)
                array_push($all, $ob);
            foreach ($video_summeries as $ob)
                array_push($all, $ob);

            usort($all, function ($a, $b) {
                return $a->number - $b->number;
            });


            return $all;
        });
        return $transaction;

    }

    private function my_bookmarks($user_id)
    {
        $transaction =  DB::transaction(function() use($user_id) {
            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            $text_summeries = Text_lecture::with(['text_sections', 'text_sections.lecture_text_bookmarks.user', 'text_sections.lecture_text_bookmarks' => function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            }, 'text_sections.lecture_text_bookmarks.vote' => function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            }])
                ->get();

            $video_summeries = Video_lecture::with(['video_sections', 'video_sections.lecture_video_bookmarks.user', 'video_sections.lecture_video_bookmarks' => function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            }, 'video_sections.lecture_video_bookmarks.vote' => function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            }])
                ->get();

            $all = array();
            foreach ($text_summeries as $ob)
                array_push($all, $ob);
            foreach ($video_summeries as $ob)
                array_push($all, $ob);

            usort($all, function ($a, $b) {
                return $a->number - $b->number;
            });

            return $all;
        });
        return $transaction;

    }

    private function my_questions($user_id)
    {
        $transaction =  DB::transaction(function() use($user_id) {
            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();
            $text_lectures_questions = Lecture_text_questions::with(['text_sections.text_lecture',
                'user' => function ($query) use ($user) {
                    $query->where('id', '=', $user->id);
                }, 'vote' => function ($query) use ($user) {
                    $query->where('user_id', '=', $user->id);
                }])
                ->orderBy('updated_at', 'desc')
                ->get();

            $video_lectures_questions = Lecture_video_questions::with(['video_sections.video_lecture',
                'user' => function ($query) use ($user) {
                    $query->where('id', '=', $user->id);
                }, 'vote' => function ($query) use ($user) {
                    $query->where('user_id', '=', $user->id);
                }])
                ->orderBy('updated_at', 'desc')
                ->get();


            $all = array();
            foreach ($text_lectures_questions as $ob)
                array_push($all, $ob);
            foreach ($video_lectures_questions as $ob)
                array_push($all, $ob);

            usort($all, function ($a, $b) {
                $a = new \DateTime($a->updated_at);
                $b = new \DateTime($b->updated_at);
                return strtotime($b->format('y-m-d')) - strtotime($a->format('y-m-d'));
            });

            return $all;
        });
        return $transaction;

    }

    private function my_answers($user_id)
    {
        $transaction =  DB::transaction(function() use($user_id) {

            $text_answers = DB::table('lecture_text_answers')
                ->where('user_id', '=', $user_id)
                ->get();

            $video_answers = DB::table('lecture_video_answers')
                ->where('user_id', '=', $user_id)
                ->get();

            $all = array();
            foreach ($text_answers as $ob)
                array_push($all, $ob);
            foreach ($video_answers as $ob)
                array_push($all, $ob);

            usort($all, function ($a, $b) {
                $a = new \DateTime($a->updated_at);
                $b = new \DateTime($b->updated_at);
                return strtotime($b->format('y-m-d')) - strtotime($a->format('y-m-d'));
            });

            return $all;
        });
        return $transaction;
    }

    private function my_badges($user_id){
        $badges = DB::table('badge_user')
            ->join('badges','badges.id','badge_user.badge_id')
            ->where([['badge_user.user_id','=',$user_id]])
            ->get();

        return $badges;

    }

    private function all_badges(){
        $badges = DB::table('badges')
            ->get();

        return $badges;
    }

}