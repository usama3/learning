<?php
/**
 * Created by PhpStorm.
 * User: Usama
 * Date: 18/09/14
 * Time: 6:56 PM
 */

namespace App\Http\Controllers;
use App\Models\Cities;
use App\Models\Colleges;
use App\Models\Countries;
use App\Models\Course;
use App\Models\Majors;
use App\Models\Text_lecture;
use App\Models\Universities;
use App\Models\User;
use App\Models\Video_lecture;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Request;
use Validator;
use DB;

class LectureController extends Controller
{

    protected function index($course_id,$type,$lecture_id){

        $transaction =  DB::transaction(function() use($course_id,$type,$lecture_id) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            $user_role = User::with('roles')
                ->where('users.id', '=', $user->id)
                ->get();

            $admin = false;
            foreach ($user_role[0]->roles as $role)
                if ($role->name == 'Administrator')
                    $admin = true;

            $progress = app('App\Http\Controllers\UserProgressController')->user_course_progress($course_id);

            $limitations = User::with('offers.limitations')
                ->where('users.id', '=', $user->id)
                ->get();

            $limits = [];
            if (!$admin)
                if (!empty($limitations[0]->offers[0]))
                    foreach ($limitations[0]->offers[0]->limitations as $limt)
                        $limits[$limt->name] = true;
                else
                    $limits = [];
            else
                $limits = 'Admin';


            $all = new \stdClass();
            $all->list_lecture = $this->lectures($course_id)->original['data'];
            $all->lecture = null;
            $all->next = null;
            $found = false;
            $i = 0;
            foreach($all->list_lecture as $obj)
            {
                if($type == "text"){
                if ($obj->id == $lecture_id && isset($obj->text_sections))
                {
                    $all->lecture = $obj;
                    $found = true;
                }
             }
                else if($type === "video"){
                if ($obj->id == $lecture_id && isset($obj->video_sections))
                { 
                    $all->lecture = $obj;
                    $found = true;
                }
             }
                 $i++;
                 if($found && isset($all->list_lecture[$i])){
                          $all->next = $all->list_lecture[$i];
                          break;
                 }

            }
            
            $all->progress = $progress;
            $all->limitations = $limits;


            return $this->ApiResponse($all, null, 200);
        });
        return $transaction;
    }

    public function lectures($course_id)
    {
        $transaction =  DB::transaction(function() use($course_id) {

            $text_lectures = Text_lecture::with(['text_sections' => function ($query) {
                $query->orderBy('section_number');
            }])
                ->where('courses_id', '=', $course_id)
                ->get();

            $video_lectures = Video_lecture::with(['video_sections' => function ($query) {
                $query->orderBy('section_number');
            }])
                ->where('courses_id', '=', $course_id)
                ->get();

            $all = array();
            foreach ($text_lectures as $ob)
                array_push($all, $ob);
            foreach ($video_lectures as $ob)
                array_push($all, $ob);

            usort($all, function ($a, $b) {
                return $a->number - $b->number;
            });

            return $this->ApiResponse($all, null, 200);
        });
        return $transaction;

    }


}