<?php

namespace App\Http\Controllers;

use App\Models\Badge;
use App\Models\Colleges;
use App\Models\Course;
use App\Models\Lecture_text_answers;
use App\Models\Lecture_text_questions;
use App\Models\Lecture_text_summaries;
use App\Models\Lecture_video_answers;
use App\Models\Lecture_video_questions;
use App\Models\Lecture_video_summaries;
use App\Events\Notifications;
use App\Models\Text_lecture;
use App\Models\Text_section;
use App\Models\Universities;
use App\Models\Video_lecture;
use App\Models\Video_section;
use Encore\Admin\Facades\Admin;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Validator;
use DB;
use Request;


class NotificationsController extends Controller
{

    public function GetUserNotifications(){
        $transaction =  DB::transaction(function() {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            $notifications = DB::table('notification_users')
                ->join('notifications', 'notifications.id', 'notification_users.notifications_id')
                ->where('notification_users.user_id', '=', $user->id)
                ->select('notifications.*', 'notification_users.is_seen')
                ->take(50)
                ->latest()
                ->get();

            return $this->ApiResponse($notifications, null, 200);
        });
        return $transaction;

    }
    public function seen(\Illuminate\Http\Request $request){
        $transaction =  DB::transaction(function() use($request) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            $id = DB::table('notification_users')
                ->where([
                    ['notifications_id', '=', $request->notification_id],
                    ['user_id', '=', $user->id],
                ])
                ->update([
                    'is_seen' => 1,
                    'is_seen_date' => now(),
                ]);

            if (empty($id)) {
                $errors = 'Failed to update row in database.';
                return $this->ApiResponse(null, $errors, 250);
            } else
                return $this->ApiResponse($request->all(), null, 200);
        });
        return $transaction;
    }

    public function NewQuestion($question_id,$question_type){
        $transaction =  DB::transaction(function() use($question_id,$question_type) {

            if ($question_type == 'text') {
                $notification_type = 'text_question';
                $course_id = Lecture_text_questions::where('id', '=', $question_id)->first()->course_id;
            }
            else {
                $notification_type = 'video_question';
                $course_id = Lecture_video_questions::where('id', '=', $question_id)->first()->course_id;
            }


            $to_users_ids = $this->UsersOfCourse($course_id);
            $course = Course::find($course_id)->first();

            $template = DB::table('notification_type')->where('name', '=', "Question")->first();
            $text = $this->format_msg($template->text, array('$course_name'),
                array($course->name), true);

            return $this->insert_notification(1, $to_users_ids, $template->title, $text, $course->picture, $notification_type, $question_id, $course_id);
        });
        return $transaction;
    }

    public function QuestionBounty($question_id,$question_type){
        $transaction =  DB::transaction(function() use($question_id,$question_type) {

            if ($question_type == 'text') {
                $notification_type = 'text_question_bounty';
                $question = Lecture_text_questions::where('id', '=', $question_id)->first();
                $course_id = $question->course_id;
            } else {
                $notification_type = 'video_question_bounty';
                $question = Lecture_video_questions::where('id', '=', $question_id)->first();
                $course_id = $question->course_id;
            }

            $user = User::find($question->user_id);
            $to_users_ids = $this->UsersOfCourse($course_id);
            $course = Course::find($course_id)->first();

            $template = DB::table('notification_type')->where('name', '=', "Question bounty")->first();
            $text = $this->format_msg($template->text, array('$user_name'),
                array($user->username), true);

            return $this->insert_notification(1, $to_users_ids, $template->title, $text, $course->picture, $notification_type, $question_id, $course_id);
        });
        return $transaction;
    }


    public function Badge($user_id,$badge_id){
        $transaction =  DB::transaction(function() use($user_id,$badge_id) {

            $notification_type = 'Badge';
            $badge = Badge::find((int)$badge_id);

            $template = DB::table('notification_type')->where('name', '=', $notification_type)->first();
            $text = $this->format_msg($template->text, array('$badge_name'),
                array($badge->name), true);

            $to_users_ids = array((int)$user_id);

            return $this->insert_notification($user_id, $to_users_ids, $template->title, $text, $badge->earned_picture, $notification_type, $user_id, 0);
        });
        return $transaction;
    }

    public function AnswerMarked($marked_user_id,$answer_user_id,$answer_id,$course_id,$Answer_type){
        $transaction =  DB::transaction(function() use($marked_user_id,$answer_user_id,$answer_id,$course_id,$Answer_type) {

            if ($Answer_type == 'text')
                $notification_type = 'text_answer_marked';
            else
                $notification_type = 'video_answer_marked';

            $user = User::find((int)$marked_user_id);
            $template = DB::table('notification_type')->where('name', '=', 'Answer marked')->first();
            $text = $this->format_msg($template->text, array('$user_name'),
                array($user->username), true);

            $to_users_ids = array((int)$answer_user_id);

            if (empty($user)) {
                $errors = 'No user have this id!';
                return $this->ApiResponse(null, $errors, 17);
            }

            return $this->insert_notification($marked_user_id, $to_users_ids, $template->title, $text, $user->picture, $notification_type, $answer_id, $course_id);
        });
        return $transaction;
    }

    public function QuestionAnswered($answered_by_user_id, $question_id, $question_type,$course_id){
        $transaction =  DB::transaction(function() use($answered_by_user_id, $question_id, $question_type,$course_id) {

            $answerd_by_user = User::find((int)$answered_by_user_id);
            $question_and_answers_users_id = array();

            if ($question_type == 'text') {
                $notification_type = 'text_question_answered';
                $question_user_id = Lecture_text_questions::where('id', '=', $question_id)->first()->user_id;
                $answers_users_ids = Lecture_text_answers::where('lecture_text_questions_id', '=', $question_id)
                    ->select('user_id')->get();
                array_push($question_and_answers_users_id, $question_user_id);
                foreach ($answers_users_ids as $ans)
                    array_push($question_and_answers_users_id, $ans->user_id);
            } else {
                $notification_type = 'video_question_answered';
                $question_user_id = Lecture_video_questions::where('id', '=', $question_id)->first()->user_id;
                $answers_users_ids = Lecture_video_answers::where('lecture_video_questions_id', '=', $question_id)
                    ->select('user_id')->get();
                array_push($question_and_answers_users_id, $question_user_id);
                foreach ($answers_users_ids as $ans)
                    array_push($question_and_answers_users_id, $ans->user_id);
            }

            $question_and_answers_users_id = array_unique($question_and_answers_users_id);

            if (($key = array_search($answered_by_user_id, $question_and_answers_users_id)) !== false)
                unset($question_and_answers_users_id[$key]);

            $question_and_answers_users_id = array_values($question_and_answers_users_id);

            $template = DB::table('notification_type')->where('name', '=', 'Question answered')->first();
            $text = $this->format_msg($template->text, array('$user_name'),
                array($answerd_by_user->username), true);
            return $this->insert_notification($answered_by_user_id, $question_and_answers_users_id, $template->title, $text, $answerd_by_user->picture, $notification_type, $question_id, $course_id);
        });
        return $transaction;
    }

    public function QuestionVote($vote_user_id,$question_user_id,$vote_type,$event_id,$course_id,$question_type){
        if ($question_type == 'text')
            $notification_type = 'text_question_vote';
        else
            $notification_type = 'video_question_vote';
        return $this->vote_noti($vote_user_id,$question_user_id,$vote_type.'voted',$notification_type,$event_id,$course_id);
    }

    public function AnswerVote($vote_user_id,$answer_user_id,$vote_type,$event_id,$course_id,$answer_type){
        if ($answer_type == 'text')
            $notification_type = 'text_answer_vote';
        else
            $notification_type = 'video_answer_vote';
        return $this->vote_noti($vote_user_id,$answer_user_id,$vote_type.'voted',$notification_type,$event_id,$course_id);
    }

    public function SummaryVote($vote_user_id,$summary_user_id,$vote_type,$event_id,$course_id,$summary_type){
        if ($summary_type == 'text')
            $notification_type = 'text_summary_vote';
        else
            $notification_type = 'video_summary_vote';
        return $this->vote_noti($vote_user_id,$summary_user_id,$vote_type.'voted',$notification_type,$event_id,$course_id);
    }

    public function BookmarkVote($vote_user_id,$bookmark_user_id,$vote_type,$event_id,$course_id,$bookmark_type){
        if ($bookmark_type == 'text')
            $notification_type = 'text_bookmark_vote';
        else
            $notification_type = 'video_bookmark_vote';
        return $this->vote_noti($vote_user_id,$bookmark_user_id,$vote_type.'voted',$notification_type,$event_id,$course_id);
    }

    private function vote_noti($vote_user_id,$owner_user_id,$vote_type,$notification_type,$event_id,$course_id){
        $transaction =  DB::transaction(function() use($vote_user_id,$owner_user_id,$vote_type,$notification_type,$event_id,$course_id) {
            if($notification_type == 'text_question_vote' || $notification_type == 'video_question_vote')
                $noti_type = "Question Vote";
            if($notification_type == 'text_answer_vote' || $notification_type == 'video_answer_vote')
                $noti_type = "Answer vote";
            if($notification_type == 'text_summary_vote' || $notification_type == 'video_summary_vote')
                $noti_type = "Summary vote";
            if($notification_type == 'text_bookmark_vote' || $notification_type == 'video_bookmark_vote')
                $noti_type = "Bookmark vote";

            $user = User::find((int)$vote_user_id);
            $template = DB::table('notification_type')->where('name', '=', $noti_type)->first();
            $text = $this->format_msg($template->text, array('$user_name', '$vote_type'),
                array($user->username, $vote_type), true);


            $to_users_ids = array((int)$owner_user_id);

            if (empty($user)) {
                $errors = 'No user have this id!';
                return $this->ApiResponse(null, $errors, 17);
            }

            return $this->insert_notification($vote_user_id, $to_users_ids, $template->title, $text, $user->picture, $notification_type, $event_id, $course_id);
        });
        return $transaction;
    }

    private function insert_notification($by_user_id,$to_users_ids,$title,$text,$picture,$type,$event_id,$course_id)
    {
        $transaction =  DB::transaction(function() use($by_user_id,$to_users_ids,$title,$text,$picture,$type,$event_id,$course_id) {

            $id = DB::table('notifications')->insertGetId([
                'user_id' => $by_user_id,
                'title' => $title,
                'text' => $text,
                'picture' => $picture,
                'type' => $type,
                'event_id' => $event_id,
                'course_id' => $course_id,
            ]);

            foreach ($to_users_ids as $user)
                DB::table('notification_users')->insertGetId([
                    'notifications_id' => $id,
                    'user_id' => $user,
                    'is_seen' => 0
                ]);

            if (empty($id)) {
                $errors = 'Failed to insert row in database.';
                return $this->ApiResponse(null, $errors, 17);
            } else {
                $data = [
                    'by_user_id' => $by_user_id,
                    'to_users_ids' => $to_users_ids,
                    'title' => $title,
                    'text' => $text,
                    'picture' => $picture,
                    'type' => $type,
                    'event_id' => $event_id,

                ];

                $notifications = DB::table('notification_users')
                    ->join('notifications', 'notifications.id', 'notification_users.notifications_id')
                    ->where('notification_users.notifications_id', '=', $id)
                    ->select('notifications.*', 'notification_users.is_seen')
                    ->first();

                foreach ($to_users_ids as $id) {
                    $user = User::find($id);
                    event(new Notifications($user, $notifications));
                }

                return $this->ApiResponse($data, null, 200);
            }
        });
        return $transaction;

    }

    private function format_msg($msg, $variabel, $value, $multiple = false) {
        if(!$multiple){
            if (strpos($msg, $variabel) != false) {
                $msg=str_replace($variabel, $value, $msg);
            }
            return $msg;
        }
        else{
            if(sizeof($variabel)==sizeof($value)){
                for ($i=0; $i < sizeof($variabel); $i++) {
                    if(strpos($msg, $variabel[$i]) != false)
                        $msg=str_replace($variabel[$i], $value[$i], $msg);
                }
                return $msg;
            }
            else
                return false;
        }
    }

    private function UsersOfCourse($course_id){
        $course = Course::find($course_id);
        $users = User::where([
            ['universities_id', '=', $course->universities_id],
            ['colleges_id', '=', $course->colleges_id ],
            ['majors_id', '=', $course->majors_id],
        ])
            ->select('id')
            ->get();

        $recipeant_users = array();
        foreach ($users as $user)
            array_push($recipeant_users,$user->id);

        return $recipeant_users;

    }



}
