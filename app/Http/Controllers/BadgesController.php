<?php

namespace App\Http\Controllers;
use App\Models\Badge;
use App\Models\Lecture_text_answers;
use App\Models\Lecture_text_questions;
use App\Models\Lecture_video_answers;
use App\Models\Lecture_video_questions;
use Carbon\Carbon;
use Request;
use Validator;
use DB;

class BadgesController extends Controller
{
    /////////////////////////// Bronze  ///////////////////////////

    //first up vote (vote)
    public function Bronze_Supporter($user_id){
        $badge_id = $this->check_existing_badge($user_id,'Supporter');
        if($badge_id != 0)
        $this->Badge($user_id,'Supporter',$badge_id);
    }
    //Answer a question with score of 1 or more
    public function Bronze_Teacher($user_id,$event_id,$table_name){
        $badge_id = $this->check_existing_badge($user_id,'Teacher');
        if($badge_id != 0) {
            $owner_user = DB::table($table_name)
                ->where('id', '=', $event_id)
                ->first();
            if (!empty($owner_user))
                $this->Badge($owner_user->user_id, 'Teacher',$badge_id);
        }
    }
    //First bounty you offer on your own question
    public function Bronze_Promoter($user_id){
        $badge_id = $this->check_existing_badge($user_id,'Promoter');
        if($badge_id != 0)
        $this->Badge($user_id,'Promoter',$badge_id);
    }
    //First bounty you manually award on your own question
    public function Bronze_Benefactor($user_id){
        $badge_id = $this->check_existing_badge($user_id,'Benefactor');
        if($badge_id != 0)
        $this->Badge($user_id,'Benefactor',$badge_id);
    }
    //first down vote
    public function Bronze_Critic($user_id){
        $badge_id = $this->check_existing_badge($user_id,'Critic');
        if($badge_id != 0)
        $this->Badge($user_id,'Critic',$badge_id);
    }
    //earn at least 200 reputation in a single day
    public function Bronze_Mortarboard($user_id){
        $badge_id = $this->check_existing_badge($user_id,'Refiner');
        if($badge_id != 0){

            $now = Carbon::now('America/Boise')->subDay()->toDateString();
            $check = DB::table('user_points')
                ->where([
                    ['user_id','=', $user_id],
                    ['date','=', $now],
                    ['points','>=',200]
                ])
                ->get();

            if($check->count() >= 0)
                 $this->Badge($user_id, 'Mortarboard', $badge_id);


        }

    }
    //Ask a question and accept an answer.
    public function Bronze_Rare($user_id){
        $badge_id = $this->check_existing_badge($user_id,'Rare');
        if($badge_id != 0)
        $this->Badge($user_id,'Rare',$badge_id);
    }
    //Answer you own question with 3 or more up vote.
    public function Bronze_Self_Learner($user_id,$event_id,$table_name){

        $transaction =  DB::transaction(function() use($user_id,$event_id,$table_name) {
                $badge_id = $this->check_existing_badge($user_id, 'Self-Learner');
                if ($badge_id != 0) {
                    $owner_user = DB::table($table_name)
                        ->where('id', '=', $event_id)
                        ->first();
//                    var_dump($owner_user);die();

//                    var_dump($owner_user);die();
                    if ($owner_user->id == $user_id) {

                        $votes1 = DB::table('vote_text_answers')
                            ->where([
                                ['lecture_text_answers_id', '=', $event_id],
                                ['vote_type', '=', 'up'],
                            ])
                            ->get();

                        $votes2 = DB::table('vote_video_answers')
                            ->where([
                                ['lecture_video_answers_id', '=', $event_id],
                                ['vote_type', '=', 'up'],
                            ])
                            ->get();
                        if (!empty($votes1))
                            if ($votes1->count() >= 3)
                                $this->Badge($user_id, 'Self-Learner', $badge_id);
                        if (!empty($votes2))
                            if ($votes2->count() >= 3)
                                $this->Badge($user_id, 'Self-Learner', $badge_id);
                    }

                }
            });
    }
    //first question with score of 1 or more.
    public function Bronze_Common($user_id,$event_id,$table_name){
        $question = DB::table($table_name)
            ->where('id','=',$event_id)
            ->first();


        $owner_id = $question->user_id;
        $badge_id = $this->check_existing_badge($owner_id,'Common');

        if($badge_id != 0) {
            if (!empty($question))
                $this->Badge($owner_id, 'Common',$badge_id);
        }

    }

    /////////////////////////// Silver  ///////////////////////////

    //visit the site for 30 days.
    public function Silver_Enthusiast($user_id){

        $badge_id = $this->check_existing_badge($user_id,'Enthusiast');

        $check = DB::table('user_visits')->where([
            ['user_id','=', $user_id],
        ])
            ->get();
        if($check->count() >= 30)
            $this->Badge($user_id,'Enthusiast',$badge_id);


    }
    //earn at least 17 reputation in a single day
    public function Silver_Amazing($user_id){
        $badge_id = $this->check_existing_badge($user_id,'Refiner');
        if($badge_id != 0){

            $now = Carbon::now('America/Boise')->subDay()->toDateString();
            $check = DB::table('user_points')
                ->where([
                    ['user_id','=', $user_id],
                    ['date','=', $now],
                    ['points','>=',17]
                ])
                ->get();

            if($check->count() >= 0)
                $this->Badge($user_id, 'Amazing', $badge_id);


        }

    }

    //Answer 50 question
    public function Silver_Refiner($user_id){
        $badge_id = $this->check_existing_badge($user_id,'Refiner');
        if($badge_id != 0)
        $this->Badge($user_id,'Refiner',$badge_id);

    }
    //Up vote answer on a question where an answer of you has a positive score.
    public function Silver_Sportsmanship($user_id,$event_id,$table_name,$vote_type){

        $transaction =  DB::transaction(function() use($user_id,$event_id,$table_name,$vote_type) {

            $badge_id = $this->check_existing_badge($user_id, 'Sportsmanship');

            if ($badge_id != 0) {
                $owner_user = DB::table($table_name)
                    ->where('id', '=', $event_id)
                    ->first();

                if($table_name == lecture_text_answers) {
                    $check = DB::table('lecture_text_answers')
                        ->where([
                            ['lecture_text_questions_id', '=', $owner_user->lecture_text_questions_id],
                            ['user_id', '=', $user_id],
                            ['up_vote', '>=', 0],

                        ])
                        ->first();
                }
                else{
                    $check = DB::table('lecture_video_answers')
                        ->where([
                            ['lecture_text_questions_id', '=', $owner_user->lecture_video_questions_id],
                            ['user_id', '=', $user_id],
                            ['up_vote', '>=', 0],

                        ])
                        ->first();
                }


                if (!empty($check) && $vote_type == 'up')
                    $this->Badge($user_id, 'Sportsmanship', $badge_id);
            }
        });
    }


    /////////////////////////// Gold  ///////////////////////////

    // Visit the site 100 times in 100 days.
    public function Gold_Fanatic($user_id){

        $badge_id = $this->check_existing_badge($user_id,'Fanatic');
        if($badge_id != 0) {
            $check = DB::table('user_visits')->where([
                ['user_id', '=', $user_id],
            ])
                ->get();
            if ($check->count() >= 100)
                $this->Badge($user_id, 'Fanatic', $badge_id);
        }


    }
    //Earn 200 daily reputation 150 times
    public function Gold_Legendary($user_id){

        $badge_id = $this->check_existing_badge($user_id,'Refiner');
        if($badge_id != 0){

            $now = Carbon::now('America/Boise')->subDay()->toDateString();
            $check = DB::table('user_points')
                ->where([
                    ['user_id','=', $user_id],
                    ['date','=', $now],
                    ['points','>=', 200],
                ])
                ->get();

            if($check->count() >= 150)
                $this->Badge($user_id, 'Legendary', $badge_id);


        }


    }
    //Vote on 600 questions or answers
    public function Gold_Electorate($user_id){
        $transaction =  DB::transaction(function() use($user_id) {

            $badge_id = $this->check_existing_badge($user_id, 'Electorate');
            if ($badge_id != 0) {

                $c1 = DB::table('vote_text_questions')
                    ->where([
                        ['user_id', '=', $user_id],
                    ])
                    ->select(DB::raw('COUNT(*) as count'))
                    ->get();

                $c2 = DB::table('vote_video_questions')
                    ->where([
                        ['user_id', '=', $user_id],
                    ])
                    ->select(DB::raw('COUNT(*) as count'))
                    ->get();

                $c3 = DB::table('vote_text_answers')
                    ->where([
                        ['user_id', '=', $user_id],
                    ])
                    ->select(DB::raw('COUNT(*) as count'))
                    ->get();

                $c4 = DB::table('vote_video_answers')
                    ->where([
                        ['user_id', '=', $user_id],
                    ])
                    ->select(DB::raw('COUNT(*) as count'))
                    ->get();

                if ($c1[0]->count + $c1[0]->count + $c1[0]->count + $c1[0]->count == 600)
                    $this->Badge($user_id, 'Electorate', $badge_id);
            }
        });

    }

    /////////////////////////////////////////////////////////////////
    private function check_existing_badge($user_id, $badge_name){
        $badge_id = Badge::where('name','=',$badge_name)->first()->id;

        $check = DB::table('badge_user')
            ->where([
                ['badge_user.user_id','=',$user_id],
                ['badge_user.badge_id','=',$badge_id],
            ])
            ->get();

        if($check->count() == 0)
            return $badge_id;

        return 0;
    }

    private function Badge($user_id,$badge_name,$badge_id){
        $transaction =  DB::transaction(function() use($user_id,$badge_name,$badge_id) {

            $badge_points = Badge::where('id', '=', $badge_id)->first()->points;
            $this->add_points_and_date($user_id, $badge_points);
//        DB::table('vote')->where('user_id', '=', $user_id)->get();
            DB::table('badge_user')->insertGetId([
                'user_id' => $user_id,
                'badge_id' => $badge_id,
            ]);

            DB::table('users')
                ->where([
                    ['id', '=', $user_id],
                ])
                ->increment('points', $badge_points);

            app('App\Http\Controllers\BadgesController')->Bronze_Mortarboard($user_id);
            app('App\Http\Controllers\BadgesController')->Silver_Amazing($user_id);
            app('App\Http\Controllers\BadgesController')->Gold_Legendary($user_id);

            // Notification
            app('App\Http\Controllers\NotificationsController')->Badge($user_id, $badge_id);
        });

    }
    public function add_points_and_date($user_id,$points){

        $now = Carbon::now('America/Boise')->subDay()->toDateString();
        $check = DB::table('user_points')
            ->where([
                ['user_id','=', $user_id],
                ['date','=', $now],
            ])
        ->get();

        if($check->count() == 0) {

            DB::table('user_points')->insert([
                'user_id' => $user_id,
                'points' => $points,
                'date' => $now,
            ]);

        }
        else{
            $id = DB::table('user_points')
                ->where([
                    ['user_id', '=', $user_id],
                    ['date', '=', $now],
                ])
                ->increment('points', $points);

        }

    }


}
