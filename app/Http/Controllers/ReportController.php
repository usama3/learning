<?php

namespace App\Http\Controllers;

use App\Models\Notifications;
use App\Models\User;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Support\Facades\Broadcast;
use Validator;
use DB;
use Request;



class ReportController extends Controller
{

    public function report(\Illuminate\Http\Request $request)
    {
        $transaction =  DB::transaction(function() use($request) {

            $url = url('') . '/' . $request->url;
            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            $id = DB::table('reports')->insertGetId([
                'users_id' => $user->id,
                'reports_msg_id' => $request->post('reports_msg_id'),
                'url' => $url
            ]);
            EmailController::SendReportMail($user->id, $request->post('reports_msg_id')
                , $url);

            if (empty($id)) {
                $errors = 'Failed to insert row in database.';
                return $this->ApiResponse(null, $errors, 17);
            } else
                return $this->ApiResponse($request->all(), null, 200);
        });
        return $transaction;

    }

    public function get_report_msg($type){

        $msg = DB::table('reports_msg')
            ->where('type', '=' ,$type)
            ->get();

        if(empty($msg)) {
            $errors = 'Failed to insert row in database.';
            return $this->ApiResponse(null, $errors, 17);
        }
        else
            return $this->ApiResponse($msg, null, 200);

    }


}