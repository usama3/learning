<?php

namespace App\Http\Controllers;
use App\Events\Bookmarks;
use App\Events\QuestionAndAnswers;
use App\Events\Questions;
use App\Events\Summaries;
use App\Models\Lecture_text_answers;
use App\Models\Lecture_text_bookmarks;
use App\Models\Lecture_text_questions;
use App\Models\Lecture_text_summaries;
use App\Models\Lecture_video_answers;
use App\Models\Lecture_video_bookmarks;
use App\Models\Lecture_video_questions;
use App\Models\Lecture_video_summaries;
use App\Models\User;
use Request;
use Symfony\Component\Console\Question\Question;
use Validator;
use DB;


class VoteController extends Controller
{
//    // TOFIX
//    public function check_if_user_voted($user_id,$event_id,$event_table,$event_column_name){
//
//        $check = DB::table($event_table)->where([
//            ['user_id', '=', $user_id],
//            [$event_column_name, '=', $event_id],
//        ])->first();
//
//        if($check)
//        return $check->vote_type;
////
//        return null;
//    }

    // TOFIX
    public function users_who_voted($event_id,$event_table)
    {
        if($event_table == 'vote_text_answers')
            $column = 'lecture_text_answers_id';
        if($event_table == 'vote_video_answers')
            $column = 'lecture_video_answers_id';

        if($event_table == 'vote_text_bookmarks')
            $column = 'lecture_text_bookmarks_id';
        if($event_table == 'vote_video_bookmarks')
            $column = 'lecture_video_bookmarks_id';

        if($event_table == 'vote_text_questions')
            $column = 'lecture_text_questions_id';
        if($event_table == 'vote_video_questions')
            $column = 'lecture_video_questions_id';

        if($event_table == 'vote_text_summaries')
            $column = 'lecture_text_summaries_id';
        if($event_table == 'vote_video_summaries')
            $column = 'lecture_video_summaries_id';

        $users = DB::table($event_table)
            ->join('users','users.id',"$event_table.user_id")
            ->where("$event_table.$column",'=',$event_id)
            ->select('users.*')
            ->get();

        return $this->ApiResponse($users, null, 200);
    }

    public function vote_text_question(\Illuminate\Http\Request $request)
    {
        $transaction =  DB::transaction(function() use($request) {
            $user = User::where('remember_token', '=', Request::header('token'))->first();
            app('App\Http\Controllers\BadgesController')->Bronze_Common($user->id, $request->event_id, 'lecture_text_questions');
            $question_user_id = Lecture_text_questions::find($request->event_id)->user_id;
            app('App\Http\Controllers\NotificationsController')->QuestionVote($user->id, $question_user_id, $request->vote_type, $request->event_id, $request->course_id,'text');
            $vote = $this->vote($request, 'lecture_text_questions', 'vote_text_questions', 'lecture_text_questions_id');
            $question = Lecture_text_questions::with('user')
                ->where('id','=',$request->event_id)->first();

            $socket_data = [
                'type' => 'edit_question',
                'by_user' => $user->id,
                'data' => $question
            ];

            event(new Questions($request->course_id,$socket_data));
            event(new QuestionAndAnswers('text',$request->event_id,$socket_data));

            return $vote;
        });



        return $transaction;

    }
    public function cancel_vote_text_question(\Illuminate\Http\Request $request){
        return $this->cancel_vote('lecture_text_questions',$request->post('event_id'),$request->post('vote_type'),'vote_text_questions','lecture_text_questions_id');
    }

    public function vote_video_question(\Illuminate\Http\Request $request){
        $transaction =  DB::transaction(function() use($request) {
            $user = User::where('remember_token', '=', Request::header('token'))->first();
            app('App\Http\Controllers\BadgesController')->Bronze_Common($user->id, $request->event_id, 'lecture_video_questions');
            $question_user_id = Lecture_video_questions::find($request->event_id)->user_id;
            app('App\Http\Controllers\NotificationsController')->QuestionVote($user->id, $question_user_id, $request->vote_type, $request->event_id, $request->course_id,'video');

            $vote = $this->vote($request, 'lecture_video_questions', 'vote_video_questions', 'lecture_video_questions_id');

            $question = Lecture_video_questions::with('user')
                ->where('id','=',$request->event_id)->first();

            $socket_data = [
                'type' => 'edit_question',
                'by_user' => $user->id,
                'data' => $question
            ];

            event(new Questions($request->course_id,$socket_data));
            event(new QuestionAndAnswers('video',$request->event_id,$socket_data));

            return $vote;
        });
        return $transaction;
    }

    public function cancel_vote_video_question(\Illuminate\Http\Request $request){
        return $this->cancel_vote('lecture_video_questions',$request->post('event_id'),$request->post('vote_type'),'vote_video_questions','lecture_video_questions_id');
    }
    public function vote_text_answer(\Illuminate\Http\Request $request){
        $transaction =  DB::transaction(function() use($request) {
            $user = User::where('remember_token', '=', Request::header('token'))->first();
            app('App\Http\Controllers\BadgesController')->Bronze_Teacher($user->id, $request->event_id, 'lecture_text_answers');

            app('App\Http\Controllers\BadgesController')->Bronze_Self_Learner($user->id, $request->event_id, 'lecture_text_answers');

            app('App\Http\Controllers\BadgesController')->Silver_Sportsmanship($user->id, $request->event_id, 'lecture_text_answers', $request->vote_type);

            $question_user_id = Lecture_text_answers::find($request->event_id)->user_id;
            app('App\Http\Controllers\NotificationsController')->AnswerVote($user->id, $question_user_id, $request->vote_type, $request->event_id, $request->course_id,'text');
            $vote = $this->vote($request, 'lecture_text_answers', 'vote_text_answers', 'lecture_text_answers_id');

            $answer = Lecture_text_answers::with('user')
                ->where('id','=',$request->event_id)->first();

            $socket_data = [
                'type' => 'edit_answer',
                'by_user' => $user->id,
                'data' => $answer
            ];

            event(new QuestionAndAnswers('text',$answer->lecture_text_questions_id,$socket_data));

            return $vote;
        });

        return $transaction;

    }

    public function cancel_vote_text_answer(\Illuminate\Http\Request $request){
        return $this->cancel_vote('lecture_text_answers',$request->post('event_id'),$request->post('vote_type'),'vote_text_answers','lecture_text_answers_id');
    }

    public function vote_video_answer(\Illuminate\Http\Request $request){
        $transaction =  DB::transaction(function() use($request) {

            $user = User::where('remember_token', '=', Request::header('token'))->first();
            app('App\Http\Controllers\BadgesController')->Bronze_Teacher($user->id, $request->event_id, 'lecture_video_answers');

            app('App\Http\Controllers\BadgesController')->Bronze_Self_Learner($user->id, $request->event_id, 'lecture_video_answers');
        app('App\Http\Controllers\BadgesController')->Silver_Sportsmanship($user->id, $request->event_id, 'lecture_video_answers', $request->vote_type);

        $question_user_id = Lecture_video_answers::find($request->event_id)->user_id;
            app('App\Http\Controllers\NotificationsController')->AnswerVote($user->id, $question_user_id, $request->vote_type, $request->event_id, $request->course_id,'video');

            $vote =  $this->vote($request, 'lecture_video_answers', 'vote_video_answers', 'lecture_video_answers_id');
            $answer = Lecture_video_answers::with('user')
                ->where('id','=',$request->event_id)->first();

            $socket_data = [
                'type' => 'edit_answer',
                'by_user' => $user->id,
                'data' => $answer
            ];

            event(new QuestionAndAnswers('video',$answer->lecture_video_questions_id,$socket_data));

            return $vote;
        });

        return $transaction;

    }

    public function cancel_vote_video_answer(\Illuminate\Http\Request $request){
        return $this->cancel_vote('lecture_video_answers',$request->post('event_id'),$request->post('vote_type'),'vote_video_answers','lecture_video_answers_id');
    }

    public function vote_text_summary(\Illuminate\Http\Request $request){
        $transaction =  DB::transaction(function() use($request) {

            $user = User::where('remember_token', '=', Request::header('token'))->first();
            $lecture_text_summaries = Lecture_text_summaries::find($request->event_id);
            $question_user_id = $lecture_text_summaries->user_id;
            app('App\Http\Controllers\NotificationsController')->SummaryVote($user->id, $question_user_id, $request->vote_type, $request->event_id, $request->course_id,'text');

            $vote = $this->vote($request, 'lecture_text_summaries', "vote_text_summaries", 'lecture_text_summaries_id');

            $socket_data = [
                'type' => 'edit_summary',
                'by_user' => $user->id,
                'data' => $lecture_text_summaries
            ];

            event(new Summaries($request->course_id,$socket_data));

            return $vote;
        });
        return $transaction;


    }

    public function cancel_vote_text_summary(\Illuminate\Http\Request $request){
        return $this->cancel_vote('lecture_text_summaries',$request->post('event_id'),$request->post('vote_type'),'vote_text_summaries','lecture_text_summaries_id');
    }
    public function vote_video_summary(\Illuminate\Http\Request $request){
        $transaction =  DB::transaction(function() use($request) {

            $user = User::where('remember_token', '=', Request::header('token'))->first();
            $lecture_video_summaries = Lecture_video_summaries::find($request->event_id);
            $question_user_id = $lecture_video_summaries->user_id;
            app('App\Http\Controllers\NotificationsController')->SummaryVote($user->id, $question_user_id, $request->vote_type, $request->event_id, $request->course_id,'video');

            $vote =  $this->vote($request, 'lecture_video_summaries', 'vote_video_summaries', 'lecture_video_summaries_id');


            $socket_data = [
                'type' => 'edit_summary',
                'by_user' => $user->id,
                'data' => $lecture_video_summaries
            ];

            event(new Summaries($request->course_id,$socket_data));

            return $vote;
        });
        return $transaction;

    }

    public function cancel_vote_video_summary(\Illuminate\Http\Request $request){
        return $this->cancel_vote('lecture_video_summaries',$request->post('event_id'),$request->post('vote_type'),'vote_video_summaries','lecture_video_summaries_id');
    }

    public function vote_text_bookmark(\Illuminate\Http\Request $request){
        $transaction =  DB::transaction(function() use($request) {

            $user = User::where('remember_token', '=', Request::header('token'))->first();
            $lecture_text_bookmarks = Lecture_text_bookmarks::find($request->event_id);
            $question_user_id = $lecture_text_bookmarks->user_id;
            app('App\Http\Controllers\NotificationsController')->BookmarkVote($user->id, $question_user_id, $request->vote_type, $request->event_id, $request->course_id,'text');

            $vote = $this->vote($request, 'lecture_text_bookmarks', 'vote_text_bookmarks', 'lecture_text_bookmarks_id');

            $socket_data = [
                'type' => 'edit_bookmark',
                'by_user' => $user->id,
                'data' => $lecture_text_bookmarks
            ];

            event(new Bookmarks($request->course_id,$socket_data));


            return $vote;
        });
        return $transaction;

    }

    public function cancel_vote_text_bookmark(\Illuminate\Http\Request $request){
        return $this->cancel_vote('lecture_text_bookmarks',$request->post('event_id'),$request->post('vote_type'),'vote_text_bookmarks','lecture_text_bookmarks_id');
    }
    public function vote_video_bookmark(\Illuminate\Http\Request $request){
        $transaction =  DB::transaction(function() use($request) {

            $user = User::where('remember_token', '=', Request::header('token'))->first();

            $lecture_video_bookmarks = Lecture_video_bookmarks::find($request->event_id);
            $question_user_id = $lecture_video_bookmarks->user_id;
//            var_dump($lecture_video_bookmarks);die();
            app('App\Http\Controllers\NotificationsController')->BookmarkVote($user->id, $question_user_id, $request->vote_type, $request->event_id, $request->course_id,'video');
            $vote = $this->vote($request, 'lecture_video_bookmarks', 'vote_video_bookmarks', 'lecture_video_bookmarks_id');
            $socket_data = [
                'type' => 'edit_bookmark',
                'by_user' => $user->id,
                'data' => $lecture_video_bookmarks
            ];

            event(new Bookmarks($request->course_id,$socket_data));


            return $vote;
        });
        return $transaction;


    }

    public function cancel_vote_video_bookmark(\Illuminate\Http\Request $request){
        return $this->cancel_vote('lecture_video_bookmarks',$request->post('event_id'),$request->post('vote_type'),'vote_video_bookmarks','lecture_video_bookmarks_id');
    }


    private function vote(\Illuminate\Http\Request $request,$event_table,$vote_table,$event_column_name)
    {
        $transaction =  DB::transaction(function() use($request,$event_table,$vote_table,$event_column_name) {

            $user = User::where('remember_token', '=', Request::header('token'))->first();
            app('App\Http\Controllers\BadgesController')->Gold_Electorate($user->id);

            $check = DB::table($vote_table)->where([
                ['user_id', '=', $user->id],
                [$event_column_name, '=', $request->post('event_id')],
            ])->get();


            if ($check->count() == 0) {
                return $this->do_vote($user->id, $request->post('event_id'), $request->post('vote_type'), $vote_table, $event_table, $event_column_name);
            } else {

                if ($request->post('vote_type') == 'up') {

                    $check1 = DB::table($vote_table)->where([
                        ['user_id', '=', $user->id],
                        [$event_column_name, '=', $request->post('event_id')],
                        ['vote_type', '=', 'up'],
                    ])->first();

                    $check2 = DB::table($vote_table)->where([
                        ['user_id', '=', $user->id],
                        [$event_column_name, '=', $request->post('event_id')],
                        ['vote_type', '=', 'down'],
                    ])->first();

                    if ($check1)
                        return $this->cancel_vote($event_table, $request->post('event_id'), "up", $vote_table, $event_column_name);
                    if ($check2)
                        $this->cancel_vote($event_table, $request->post('event_id'), "down", $vote_table, $event_column_name);

                    return $this->do_vote($user->id, $request->post('event_id'), 'up', $vote_table, $event_table, $event_column_name);
                }

               else if ($request->post('vote_type') == 'down') {

                    $check1 = DB::table($vote_table)->where([
                        ['user_id', '=', $user->id],
                        [$event_column_name, '=', $request->post('event_id')],
                        ['vote_type', '=', 'down'],
                    ])->first();

                    $check2 = DB::table($vote_table)->where([
                        ['user_id', '=', $user->id],
                        [$event_column_name, '=', $request->post('event_id')],
                        ['vote_type', '=', 'up'],
                    ])->first();

                    if ($check1)
                        return $this->cancel_vote($event_table, $request->post('event_id'), "down", $vote_table, $event_column_name);
                    if ($check2)
                        $this->cancel_vote($event_table, $request->post('event_id'), "up", $vote_table, $event_column_name);


                    return $this->do_vote($user->id, $request->post('event_id'), 'down', $vote_table, $event_table, $event_column_name);
                }
            }
        });
        return $transaction;

    }


    private function do_vote($user_id,$event_id,$vote_type,$vote_table,$event_table,$event_column_name){
        $transaction =  DB::transaction(function() use($user_id,$event_id,$vote_type,$vote_table,$event_table,$event_column_name) {

            $id = DB::table($vote_table)
                ->insertGetId([
                    'user_id' => $user_id,
                    $event_column_name => $event_id,
                    'vote_type' => $vote_type,
                ]);

            if ($vote_type == 'up') {
                $id2 = DB::table($event_table)
                    ->where('id', '=', $event_id)
                    ->increment('up_vote', 1);

                $owner_user_id = DB::table($event_table)
                    ->where('id', '=', $event_id)
                    ->get();
                $owner_user_id = $owner_user_id[0]->user_id;
                $id3 = DB::table('users')
                    ->where('id', '=', $owner_user_id)
                    ->increment('points', 1);

                app('App\Http\Controllers\BadgesController')->add_points_and_date($user_id, 1);
                app('App\Http\Controllers\BadgesController')->Bronze_Mortarboard($user_id);
                app('App\Http\Controllers\BadgesController')->Silver_Amazing($user_id);
                app('App\Http\Controllers\BadgesController')->Gold_Legendary($user_id);
                if (!empty($id2) && !empty($id3))
                    app('App\Http\Controllers\BadgesController')->Bronze_Supporter($user_id);


            } else {
                if ($vote_type == 'down') {
                    $id2 = DB::table($event_table)
                        ->where('id', '=', $event_id)
                        ->increment('down_vote', 1);

                    $owner_user_id = DB::table($event_table)
                        ->where('id', '=', $event_id)
                        ->get();
                    $owner_user_id = $owner_user_id[0]->user_id;
                    $id3 = DB::table('users')
                        ->where('id', '=', $owner_user_id)
                        ->increment('points', -1);

                    if (!empty($id2) && !empty($id3))
                        app('App\Http\Controllers\BadgesController')->Bronze_Critic($user_id);
                }
            }

            if (empty($id) || empty($id2) || empty($id3)) {
                $errors = 'Failed to update row in database.';
                return $this->ApiResponse(null, $errors, 17);
            } else {
                $data = [
                    'user_id' => $user_id,
                    $event_column_name => $event_id,
                    'vote_type' => $vote_type,
                ];
                return $this->ApiResponse($data, null, 200);
            }

        });
        return $transaction;


    }

    private function cancel_vote($event_table,$event_id,$vote_type,$vote_table,$event_column_name)
    {
        $transaction =  DB::transaction(function() use($event_table,$event_id,$vote_type,$vote_table,$event_column_name) {

            $user = User::where('remember_token', '=', Request::header('token'))->first();

            $id = DB::table($vote_table)
                ->where([
                    ['user_id', '=', $user->id],
                    [$event_column_name, '=', $event_id],
                    ['vote_type', '=', $vote_type],
                ])->delete();


            if ($vote_type == 'up') {
                $id2 = DB::table($event_table)
                    ->where('id', '=', $event_id)
                    ->increment('up_vote', -1);

                $owner_user_id = DB::table($event_table)
                    ->where('id', '=', $event_id)
                    ->get();
                $owner_user_id = $owner_user_id[0]->user_id;
                $id3 = DB::table('users')
                    ->where('id', '=', $owner_user_id)
                    ->increment('points', -1);
            } else {
                if ($vote_type == 'down') {
                    $id2 = DB::table($event_table)
                        ->where('id', '=', $event_id)
                        ->increment('down_vote', -1);

                    $owner_user_id = DB::table($event_table)
                        ->where('id', '=', $event_id)
                        ->get();
                    $owner_user_id = $owner_user_id[0]->user_id;
                    $id3 = DB::table('users')
                        ->where('id', '=', $owner_user_id)
                        ->increment('points', 1);
                }
            }

            if (empty($id) || empty($id2)|| empty($id3)) {
                $errors = 'Failed to update row in database.';
                return $this->ApiResponse(null, $errors, 17);
            } else {
                $data = [
                    $event_column_name => $event_id,
                    'vote_type' => $vote_type,
                ];
                return $this->ApiResponse($data, null, 200);
            }
        });
        return $transaction;


    }


}
