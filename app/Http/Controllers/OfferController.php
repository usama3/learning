<?php

namespace App\Http\Controllers;

use App\Models\Colleges;
use App\Models\Course;
use App\Models\Offers;
use App\Models\Universities;
use App\Models\User;
use Encore\Admin\Facades\Admin;
use Validator;
use DB;
use Request;

class OfferController extends Controller
{

    public function subscribe_offer(\Illuminate\Http\Request $request)
    {
        $transaction =  DB::transaction(function() use($request) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            $id = DB::table('offers_user')->insertGetId([
                'user_id' => $user->id,
                'offers_id' => $request->offers_id,
            ]);

            if (empty($id)) {
                $errors = 'Failed to insert row in database.';
                return $this->ApiResponse(null, $errors, 17);
            } else
                return $this->ApiResponse($request->all(), null, 200);
        });
        return $transaction;

    }

    public function un_subscribe_offer(\Illuminate\Http\Request $request)
    {
        $transaction =  DB::transaction(function() use($request) {

            $token = Request::header('token');
            $user = User::where('remember_token', '=', $token)->first();

            $id = DB::table('offers_user')
                ->where([
                    ['user_id', '=', $user->id],
                    ['offers_id', '=', $request->offers_id],
                ])->delete();

            if (empty($id)) {
                $errors = 'Failed to delete row in database.';
                return $this->ApiResponse(null, $errors, 17);
            } else
                return $this->ApiResponse($request->all(), null, 200);
        });
        return $transaction;

    }

    public function show_offers()
    {
        $token = Request::header('token');
        $user = User::where('remember_token', '=', $token)->first();

        $offers =  Offers::with('universities','colleges','limitations')
            ->where('universities_id','=',$user->universites_id)
            ->orwhere('colleges_id','=',$user->colleges_id)
            ->orwhere([
                ['universities_id','=',$user->universites_id],
                ['colleges_id','=',$user->colleges_id]
            ])
            ->orwhere([
                ['universities_id','=',0],
                ['colleges_id','=',0]
            ])
            ->get();

        return $this->ApiResponse($offers,null,200);
    }


}
