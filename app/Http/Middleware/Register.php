<?php

namespace App\Http\Middleware;
use App\Traits\ApiResponseTrait;
use Request;
use Validator;
use DB;
use Closure;

class Register
{
    use ApiResponseTrait;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( $request->post('password')!= $request->post('confirmed_password')){
            $errors = "Password doesn't match";
            return $this->ApiResponse(null, $errors, 422);
        }

        $validator =  Validator::make($request->all(), [
            'username' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => [
                'required',
                'string',
                'min:8',
                'regex:/^(?=.*[a-z|A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/',
            ],
            'first_name' => 'min:3|max:20',
            'middle_name' => 'min:3|max:20',
            'last_name' => 'min:3|max:20',
            'year_of_study' => 'required',
            'countries_id' => 'required',
            'cities_id' => 'required',
            'universities_id' => 'required',
            'colleges_id' => 'required',
            'majors_id' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 111);
        }
        else
            return $next($request);


    }
}
