<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Traits\ApiResponseTrait;
use Closure;
use Request;
use Validator;
use DB;

class CheckToken
{
    use ApiResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = Request::header('token');
        $user = User::where('remember_token','=',$token)->first();

        if(!empty($user))
            return $next($request);
        else{
            $errors = "Token doesn't belong to user!";
            return $this->ApiResponse(null, $errors, 404);
        }

    }
}
