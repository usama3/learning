<?php

namespace App\Http\Middleware;

use App\Traits\ApiResponseTrait;
use Closure;
use Request;
use Validator;
use DB;
class ResetPassword
{
    use ApiResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $reset = DB::table('password_resets')->where('email','=',$request->post('email'))->get();

        if(!$reset->isEmpty()) {
            if (Hash::check(Request::header('token'), $reset[0]->token)) {

                $validator = Validator::make($request->all(), [
                    'password' => ['required',
                        'min:8',
                        'regex:/^(?=.*[a-z|A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/',
                    ],
                ]);

                if ($validator->fails()) {
                    $errors = $validator->errors();
                    return $this->ApiResponse(null, $errors->first(), 422);
                }
                return $next($request);

            }
        }
        else{
            $errors = "Token doesn't belong to user";
            return $this->ApiResponse(null, $errors, 422);
        }
    }
}
