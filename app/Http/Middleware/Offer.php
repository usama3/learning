<?php

namespace App\Http\Middleware;

use App\Models\Course;
use App\Models\User;
use App\Traits\ApiResponseTrait;
use Carbon\Carbon;
use Closure;
use Request;
use Validator;
use DB;

class Offer
{
    use ApiResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        $course_id = $request->segment(3);
//        $token = Request::header('token');
//        $user = User::where('remember_token','=',$token)->first();
//
//        if($user) {
//
//            $user_role =  User::with('roles')
//                ->where('users.id','=',$user->id)
//                ->get();
//
//            foreach ($user_role[0]->roles as $role){}
//            if($role->name == 'Administrator')
//                return $next($request);
//
//
//            $user_offer = User::with('offers','offers.periods')
//                ->where('id', '=', $user->id)
//                ->first();
//
//            $course = Course::where('id','=',$course_id)->first();
//
//            if ($user_offer) {
//                if ($user->universities_id == $course->universities_id &&
//                    $user->colleges_id == $course->colleges_id && $user->majors_id == $course->majors_id){
//
//                    $current_time = new \DateTime(Carbon::now()->toDateTimeString());
//                    $period = false;
//                    foreach ($user_offer->offers[0]->periods as $period){
//                        $start = new \DateTime($period->start_date);
//                        $end = new \DateTime($period->end_date);
//                        if( ((strtotime($current_time->format('y-m-d')) -  strtotime($end->format('y-m-d'))) <= 0) &&
//                            ((strtotime($current_time->format('y-m-d')) -  strtotime($start->format('y-m-d'))) >= 0)) {
//                            $period = true;
//                        }
//                    }
//
//                    if($period == true){
//
//                        $course_year =  $course->year;
//                        $current_year = $user->year_of_study;
//                        $previous_year = $user->year_of_study - 1;
//                        $next_year = $user->year_of_study + 1;
//                        $allowed_years = (explode(',',$user_offer->offers[0]->years));
//
//                        if(($current_year == $course_year && in_array('current',$allowed_years))||
//                            ($previous_year == $course_year && in_array('previous',$allowed_years))||
//                            ($next_year == $course_year && in_array('next',$allowed_years))||
//                            in_array('all',$allowed_years)){
//
//                            $course_semester =  $course->semester;
//                            $allowed_semesters = (explode(',',$user_offer->offers[0]->semesters));
//                            if((in_array($course_semester,$allowed_semesters))){
                                return $next($request);
//                            }
//                            else{
//                                $errors = "Your offer doesn't allow you to access this semester.";
//                                return $this->ApiResponse(null, $errors, 403);
//                            }
//                        }
//                        else{
//                            $errors = "Your offer doesn't allow you to access this year.";
//                            return $this->ApiResponse(null, $errors, 403);
//                        }
//                    }
//                    else{
//                        $errors = "Your offer has expired!";
//                        return $this->ApiResponse(null, $errors, 403);
//                    }
//                }
//                else{
//                    $errors = "Your offer doesn't allow you to access this course.";
//                    return $this->ApiResponse(null, $errors, 403);
//                }
//            }
//            else {
//                $errors = "Please subscribe to get access.";
//                return $this->ApiResponse(null, $errors, 403);
//            }
//        }
//        else{
//            $errors = "Token doesn't belong to user";
//            return $this->ApiResponse(null, $errors, 404);
//        }

    }
}


