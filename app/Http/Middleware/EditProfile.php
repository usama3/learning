<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Traits\ApiResponseTrait;
use Closure;
use Request;
use Validator;
use DB;

class EditProfile
{
    use ApiResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::where('remember_token','=',Request::header('token'))->get();

        if(!$user->isEmpty()) {

            $validator = Validator::make($request->all(), [
                'username' => 'required|min:3|max:20',
                'email' => 'required|email',
                'first_name' => 'min:3|max:20',
                'middle_name' => 'min:3|max:20',
                'last_name' => 'min:3|max:20',
                'year_of_study' => 'required',
                'countries_id' => 'required',
                'cities_id' => 'required',
                'universities_id' => 'required',
                'colleges_id' => 'required',
                'majors_id' => 'required',

            ]);

            if ($validator->fails()) {
                $errors = $validator->errors();
                return response()->json($errors, 422);
            }
            return $next($request);

        }
        else{
            $errors = "Token doesn't belong to user";
            return $this->ApiResponse(null, $errors, 422);
        }

    }
}
