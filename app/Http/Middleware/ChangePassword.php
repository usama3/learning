<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Traits\ApiResponseTrait;
use Closure;
use Request;
use Validator;
use DB;


class ChangePassword
{
    use ApiResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::where('remember_token','=',Request::header('token'))->get();

        if(!$user->isEmpty()) {
            $validator = Validator::make($request->all(), [
                'password' => ['required',
                    'min:8',
                    'regex:/^(?=.*[a-z|A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/',
                ],
            ]);

            if ($validator->fails()) {
                $errors = $validator->errors();
                return response()->json($errors, 422);
            }

            return $next($request);
        }
        else{
            $errors = "Token doesn't belong to user";
            return $this->ApiResponse(null, $errors, 422);
        }
    }
}
