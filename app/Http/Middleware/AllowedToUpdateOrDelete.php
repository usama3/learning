<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Traits\ApiResponseTrait;
use Closure;
use Request;
use Validator;
use DB;

class AllowedToUpdateOrDelete
{
    use ApiResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $post_id_column_name, $table_suffix)
    {
        $token = Request::header('token');
        $user = User::where('remember_token','=',$token)->first();

        if($user) {
            $admin = false;
            $user_role = User::with('roles')
                ->where('users.id', '=', $user->id)
                ->get();
            foreach ($user_role[0]->roles as $role)
                if ($role->name == 'Administrator')
                    $admin = true;

            $table = null;
            if ($request->segment(3) == 'text')
                $table = 'lecture_text_' . $table_suffix;
            else if ($request->segment(3) == 'video')
                $table = 'lecture_video_' . $table_suffix;

            if ($table == null)
                if ($request->type == 'text')
                    $table = 'lecture_text_' . $table_suffix;
                else if ($request->type == 'video')
                    $table = 'lecture_video_' . $table_suffix;

            if ($post_id_column_name == 'null')
                $id = $request->segment(4);
            else
                $id = $request->post($post_id_column_name);

            $check = DB::table($table)
                ->where([
                    ['id', '=', $id],
                    ['user_id', '=', $user->id]
                ])
                ->first();

            if ($check || $admin)
                return $next($request);
            else {
                $errors = "You don't have permission!";
                return $this->ApiResponse(null, $errors, 403);
            }
        }
        else{
            $errors = "Token doesn't belong to user";
            return $this->ApiResponse(null, $errors, 404);
        }

    }
}
