<?php

namespace App\Http\Middleware;

use App\Traits\ApiResponseTrait;
use Closure;
use DB;
use Validator;
use Illuminate\Support\Facades\Auth;


class Login
{
    use ApiResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return $this->ApiResponse(null, 'Email or password not correct', 401);

        $credentials['verified'] = 1;
        if(!Auth::attempt($credentials))
            return $this->ApiResponse(null, 'Account is not verified. Please check your email.', 401);

        $credentials['deleted_at'] = null;
        if(!Auth::attempt($credentials))
            return $this->ApiResponse(null, 'This account has been deleted', 401);

        return $next($request);
    }
}
