<?php

namespace App\Traits;

use Illuminate\Http\Request;

trait ApiResponseTrait
{
    /**
     * Determines if request is an api call.
     *
     * If the request URI contains '/api/v'.
     *
     * @param Request $request
     * @return bool
     */
    public function ApiResponse($data = null, $error = null, $code = 200)
    {
       $array = [
           'data' => $data,
           'status' => $code == 200 ? true : false,
           'error' => $error,
           'code' => $code
       ];

       return response()->json($array,$code);
    }

}