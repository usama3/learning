<?php

namespace App\Traits;

use Illuminate\Http\Request;

trait ErrorResponseTrait
{
    /**
     * Determines if request is an api call.
     *
     * If the request URI contains '/api/v'.
     *
     * @param Request $request
     * @return bool
     */
    public function ErrorResponse($code,$msg = "")
    {
       $errors = [
           '200' => "The request has succeeded.",
           '201' => "The request has been fulfilled and resulted in a new resource being created.",
           '401' => "You are not logged in, using a valid information.",
           '403' => "You are authenticated but do not have access to what you are trying to do.",
           '404' => "The resource you are requesting does not exist.",
           '405' => "The request type is not allowed.",
           '422' => "Validation Error.",
           '500' => "Failed to handle row in database.",
       ];

       if($msg != "")
           $errors[$code] = $msg;

       return $errors[$code];
    }

}