<?php
/**
 * Created by PhpStorm.
 * User: Usama
 * Date: 18/09/14
 * Time: 5:18 AM
 */
namespace App\Repositories;

interface RepositoryInterface
{
    public function all();

    public function create(array $data);

    public function update(array $data, $id);

    public function delete($id);

    public function show($id);
}