<?php

namespace App\Jobs;
use App\Models\User;
use App\Notifications\SignupActivate;
use Mail;
use App\Mail\EmailVerification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendVerificationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user,$hash;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user,$hash)
    {
        $this->user = $user;
        $this->hash = $hash;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->user->notify(new SignupActivate($this->hash));
    }
}
