<?php

use Illuminate\Routing\Router;
use Encore\Admin\Facades\Admin;


Admin::registerAuthRoutes();
Route::resource('admin/auth/users', \App\Admin\Controllers\UserController::class)->middleware(config('admin.route.middleware'));
Route::resource('admin/auth/setting', \App\Admin\Controllers\SettingsController::class)->middleware(config('admin.route.middleware'));
Route::resource('admin/auth/login', \App\admin\Controllers\Auth\LoginController::class)->middleware(config('admin.route.middleware'));
//Route::resource('admin/auth/logout', \App\Http\Controllers\Auth\LogoutController::class)->middleware(config('admin.route.middleware'));

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');
//    $router->resource('/users', UserController::class);
    $router->get("/api/countries", "CountriesController@country");
    $router->get("/api/cities", "CitiesController@city");
    $router->get("/api/universities", "UniversitiesController@University");
    $router->get("/api/colleges", "CollegesController@College");
    $router->get("/api/majors", "MajorsController@Major");
    $router->get("/api/courses", "CourseController@Course");
    $router->get("/api/text_lectures", "Text_lectureController@lecture");
    $router->get("/api/video_lectures", "Video_lectureController@video");
    $router->get("/api/users", "UserController@users");
    $router->get("/api/offers", "OffersController@offers");
    $router->resource('Universities', UniversitiesController::class);
    $router->resource('Colleges', CollegesController::class);
    $router->resource('Majors', MajorsController::class);
    $router->resource('Course', CourseController::class);
    $router->resource('Text_lecture', Text_lectureController::class);
    $router->resource('Text_lecture_sections', Text_sectionController::class);
    $router->resource('Video_lecture', Video_lectureController::class);
    $router->resource('Video_lecture_sections', Video_sectionController::class);
    $router->resource('Badges', BadgeController::class);
    $router->resource('Reports',ReportsController::class);
    $router->resource('Reports_msg',ReportsMSGController::class);
    $router->resource('offers',OffersController::class);
    $router->resource('users_offers',UsersOffersController::class);
    $router->resource('Offers_period',PeriodController::class);
    $router->resource('static_images',StaticImagesController::class);





});
