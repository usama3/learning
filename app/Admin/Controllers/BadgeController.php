<?php

namespace App\Admin\Controllers;

use App\Models\badge;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class BadgeController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Index');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Show interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Detail');
            $content->description('description');

            $content->body(Admin::show(badge::findOrFail($id), function (Show $show) {

                $show->id();

                $show->created_at();
                $show->updated_at();
            }));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Edit');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Create');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(badge::class, function (Grid $grid) {
            $grid->actions(function ($actions) {
                $actions->disableView();
                $actions->disableDelete();
//                    $actions->disableEdit();
            });
            $grid->disableCreateButton();
//            $grid->disableActions();
            $grid->disableRowSelector();


            $grid->name();
            $grid->description()->display(function($description) {
                return str_limit($description, 500, 'description');
            });
            $grid->points();
            $grid->picture()->image('', 150, 150);
            $grid->earned_picture()->gallery();

            $grid->filter(function ($filter) {
                $filter->like('name', 'Name');
                $filter->like('points', 'Points');
            });


    });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(badge::class, function (Form $form) {

            $form->tools(function (Form\Tools $tools) {
                $tools->disableView();
                $tools->disableDelete();
            });
//            $form->text('name', 'Name')->rules('required|min:3|max:50');
            $form->ckeditor('description', 'Description')->rules('required');
            $form->number('points', 'Points')->min(0)->default(100)->rules('required');
//            $form->select('type', 'Type')->rules('required')->options([
//                1 => 'Bronze',
//                2 => 'Sliver',
//                3 => 'Gold',
//            ]);
            $form->image('picture')
                ->rules('required')
                ->uniqueName()
                ->name(function ($file) {
                    return 'test.' . $file->guessExtension();
                })->move('images/Badges')->removable();

            $form->image('earned_picture')
                ->rules('required')
                ->uniqueName()
                ->name(function ($file) {
                    return 'test.' . $file->guessExtension();
                })->move('images/Badges')->removable();

        });
    }
}
