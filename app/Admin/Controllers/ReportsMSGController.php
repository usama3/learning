<?php

namespace App\Admin\Controllers;

use App\Models\Reports_msg;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ReportsMSGController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Index');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Show interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Detail');
            $content->description('description');

            $content->body(Admin::show(Reports_msg::findOrFail($id), function (Show $show) {

                $show->id();

                $show->created_at();
                $show->updated_at();
            }));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Edit');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Create');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Reports_msg::class, function (Grid $grid) {

            $grid->actions(function ($actions) {
                $actions->disableView();
            });
            $grid->column('type','Type');
            $grid->column('msg','Message')->label('success');

            $grid->filter(function ($filter) {
                $filter->like('type', 'Type');
                $filter->like('msg', 'Message');
                $filter->between('updated_at',"Date")->datetime();


            });

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Reports_msg::class, function (Form $form) {

            $form->tools(function (Form\Tools $tools) {
                $tools->disableView();
            });

            $form->select('type', 'Type')->rules('required')->options([
                1 => 'user',
                2 => 'question',
                3 => 'answer',
                4 => 'summary',
                5 => 'bookmark'
            ]);

            $form->text('msg', 'Message')->rules('required|min:1|max:300');


        });
    }
}
