<?php

namespace App\Admin\Controllers;

use App\Models\Colleges;
use App\Models\Majors;
use App\Http\Controllers\Controller;
use App\Models\Universities;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use DB;

class MajorsController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Majors');
//            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Show interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Detail');
            $content->description('description');

            $content->body(Admin::show(Majors::findOrFail($id), function (Show $show) {

                $show->id();

                $show->created_at();
                $show->updated_at();
            }));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Edit');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Create');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Majors::class, function (Grid $grid) {

            $grid->actions(function ($actions) {
                $actions->disableView();
                if(!Admin::user()->isAdministrator()) {
                    $actions->disableDelete();
                    $actions->disableEdit();
                }
            });
            $grid->name();
            $grid->description()->display(function($description) {
                return str_limit($description, 500, 'description');
            });
            $grid->column('universities.name', 'University');
            $grid->column('colleges.name', 'College');
            $grid->years();
            $grid->picture()->gallery();

            $grid->filter(function ($filter) {
                $filter->like('name', 'Name');
                $filter->like('universities.name', 'University');
                $filter->like('colleges.name', 'College');
                $filter->between('updated_at',"Date")->datetime();


            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Majors::class, function (Form $form) {

            $form->tools(function (Form\Tools $tools) {
                $tools->disableView();
            });
            $form->text('name', 'Name')->rules('required|min:1|max:50');
            $form->ckeditor('description', 'Description');

            if(!Admin::user()->isRole('administrator')) {
                $form->select("universities_id", "University")->rules('required')->options(Universities::where('id', Admin::user()->universities_id)->pluck('name','id'))->default('1');
                $form->select("colleges_id", "College")->rules('required')->options(Colleges::where('id', Admin::user()->colleges_id)->pluck('name','id'))->default('1');
            }
            else
            {
                $form->select("universities_id","University")->rules('required')->options(function ($id) {
                    $ret = Universities::find($id);

    //                if ($ret) {
    //                    return [$ret->id => $ret->name];
    //                }else
                        return null;
                })->ajax("/admin/api/universities")->load('colleges_id', '/admin/api/colleges');


                $form->select("colleges_id","College")->rules('required')->options(function ($id) {
                    $ret = Colleges::find($id);

                    if ($ret) {
                        return [$ret->id => $ret->name];
                    }
                })->ajax("/admin/api/colleges")->load('majors_id', '/admin/api/majors');
            }
//            $form->number('year', 'Year')->min(1)->max(10)->default(1);
            $form->listbox('years', 'Years')->options([
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6' ,

            ])->rules('required');

            $form->image('picture')
                ->uniqueName()
                ->name(function ($file) {
                    return 'test.'.$file->guessExtension();
                })->move('images/Majors');
        });
    }

    public function Major(Request $request){

        $provinceId = $request->get('q');

        return Majors::where('colleges_id', $provinceId)->get(['id', DB::raw('name as text')]);

    }
}
