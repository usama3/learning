<?php

namespace App\Admin\Controllers;

use App\Models\Colleges;
use App\Http\Controllers\Controller;
use App\Models\Universities;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use DB;

class CollegesController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Colleges');
//            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Show interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Detail');
            $content->description('description');

            $content->body(Admin::show(Colleges::findOrFail($id), function (Show $show) {

                $show->id();

                $show->created_at();
                $show->updated_at();
            }));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Edit');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Create');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Colleges::class, function (Grid $grid) {

            $grid->actions(function ($actions) {
                $actions->disableView();
                $a = $actions->row->majors_id;
                if(!Admin::user()->isAdministrator() && Admin::user()->majors_id != $a) {
                    $actions->disableDelete();
                    $actions->disableEdit();
                }
            });

            $grid->name();
            $grid->description()->display(function($description) {
                return str_limit($description, 500, 'description');
            });
            $grid->column('universities.name', 'University');
            $grid->num_of_years();
            $grid->picture()->gallery();
            $grid->cover_picture()->gallery();


            $grid->filter(function ($filter) {
                $filter->like('name', 'Name');
                $filter->like('universities.name', 'University');
                $filter->between('updated_at',"Date")->datetime();

            });

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Colleges::class, function (Form $form) {

            $form->tools(function (Form\Tools $tools) {
                $tools->disableView();
            });
            $form->text('name', 'Name')->rules('required|min:3|max:50');
            $form->ckeditor('description', 'Description');
            $form->number('num_of_years', 'Number Of Years')->min(1)->max(10)->default(5);


            if(!Admin::user()->isRole('administrator')) {
                $form->select("universities_id", "University")->rules('required')->options(Universities::where('id', Admin::user()->universities_id)->pluck('name', 'id'))->default('1');
            }
            else{
                $form->select("universities_id","University")->rules('required')->options(function ($id) {

//                $ret = Universities::find($id);
//
//                if ($ret) {
//                    return [$ret->id => $ret->name];
//                }else
                return null;
            })->ajax("/admin/api/universities");
            }
            $form->image('picture')
                ->uniqueName()
                ->name(function ($file) {
                    return 'test.'.$file->guessExtension();
                })->move('images/Colleges');
            $form->image('cover_picture')
                ->uniqueName()
                ->rules('dimensions:min_width=600,min_height=150,max_width=1500,max_height=600')
                ->name(function ($file) {
                    return 'test.'.$file->guessExtension();
                })->move('images/colleges');
            $form->select('cover_font_color','Color of font on cover picture')->options([
                '#d8d8d8' => 'Bright',
                '#150d0b' => 'Dark'
            ]);

        });
    }

    public function college(Request $request){

        $provinceId = $request->get('q');

        return Colleges::where('universities_id', $provinceId)->get(['id', DB::raw('name as text')]);

    }
}
