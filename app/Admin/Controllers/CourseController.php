<?php

namespace App\Admin\Controllers;

use App\Models\Colleges;
use App\Models\Course;
use App\Http\Controllers\Controller;
use App\Models\Majors;
use App\Models\Universities;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use DB;


class CourseController extends Controller
{
    use ModelForm;
    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Index');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Show interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Detail');
            $content->description('description');

            $content->body(Admin::show(Course::findOrFail($id), function (Show $show) {

                $show->id();

                $show->created_at();
                $show->updated_at();
            }));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Edit');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Create');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    private $s = true;
    protected function grid()
    {
        return Admin::grid(Course::class, function (Grid $grid) {

                    $grid->actions(function ($actions) {
                        $actions->disableView();
                        $a = $actions->row->majors_id;
                        if(!Admin::user()->isAdministrator() && Admin::user()->majors_id != $a) {
                            $actions->disableDelete();
                            $actions->disableEdit();
                        }
                    });

                        $grid->name();
                        $grid->professor();
                        $grid->description()->display(function ($description) {
                            return str_limit($description, 500, 'description');
                        });
                        $grid->column('universities.name', 'University');
                        $grid->column('colleges.name', 'Colleges');
                        $grid->column('majors.name', 'Major');
                        $grid->lectures_num();
                        $grid->rating()->display(function ($rate) {
                            if ($rate > 0) {
                                $html = "<i class='fa fa-star' style='color:#ff8913'></i>";
                                $a = join('&nbsp;', array_fill(0, min(5, $rate), $html));
                                if (strpos($rate, '.') !== false) {
                                    $html2 = "<i class='fa fa-star-half' style='color:#ff8913'></i>";

                                    $b = join('&nbsp;', array_fill(0, 1, $html2));
                                    return $a . " " . $b;
                                }
                                return join('&nbsp;', array_fill(0, min(5, $rate), $html));
                            }
                            return "Not Rated Yet";
                        });
                        $grid->year();
                        $grid->semester();
                        $grid->picture()->gallery();
                        $grid->created_at();
//                        $grid->updated_at();
                        $grid->filter(function ($filter) {
                            $filter->like('name', 'Name');
                            $filter->like('universities.name', 'University');
                            $filter->like('colleges.name', 'College');
                            $filter->like('majors.name', 'Major');
                            $filter->between('updated_at',"Date")->datetime();

                        });


        });


    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Course::class, function (Form $form) {
            $form->tools(function (Form\Tools $tools) {
                $tools->disableView();
            });
            $form->text('name', 'Name')->rules('required|min:3|max:50');
            $form->text('professor', 'Professor')->rules('required|min:1|max:50');
            $form->ckeditor('description', 'Description')->rules('required');
            $form->number('lectures_num', 'Number Of Lectures')->min(1)->max(40)->default(1);
            $form->number('year', 'Year')->min(1)->max(10)->default(1);
            $form->number('semester', 'Semester')->min(1)->max(2)->default(1);

            if(!Admin::user()->isRole('administrator')) {
                $form->select("universities_id", "University")->rules('required')->options(Universities::where('id', Admin::user()->universities_id)->pluck('name','id'))->default('1');
                $form->select("colleges_id", "College")->rules('required')->options(Colleges::where('id', Admin::user()->colleges_id)->pluck('name','id'))->default('1');
                $form->select("majors_id", "Major")->rules('required')->options(Majors::where('id', Admin::user()->majors_id)->pluck('name','id'))->default('1');
            }
            else {
                $form->select("universities_id", "University")->rules('required')->options(function ($id) {

                $ret = Universities::find($id);

                if ($ret) {
                    return [$ret->id => $ret->name];
                }else
                    return null;
                })->ajax("/admin/api/universities")->load('colleges_id', '/admin/api/colleges');

//                $form->ignore(['universities_id']);


                $form->select("colleges_id", "College")->rules('required')->options(function ($id) {
                    $ret = Colleges::find($id);

                    if ($ret) {
                        return [$ret->id => $ret->name];
                    }
                })->ajax("/admin/api/colleges")->load('majors_id', '/admin/api/majors');

                $form->select("majors_id", "Major")->options(function ($id) {
                    $ret = Majors::find($id);

                    if ($ret) {
                        return [$ret->id => $ret->name];
                    }
                })->ajax("/admin/api/majors");

            }

            $form->image('picture')
                ->uniqueName()
                ->name(function ($file) {
                    return 'test.' . $file->guessExtension();
                })->move('images/Courses');

        });
    }


    public function Course(Request $request){

        $provinceId = $request->get('q');

        return Course::where('majors_id', $provinceId)->get(['id', DB::raw('name as text')]);
    }
}
