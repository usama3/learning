<?php

namespace App\Admin\Controllers;

use App\Models\Colleges;
use App\Models\Limitations;
use App\Models\Offers;
use App\Http\Controllers\Controller;
use App\Models\Period;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use DB;

class OffersController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Index');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Show interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Detail');
            $content->description('description');

            $content->body(Admin::show(Offers::findOrFail($id), function (Show $show) {

                $show->id();

                $show->created_at();
                $show->updated_at();
            }));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Edit');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Create');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Offers::class, function (Grid $grid) {

            $grid->actions(function ($actions) {
                $actions->disableView();
            });
            $grid->column('name', 'Name');
            $grid->column('years', 'Years');
            $grid->column('semesters', 'Semesters');
            $grid->column('price', 'Price');
//            $grid->column('periods_num', 'Number of periods');
            $grid->limitations()->display(function ($limits) {

                $roles = array_map(function ($limit) {
                    return "<span class='label label-success'>{$limit['name']}</span></br>";
                }, $limits);

                return join('&nbsp;', $roles);
            });
            $grid->periods()->display(function ($periods) {

                $roles = array_map(function ($period) {
                    return "<span class='label label-warning'>{$period['name']}</span></br>";
                }, $periods);

                return join('&nbsp;', $roles);
            });
            $grid->mini_description();

            $grid->column('universities.name', 'Target university');
            $grid->column('colleges.name', 'Target college');


        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Offers::class, function (Form $form) {

            $form->tools(function (Form\Tools $tools) {
                $tools->disableView();
                $tools->disableDelete();
            });
            $form->text('name', 'Name')->rules('required|min:1|max:30');
            $form->ckeditor('description', 'Description')->rules('required');
            $form->text('mini_description', 'Mini Description')->rules('required|min:40|max:196');

//            $form->number('periods_num', 'Number of periods')->min(1)->default(0)->rules('required');

            $form->select("universities_id","Target university")->options(function ($id) {

//                $ret = Universities::find($id);
//
//                if ($ret) {
//                    return [$ret->id => $ret->name];
//                }else
                return null;
            })->ajax("/admin/api/universities")->load('colleges_id', '/admin/api/colleges')
                ->help('leave it empty to select all universities');

//
            $form->select("colleges_id","Target college")->options(function ($id) {
                $ret = Colleges::find($id);

                if ($ret) {
                    return [$ret->id => $ret->name];
                }

            })->ajax("/admin/api/colleges")
                ->help('leave it empty to select all colleges');

            $form->listbox('years', 'Years')->options([
                'previous' => 'previous',
                'current' => 'current',
                'next' => 'next',
                'all' => 'all',
            ])->rules('required');

            $form->listbox('semesters', 'Semesters')->options([
                '1' => 'Semester 1',
                '2' => 'Semester 2',
            ])->rules('required');

            $form->listbox('limitations')->options(Limitations::all()->pluck('name', 'id'))->rules('required');
            $form->listbox('periods')->options(Period::all()->pluck('name', 'id'))->rules('required');
            $form->number('price', 'Price')->min(0)->default(0)->rules('required');
            $form->color('first_color', 'First Color')->default('#ccc');
            $form->color('second_color', 'Second Color')->default('#ccc');

            $form->saving(function (Form $form) {
                if($form->universities_id == null)
                    $form->universities_id = 0;
                if($form->colleges_id == null)
                    $form->colleges_id = 0;
            });

        });
    }

    public function offers(Request $request){

        $q = $request->get("q");
        return Offers::where("name", "like", "%".$q."%")->paginate(null, ["id as id", "name as text"]);

    }
}
