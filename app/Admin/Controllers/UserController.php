<?php

namespace App\Admin\Controllers;
use App\Models\Badge;
use App\Repositories\RepositoryInterface;

use App\Models\Offers;
use App\Models\Cities;
use App\Models\Colleges;
use App\Models\Countries;
use App\Models\Majors;
use App\Models\Role;
use App\Models\Universities;
use App\Models\User;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Repositories\Repository;
//use Illuminate\Support\Facades\Auth;
use Encore\Admin\Auth\Permission;
use Illuminate\Http\Request;
use DB;
use Validator;



class UserController extends Controller
{
    use ModelForm;
    protected $user_repo;

    public function __construct(User $user)
    {
        // set the model
        $this->user_repo = new Repository($user);
    }
    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {

        return Admin::content(function (Content $content) {

            $content->header('Users');
//            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Show interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Detail');
            $content->description('description');

            $content->body(Admin::show(User::findOrFail($id), function (Show $show) {


//            $grid->model()->where('name', '=', "soso");

//                $show->id('ID')->sortable();
//                $show->username();
//                $show->email()  ;
//                $show->first_name();
//                $show->middle_name();
//                $show->last_name();
//                $show->year_of_study();
//                $show->countries_id();
//                $show->column('countries','countries.name');
//                $show->column('cities.name', 'City');
//                $show->column('universities.name', 'Universities');
//                $show->column('colleges.name', 'Colleges');
//                $show->column('majors.name', 'Majors');
//                $show->mobile();
//                $show->age();
//                $show->year_of_study();
//                $show->gender();
//                $show->created_at();
//                $show->updated_at();
//                $show->picture()->image();



            }));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Edit');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });

    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Create');
            $content->description('description');

            $content->body($this->form());
        });

    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {

        return Admin::grid(User::class, function (Grid $grid) {

            $grid->actions(function ($actions) {
                if($actions->getKey()==1)
                $actions->disableDelete();
//                $actions->disableEdit();
                $actions->disableView();
            });
            $grid->disableCreateButton();
            $grid->disableRowSelector();

//            $grid->model()->where('name', '=', "soso");

//            $grid->id('ID')->sortable();

            $grid->username();
            $grid->email();
//            $grid->first_name();
//            $grid->middle_name();
//            $grid->last_name();
//            $grid->ip_address();
            $grid->mobile();
//            $grid->phone();
            $grid->column('countries.name', 'Country');
            $grid->column('cities.name', 'City');
            $grid->column('universities.name', 'University');
            $grid->column('colleges.name', 'College');
            $grid->column('majors.name', 'Major');
            $grid->column('points', 'Points');
//            $grid->birthdate();
//            $grid->year_of_study();
            $grid->roles()->display(function ($roles) {

                $roles = array_map(function ($role) {
                    return "<span class='label label-success'>{$role['name']}</span></br>";
                }, $roles);

                return join('&nbsp;', $roles);
            });
            $grid->offers()->display(function ($roles) {

                $roles = array_map(function ($role) {
                    return "<span class='label label-warning'>{$role['name']}</span></br>";
                }, $roles);

                return join('&nbsp;', $roles);
            });
            $grid->badges()->display(function ($roles) {

                $roles = array_map(function ($role) {
                    return "<span class='label label-primary'>{$role['name']}</span></br>";
                }, $roles);

                return join('&nbsp;', $roles);
            });


            $grid->picture()->gallery();

            $grid->created_at();
//            $grid->updated_at();

            $grid->filter(function ($filter) {

                $filter->like('username', 'Username');
                $filter->like('countries.name', 'Country');
                $filter->like('cities.name', 'City');
                $filter->like('universities.name', 'University');
                $filter->like('colleges.name', 'College');
                $filter->like('majors.name', 'Major');
//                $filter->date('created_at', "Date");
                $filter->between('updated_at',"Date")->datetime();
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(User::class, function (Form $form) {

            $form->tools(function (Form\Tools $tools) {
                $tools->disableView();
                $tools->disableDelete();

            });
//            $form->display('id', 'ID');

            $form->text('username', 'User Name')->rules('required|min:3|max:20');
            $form->email('email', 'email')->rules('required|email');
//            $form->password('password', 'Password')->rules(['required',
//                'min:8',
//                'regex:/^(?=.*[a-z|A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/',
//                'confirmed']);
//            $form->password('password_confirmation','Password Confirmation')->rules('required');
//            $form->ignore(['password_confirmation']);
//            $form->saving(function (Form $form) {
//                if ($form->password && $form->model()->password != $form->password) {
//                    $form->password = bcrypt($form->password);
//                }
//            });

            $form->text('first_name', 'First Name');
            $form->text('middle_name', 'Middle Name');
            $form->text('last_name', 'Last Name');
//            $form->ip('ip_address', 'Ip address');
            $form->number('year_of_study', 'Year')->min(1)->max(6)->default(1);


//            $form->select("Country")->rules('required')->options(Countries::all()->pluck("name", "cc_fips"));

            $form->select("countries_id","Country")->options(function ($id) {
//                $ret = Countries::find($id);
//
//                if ($ret) {
//                    return [$ret->id => $ret->name];
//                }else
                    return null;
            })->ajax("/admin/api/countries")->load('cities_id', '/admin/api/cities');


            $form->select("cities_id","City")->options(function ($id) {
                $ret = Cities::find($id);

                if ($ret) {
                    return [$ret->id => $ret->name];
                }
            })->ajax("/admin/api/cities");

            $form->select("universities_id","University")->rules('required')->options(function ($id) {

//                $ret = Universities::find($id);

//                if ($ret) {
//                    return [$ret->id => $ret->name];
//                }else
                 return null;
            })->ajax("/admin/api/universities")->load('colleges_id', '/admin/api/colleges');


            $form->select("colleges_id","College")->rules('required')->options(function ($id) {
                $ret = Colleges::find($id);

                if ($ret) {
                    return [$ret->id => $ret->name];
                }
            })->ajax("/admin/api/colleges")->load('majors_id', '/admin/api/majors');

            $form->select("majors_id","Major")->options(function ($id) {
                $ret = Majors::find($id);

                if ($ret) {
                    return [$ret->id => $ret->name];
                }
            })->ajax("/admin/api/majors");

            $form->mobile('mobile', 'Mobile')->options(['mask' => '00999 999999999']);
            $form->date('birthdate', 'Birthdate');
            $form->radio("gender","Gender")->options(['Male' => 'Male', 'Female'=> 'Female'])->default('m');
            $form->multipleSelect('roles', trans('admin.roles'))->options(Role::all()->pluck('name', 'id'))->rules('required');

            $form->image('picture')
            ->uniqueName()
            ->name(function ($file) {
                return 'test.'.$file->guessExtension();
            })->move('images/Users');

            $form->listbox('offers')->options(Offers::all()->pluck('name', 'id'))->rules('required');
            $form->listbox('badges')->options(Badge::all()->pluck('name', 'id'));



        });
    }
    public function users(Request $request){

        $q = $request->get("q");
        return User::where("username", "like", "%".$q."%")->paginate(null, ["id as id", "username as text"]);
    }


}
