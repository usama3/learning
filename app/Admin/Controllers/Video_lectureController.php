<?php

namespace App\Admin\Controllers;

use App\Models\Colleges;
use App\Models\Course;
use App\Models\Majors;
use App\Models\Universities;
use App\Models\Video_lecture;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use DB;


class Video_lectureController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Video Lectures');
//            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Show interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Detail');
            $content->description('description');

            $content->body(Admin::show(Video_lecture::findOrFail($id), function (Show $show) {

                $show->id();

                $show->created_at();
                $show->updated_at();
            }));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Edit');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Create');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Video_lecture::class, function (Grid $grid) {

            $grid->actions(function ($actions) {
                $actions->disableView();
                $a = $actions->row->majors_id;
                if(!Admin::user()->isAdministrator() && Admin::user()->majors_id != $a) {
                    $actions->disableDelete();
                    $actions->disableEdit();
                }
            });

            $grid->name();
            $grid->number();
            $grid->column('universities.name', 'University');
            $grid->column('colleges.name', 'Colleges');
            $grid->column('majors.name', 'Major');
            $grid->column('courses.name', 'Course');
//            $grid->video()->image();
            $grid->created_at();
            $grid->updated_at();

            $grid->filter(function ($filter) {
                $filter->like('name', 'Name');
                $filter->like('universities.name', 'University');
                $filter->like('colleges.name', 'College');
                $filter->like('majors.name', 'Major');
                $filter->like('courses.name', 'Course');
                $filter->between('updated_at',"Date")->datetime();

            });
        });

    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Video_lecture::class, function (Form $form) {

            $form->tools(function (Form\Tools $tools) {
                $tools->disableView();
            });
            $form->text('name', 'Name')->rules('required|min:3|max:50');
            $form->number('number', 'Lecture Number')->min(1)->max(40)->default(1)->rules('required');

            if(!Admin::user()->isRole('administrator')) {
                $form->select("universities_id", "University")->rules('required')->options(Universities::where('id', Admin::user()->universities_id)->pluck('name','id'))->default('1');
                $form->select("colleges_id", "College")->rules('required')->options(Colleges::where('id', Admin::user()->colleges_id)->pluck('name','id'))->default('1');
                $form->select("majors_id", "Major")->rules('required')->options(Majors::where('id', Admin::user()->majors_id)->pluck('name','id'))->default('1');
                $form->select("courses_id","Course")->rules('required')->options(Course::where([
                    ['universities_id', '=', Admin::user()->universities_id],
                    ['colleges_id', '=', Admin::user()->colleges_id],
                    ['majors_id', '=', Admin::user()->majors_id],

                ])->pluck('name','id'))->default('1');
            }
            else
            {
                $form->select("universities_id","University")->rules('required')->options(function ($id) {

                    //                $ret = Universities::find($id);
                    //
                    //                if ($ret) {
                    //                    return [$ret->id => $ret->name];
                    //                }else
                    return null;
                })->ajax("/admin/api/universities")->load('colleges_id', '/admin/api/colleges');

                $form->select("colleges_id","College")->rules('required')->options(function ($id) {
                    $ret = Colleges::find($id);

                    if ($ret) {
                        return [$ret->id => $ret->name];
                    }
                })->ajax("/admin/api/colleges")->load('majors_id', '/admin/api/majors');

                $form->select("majors_id","Major")->options(function ($id) {
                    $ret = Majors::find($id);

                    if ($ret) {
                        return [$ret->id => $ret->name];
                    }
                })->ajax("/admin/api/majors")->load('courses_id', '/admin/api/courses');

                $form->select("courses_id","Course")->options(function ($id) {
                    $ret = Course::find($id);

                    if ($ret) {
                        return [$ret->id => $ret->name];
                    }
                })->ajax("/admin/api/courses");
            }
            $form->file('video')
                ->rules('required|mimes:mp4')
                ->uniqueName()
                ->name(function ($file) {
                    return 'test.'.$file->guessExtension();
                })->move('lectures/videos');

        });
    }

    public function video(Request $request){
        $provinceId = $request->get('q');
        return Video_lecture::where('courses_id', $provinceId)->get(['id', DB::raw('name as text')]);

    }
}
