<?php

namespace App\Admin\Controllers;

use App\Models\Colleges;
use App\Models\Universities;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class UniversitiesController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Universities');
//            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Show interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Detail');
            $content->description('description');

            $content->body(Admin::show(Universities::findOrFail($id), function (Show $show) {

                $show->id();

                $show->created_at();
                $show->updated_at();
            }));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Edit');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Create');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Universities::class, function (Grid $grid) {

            $grid->actions(function ($actions) {
                $actions->disableView();
            });
            $grid->name();
            $grid->description()->display(function($description) {
                return str_limit($description, 500, 'description');
            });
            $grid->picture()->gallery();

            $grid->filter(function ($filter) {
                $filter->like('name', 'Name');
                $filter->between('updated_at',"Date")->datetime();

            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Universities::class, function (Form $form) {

            $form->tools(function (Form\Tools $tools) {
                $tools->disableView();
            });

            $form->text('name', 'Name')->rules('required|min:3|max:50');
            $form->ckeditor('description', 'Description');
            $form->image('picture')
                ->uniqueName()
                ->name(function ($file) {
                    return 'test.'.$file->guessExtension();
                })->move('images/universities');

        });
    }

    public function University(Request $request){

        $q = $request->get("q");

        return Universities::where("name", "like", "%".$q."%")->paginate(null, ["id as id", "name as text"]);
    }

}
