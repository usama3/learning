<?php

namespace App\Admin\Controllers;

use App\Models\Colleges;
use App\Models\Course;
use App\Models\Majors;
use App\Models\Text_lecture;
use App\Models\Universities;
use App\Models\Video_lecture;
use App\Models\Video_section;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class Video_sectionController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Video Sections');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Show interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Detail');
            $content->description('description');

            $content->body(Admin::show(Video_section::findOrFail($id), function (Show $show) {

                $show->id();

                $show->created_at();
                $show->updated_at();
            }));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Edit');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Create');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Video_section::class, function (Grid $grid) {

            $grid->actions(function ($actions) {
                $actions->disableView();
                $a = $actions->row->majors_id;
                if(!Admin::user()->isAdministrator() && Admin::user()->majors_id != $a) {
                    $actions->disableDelete();
                    $actions->disableEdit();
                }
            });

            $grid->name();
            $grid->section_start();
            $grid->section_end();
            $grid->section_number();
            $grid->column('universities.name', 'University');
            $grid->column('colleges.name', 'Colleges');
            $grid->column('majors.name', 'Major');
            $grid->column('courses.name', 'Course');
            $grid->column('video_lecture.name', 'Lecture');
            $grid->created_at();
            $grid->updated_at();

            $grid->filter(function ($filter) {
                $filter->like('name', 'Name');
                $filter->like('universities.name', 'University');
                $filter->like('colleges.name', 'College');
                $filter->like('majors.name', 'Major');
                $filter->like('courses.name', 'Course');
                $filter->like('video_lecture.name', 'Lecture');
                $filter->between('updated_at',"Date")->datetime();

            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {

        return Admin::form(Video_section::class, function (Form $form) {

            $form->tools(function (Form\Tools $tools) {
                $tools->disableView();
            });
            $form->text('name', 'Section name')->rules('required|min:3|max:20');
            $form->number('section_start', 'Start time')->min(0)->max(400)->default(0)->rules('required')
                ->help('Section 1 has to start at 0');
            $form->number('section_end', 'End time')->min(0)->max(400)->default(1)->rules('required');
            $form->number('section_number', 'Section number')->min(1)->max(40)->default(1)->rules('required');

            if(!Admin::user()->isRole('administrator')) {
                $form->select("universities_id", "University")->rules('required')->options(Universities::where('id', Admin::user()->universities_id)->pluck('name','id'))->default('1');
                $form->select("colleges_id", "College")->rules('required')->options(Colleges::where('id', Admin::user()->colleges_id)->pluck('name','id'))->default('1');
                $form->select("majors_id", "Major")->rules('required')->options(Majors::where('id', Admin::user()->majors_id)->pluck('name','id'))->default('1');
                $form->select("courses_id","Course")->rules('required')->options(Course::where([
                    ['universities_id', '=', Admin::user()->universities_id],
                    ['colleges_id', '=', Admin::user()->colleges_id],
                    ['majors_id', '=', Admin::user()->majors_id],
                ])->pluck('name','id'))->default('1');

                $form->select("video_lecture_id", "Video Lecture")->rules('required')->options(Text_lecture::where([
                    ['universities_id', '=', Admin::user()->universities_id],
                    ['colleges_id', '=', Admin::user()->colleges_id],
                    ['majors_id', '=', Admin::user()->majors_id],
                ])->pluck('name','id'))->default('1');
            }
            else {
                $form->select("universities_id", "University")->rules('required')->options(function ($id) {

                    //                $ret = Universities::find($id);
                    //
                    //                if ($ret) {
                    //                    return [$ret->id => $ret->name];
                    //                }else
                    return null;
                })->ajax("/admin/api/universities")->load('colleges_id', '/admin/api/colleges');

                $form->select("colleges_id", "College")->rules('required')->options(function ($id) {
                    $ret = Colleges::find($id);

                    if ($ret) {
                        return [$ret->id => $ret->name];
                    }
                })->ajax("/admin/api/colleges")->load('majors_id', '/admin/api/majors');

                $form->select("majors_id", "Major")->options(function ($id) {
                    $ret = Majors::find($id);

                    if ($ret) {
                        return [$ret->id => $ret->name];
                    }
                })->ajax("/admin/api/majors")->load('courses_id', '/admin/api/courses');

                $form->select("courses_id", "Course")->options(function ($id) {
                    $ret = Course::find($id);

                    if ($ret) {
                        return [$ret->id => $ret->name];
                    }
                })->ajax("/admin/api/courses")->load('video_lecture_id', '/admin/api/video_lectures');

                $form->select("video_lecture_id", "Video Lecture")->options(function ($id) {
                    $ret = Video_lecture::find($id);

                    if ($ret) {
                        return [$ret->id => $ret->name];
                    }
                })->ajax("/admin/api/video_lectures");
            }

        });
    }
}
