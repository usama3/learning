<?php

namespace App\Admin\Controllers;

use App\Models\Cities;
use App\Models\Colleges;
use App\Models\Countries;
use App\Models\Majors;
use App\Models\Role;
use App\Models\Universities;
use App\Models\User;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class SettingsController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return redirect( '/admin/auth/users/'.Admin::user()->id.'/edit');
    }


}
