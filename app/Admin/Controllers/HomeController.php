<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Colleges;
use App\Models\Course;
use App\Models\Lecture_text_answers;
use App\Models\Lecture_text_bookmarks;
use App\Models\Lecture_text_questions;
use App\Models\Lecture_text_summaries;
use App\Models\Lecture_video_answers;
use App\Models\Lecture_video_bookmarks;
use App\Models\Lecture_video_questions;
use App\Models\Lecture_video_summaries;
use App\Models\Text_lecture;
use App\Models\Universities;
use App\Models\Video_lecture;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Grid\Displayers\Badge;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\InfoBox;
use App\Models\User;




class HomeController extends Controller
{
    public function index()
    {
//        var_dump(Admin::user()->id);die();
//        $a = new User();s
//        var_dump($a->all_users());

        return Admin::content(function (Content $content) {

            $content->header('Dashboard');
//            $content->description('Description...');


            $content->row(function ($row) {
                $questions1  = Lecture_text_questions::count();
                $questions12 = Lecture_video_questions::count();
                $answers1    = Lecture_text_answers::count();
                $answers2    = Lecture_video_answers::count();
                $summaries1  = Lecture_text_summaries::count();
                $summaries2  = Lecture_video_summaries::count();
                $bookmarks1  = Lecture_text_bookmarks::count();
                $bookmarks2  = Lecture_video_bookmarks::count();

                $row->column(3, new InfoBox('Users', 'users', 'primary', '/admin/auth/users', User::all()->count()));
                $row->column(3, new InfoBox('Universities', 'university', 'green', '/admin/Universities', Universities::all()->count()));
                $row->column(3, new InfoBox('Colleges', 'building', 'yellow', '/admin/Colleges', Colleges::all()->count()));
                $row->column(3, new InfoBox('Courses', 'book', 'info', '/admin/Course', Course::all()->count()));
                $row->column(3, new InfoBox('Text Lectures', 'file-text', 'aqua', '/admin/Text_lecture', Text_lecture::all()->count()));
                $row->column(3, new InfoBox('Video Lectures', 'camera', 'aqua', '/admin/video_lecture', Video_lecture::all()->count()));
                $row->column(3, new InfoBox('Questions', 'fas fa-question', 'white', '',$questions1 +$questions12));
                $row->column(3, new InfoBox('Answers', 'fas fa-check', 'white', '', $answers1+$answers2));
                $row->column(3, new InfoBox('Summaries', 'far fa-file', 'white', '', $summaries1+$summaries2));
                $row->column(3, new InfoBox('Bookmarks', 'far fa-comments', 'white', '', $bookmarks1+$bookmarks2));
                $row->column(3, new InfoBox('Badges', 'ra', 'orange', '/admin/Badges', \App\Models\Badge::all()->count()));
                $row->column(3, new InfoBox('Offers', 'far fa-coffee', 'blue', '/admin/offers', \App\Models\Offers::all()->count()));
                $row->column(3, new InfoBox('Reports', 'far fa-bug', 'red', '/admin/Reports', \App\Models\Reports::all()->count()));
            });

//            $content->body(view('admin.test'));

//            $content->row(function (Row $row) {
//
//                $words = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
//                    Maecenas feugiat consequat diam. Maecenas metus. Vivamus diam purus, cursus a, commodo non,
//                    facilisis vitae, nulla. Aenean dictum lacinia tortor. Nunc iaculis, nibh non iaculis aliquam,
//                    orci felis euismod neque, sed ornare massa mauris sed velit. Nulla pretium mi et risus.';
//
//                $row->column(6, function (Column $column) use ($words) {
//                    $column->append(new Alert($words));
//                    $column->append((new Alert($words, 'Alert!!'))->style('success')->icon('user'));
//                    $column->append((new Alert($words))->style('warning')->icon('book'));
//                    $column->append((new Alert($words))->style('info')->icon('info'));
//                });
//
//            });

//            return view('test', ['name' => 'James']);

        });
    }
}
