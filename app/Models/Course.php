<?php

namespace App\Models;

use App\Admin\Controllers\Video_lectureController;
use Illuminate\Database\Eloquent\Model;
use DB;

class Course extends Model
{
    public function universities(){
        return $this->belongsTo(Universities::class);
    }
    public function colleges(){
        return $this->belongsTo(Colleges::class);
    }
    public function majors(){
        return $this->belongsTo(Majors::class);
    }
    public function text_lectures(){
        return $this->hasMany(Text_lecture::class);
    }
    public function video_lectures(){
        return $this->hasMany(Video_lecture::class);
    }
    public function lecture_text_questions(){
        return $this->hasMany(Lecture_text_questions::class);
    }
    public function lecture_video_questions(){
        return $this->hasMany(Lecture_video_questions::class);
    }
    public function lecture_text_summaries(){
        return $this->hasMany(Lecture_text_summaries::class);
    }
    public function lecture_video_summaries(){
        return $this->hasMany(Lecture_video_summaries::class);
    }
    public function lecture_text_bookmarks(){
        return $this->hasMany(Lecture_text_bookmarks::class);
    }
    public function lecture_video_bookmarks(){
        return $this->hasMany(Lecture_video_bookmarks::class);
    }

}
