<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lecture_video_answers extends Model
{
    public function lecture_text_questions(){
        return $this->belongsTo(Lecture_text_questions::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function vote(){
        return $this->hasMany(Vote_video_answers::class);
    }
}
