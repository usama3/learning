<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offers extends Model
{
    public function limitations()
    {
        return $this->belongsToMany(Limitations::class);
    }
    public function periods()
    {
        return $this->belongsToMany(Period::class);
    }
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
    public function universities(){
        return $this->belongsTo(Universities::class);
    }
    public function colleges(){
        return $this->belongsTo(Colleges::class);
    }

    public function setYearsAttribute($value)
    {
        $this->attributes['years'] =  implode(',', $value);
    }
    public function setSemestersAttribute($value)
    {
        $this->attributes['semesters'] =  implode(',', $value);
    }

}
