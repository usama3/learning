<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    public function offers()
    {
        return $this->belongsToMany(Offers::class);
    }
}
