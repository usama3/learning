<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Universities extends Model
{


    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function colleges(){
        return $this->hasMany(Colleges::class);
    }
    public function courses(){
        return $this->hasMany(Course::class);
    }
    public function text_lectures(){
        return $this->hasMany(Text_lecture::class);
    }
    public function text_sections(){
        return $this->hasMany(Text_section::class);
    }
    public function video_lectures(){
        return $this->hasMany(Video_lecture::class);
    }
    public function video_sections(){
        return $this->hasMany(Video_section::class);
    }
    public function uni_and_col(){
        return $this->join('colleges','universities.id','colleges.universities_id')
            ->select('universities.name as uni','colleges.name as col')
            ->get();
    }

//    public function university_by_user_id($user_id){
//
//        return Universities::where("id", $user_id)->select('name')->get()[0];
//
//    }

}
