<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reports extends Model
{
    public function reports_msg(){
        return $this->belongsTo(Reports_msg::class);
    }
    public function users(){
        return $this->belongsTo(User::class);
    }
}
