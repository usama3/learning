<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification_users extends Model
{
    public function notifications(){
        return $this->belongsTo(Notifications::class);
    }
}
