<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Text_lecture extends Model
{
    protected $table = 'text_lectures';

    public function universities(){
        return $this->belongsTo(Universities::class);
    }
    public function colleges(){
        return $this->belongsTo(Colleges::class);
    }
    public function majors(){
        return $this->belongsTo(Majors::class);
    }
    public function courses(){
        return $this->belongsTo(Course::class);
    }
    public function text_sections(){
        return $this->hasMany(Text_section::class);
    }
    public function lecture_text_questions(){
        return $this->hasMany(Lecture_text_questions::class);
    }
    public function lecture_text_summaries(){
        return $this->hasMany(Lecture_text_summaries::class);
    }

}
