<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Limitations extends Model
{
    public function offers()
    {
        return $this->belongsToMany(Offers::class);
    }
}
