<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lecture_video_bookmarks extends Model
{
    public function courses(){
        return $this->belongsTo(Course::class);
    }
    public function video_section(){
        return $this->belongsTo(Video_lecture::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function vote(){
        return $this->hasMany(Vote_video_bookmarks::class);
    }

}
