<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Majors extends Model
{
    public function users()
    {
        return $this->hasMany(User::class);
    }
    public function universities(){
        return $this->belongsTo(Universities::class);
    }
    public function colleges(){
        return $this->belongsTo(Colleges::class);
    }
    public function course(){
        return $this->hasMany(Course::class);
    }
    public function text_lectures(){
        return $this->hasMany(Text_lecture::class);
    }
    public function video_lectures(){
        return $this->hasMany(Video_lecture::class);
    }
    public function courses()
    {
        return $this->hasMany(Course::class);
    }
    public function setYearsAttribute($value)
    {
        $this->attributes['years'] =  implode(',', $value);
    }
    public function getYearsAttribute($value)
    {
        return ucfirst($value);
    }
}
