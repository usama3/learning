<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reports_msg extends Model
{
    protected $table = 'reports_msg';

    public function report(){
        return $this->hasOne(Reports::class);
    }
}
