<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
use SebastianBergmann\CodeCoverage\Report\Xml\Report;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    protected $fillable = [
        'username', 'email', 'password','first_name','middle_name','last_name','ip_address','country_id','city_id',
        'year_of_study','college_id','major_id','birthdate'
    ];

    protected $hidden = [
        'password', 'remember_token','activation_token'
    ];

    public function countries()
    {
        return $this->belongsTo(Countries::class);
    }

    public function cities()
    {
        return $this->belongsTo(Cities::class);
    }
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function universities()
    {
        return $this->belongsTo(Universities::class);
    }
    public function colleges()
    {
        return $this->belongsTo(Colleges::class);
    }
    public function majors()
    {
        return $this->belongsTo(Majors::class);
    }
    public function lecture_text_questions()
    {
        return $this->hasMany(Lecture_text_questions::class);
    }
    public function lecture_video_questions(){
        return $this->hasMany(Lecture_video_questions::class);
    }
    public function lecture_text_answers(){
        return $this->hasMany(Lecture_text_answers::class);
    }
    public function lecture_video_answers(){
        return $this->hasMany(Lecture_video_answers::class);
    }
    public function lecture_text_summaries(){
        return $this->hasMany(Lecture_text_summaries::class);
    }
    public function lecture_video_summaries(){
        return $this->hasMany(Lecture_video_summaries::class);
    }
    public function lecture_text_bookmarks(){
        return $this->hasMany(Lecture_text_bookmarks::class);
    }
    public function lecture_video_bookmarks(){
        return $this->hasMany(Lecture_video_bookmarks::class);
    }
    public function reports(){
        return $this->hasMany(Report::class);
    }
    public function offers()
    {
        return $this->belongsToMany(Offers::class);
    }
    public function badges()
    {
        return $this->belongsToMany(Badge::class);
    }
    public function visits()
    {
        return visits($this);
    }
    public function notification_users(){
        return $this->hasMany(Notification_users::class);
    }

}
