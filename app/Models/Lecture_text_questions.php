<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lecture_text_questions extends Model
{
    public $timestamps = false;

    public function course(){
        return $this->belongsTo(Course::class);
    }
    public function text_lecture(){
        return $this->belongsTo(Text_lecture::class);
    }
    public function text_section(){
        return $this->belongsTo(Text_section::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function text_answers(){
        return $this->hasMany(Lecture_text_answers::class);
    }
    public function text_sections(){
        return $this->belongsTo(Text_section::class);
    }
    public function vote(){
        return $this->hasMany(Vote_text_questions::class);
    }

}
