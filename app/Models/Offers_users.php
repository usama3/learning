<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offers_users extends Model
{
    protected $table = 'offers_user';

    public function users(){
        return $this->belongsTo(User::class);
    }
    public function offers()
    {
        return $this->belongsTo(Offers::class);
    }

}
