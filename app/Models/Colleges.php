<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Colleges extends Model
{
    public function users()
    {
        return $this->hasMany(User::class);
    }
    public function universities(){
        return $this->belongsTo(Universities::class);
    }
    public function majors(){
        return $this->hasMany(Majors::class);
    }
    public function course(){
        return $this->hasMany(Course::class);
    }
    public function text_lectures(){
        return $this->hasMany(Text_lecture::class);
    }
    public function text_sections(){
        return $this->hasMany(Text_section::class);
    }
    public function video_lectures(){
        return $this->hasMany(Video_lecture::class);
    }
    public function video_sections(){
        return $this->hasMany(Video_section::class);
    }

}
