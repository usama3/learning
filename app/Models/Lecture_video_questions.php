<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lecture_video_questions extends Model
{
    public function course(){
        return $this->belongsTo(Course::class);
    }
    public function video_lecture(){
        return $this->belongsTo(Video_lecture::class);
    }
    public function video_section(){
        return $this->belongsTo(Video_section::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function video_answers(){
        return $this->hasMany(Lecture_video_answers::class);
    }
    public function video_sections(){
        return $this->belongsTo(Video_section::class);
    }
    public function vote(){
        return $this->hasMany(Vote_video_questions::class);
    }

}
