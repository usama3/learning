<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lecture_text_summaries extends Model
{
    public function courses(){
        return $this->belongsTo(Course::class);
    }
    public function text_lecture(){
        return $this->belongsTo(Video_lecture::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function vote(){
        return $this->hasMany(Vote_text_summaries::class);
    }
}
