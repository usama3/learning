<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video_lecture extends Model
{
    protected $table = 'video_lectures';

    public function universities(){
        return $this->belongsTo(Universities::class);
    }
    public function colleges(){
        return $this->belongsTo(Colleges::class);
    }
    public function majors(){
        return $this->belongsTo(Majors::class);
    }
    public function courses(){
        return $this->belongsTo(Course::class);
    }
    public function video_sections(){
        return $this->hasMany(Video_section::class);
    }
    public function lecture_video_questions(){
        return $this->hasMany(Lecture_video_questions::class);
    }
    public function lecture_video_summaries(){
        return $this->hasMany(Lecture_video_summaries::class);
    }

}
