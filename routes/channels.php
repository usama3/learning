<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/


Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});
//Broadcast::channel('notifications-channel.{toUserId}', function ($user, $toUserId) {
//    return [ 'message' => "soso"];
//});

//Broadcast::channel('notifications-channel.{toUserId}', function ($user) {
////    return true; //for public access
////    // or
//    return [ 'message' => "soso"];
//});

Broadcast::channel('notifications-channel.{toUserToken}', function ($user, $token) {
    return $user->remember_token == $token;
});
//Broadcast::channel('survey.{survey_id}', function ($user, $survey_id) {
//    return [
//        'id' => $user->id,
//        'image' => $user->image(),
//        'full_name' => $user->full_name
//    ];
//});

