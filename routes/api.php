<?php

//////////////////////////// API Start //////////////////////////////////////

Route::group([
    'middleware' => 'api',
], function () {

    //////////////////////////// Visitor Routes Start //////////////////////////////////////

    // Home
    Route::get('/home','\App\Http\Controllers\HomeController@index');
    // College
    Route::get('/college/{id}','\App\Http\Controllers\CollegeController@college');
    Route::get('/courses_by_majors/{college_id}/{years_array}/{majors_array}','\App\Http\Controllers\CollegeController@courses_by_majors');
    // Course
    Route::get('/course/{course_id}/info','\App\Http\Controllers\CourseController@course_info');

    //////////////////////////// Visitor Routes End //////////////////////////////////////


    //////////////////////////// Offers Routes Start //////////////////////////////////////

    Route::group([
        'middleware' => 'Offer',
    ], function () {
        Route::get('/course/{id}','\App\Http\Controllers\CourseController@index');
        Route::get('/course/{course_id}/lectures','\App\Http\Controllers\LectureController@lectures');
        Route::get('/course/{course_id}/lecture/{type}/{id}','\App\Http\Controllers\LectureController@index');

    });

    //////////////////////////// Offers Routes End //////////////////////////////////////


    //////////////////////////// Subscribed Users Routes Start //////////////////////////////////////

    Route::group([
        'middleware' => 'CheckToken',
    ], function () {

        Route::get('/section_summaries/{type}/{lecture_id}','\App\Http\Controllers\SummariesController@section_summary');
        Route::get('/section_bookmarks/{type}/{section_id}','\App\Http\Controllers\BookmarksController@section_bookmarks');
        Route::get('/section_questions/{type}/{section_id}','\App\Http\Controllers\QuestionsController@section_questions');

        // Course
        Route::post('/course/{course_id}/rate','\App\Http\Controllers\CourseController@rate_course');

        // Questions
        Route::get('/course/{course_id}/questions','\App\Http\Controllers\QuestionsController@questions')->middleware('Limitations:read users questions');
        Route::get('/course/{course_id}/question_and_answers/{type}/{id}','\App\Http\Controllers\QuestionsController@question_and_answers')->middleware('Limitations:read users questions');
        Route::post('/add_question','\App\Http\Controllers\QuestionsController@add_question')->middleware('Limitations:add questions');
        Route::put('/edit_question','\App\Http\Controllers\QuestionsController@edit_question')->middleware('AllowedToUpdateOrDelete:id,questions');
        Route::delete('/delete_question/{type}/{id}','\App\Http\Controllers\QuestionsController@delete_question')->middleware('AllowedToUpdateOrDelete:null,questions');
        Route::put('/add_bounty','\App\Http\Controllers\QuestionsController@add_bounty')->middleware('Limitations:add bounty on question');
        Route::put('/cancel_bounty','\App\Http\Controllers\QuestionsController@cancel_bounty');

        // Answers
        Route::post('/add_answer','\App\Http\Controllers\AnswersController@add_answer')->middleware('Limitations:read users answers');
        Route::put('/edit_answer','\App\Http\Controllers\AnswersController@edit_answer')->middleware('AllowedToUpdateOrDelete:id,answers');
        Route::delete('/delete_answer/{type}/{id}','\App\Http\Controllers\AnswersController@delete_answer')->middleware('AllowedToUpdateOrDelete:null,answers');
        Route::put('/mark_answer','\App\Http\Controllers\AnswersController@mark_answer')->middleware('Limitations:mark answer');
        Route::put('/un_mark_answer','\App\Http\Controllers\AnswersController@un_mark_answer');

        // Summary
        Route::get('/summary/{type}/{id}','\App\Http\Controllers\SummariesController@get_summary')->middleware('Limitations:read users summaries');
        Route::get('/course/{id}/all_summaries','\App\Http\Controllers\SummariesController@all_summaries')->middleware('Limitations:read users summaries');
        Route::get('/course/{id}/my_summaries','\App\Http\Controllers\SummariesController@my_summaries');
        Route::post('/add_summery','\App\Http\Controllers\SummariesController@add_summery')->middleware('Limitations:add summaries');
        Route::put('/edit_summery','\App\Http\Controllers\SummariesController@edit_summery')->middleware('AllowedToUpdateOrDelete:id,summaries');
        Route::delete('/delete_summery/{type}/{id}','\App\Http\Controllers\SummariesController@delete_summery')->middleware('AllowedToUpdateOrDelete:null,summaries');

        // Bookmarks
        Route::get('/bookmark/{type}/{id}','\App\Http\Controllers\BookmarksController@get_bookmark')->middleware('Limitations:read users bookmarks');
        Route::get('/course/{id}/all_bookmarks','\App\Http\Controllers\BookmarksController@all_bookmarks')->middleware('Limitations:read users bookmarks');
        Route::get('/course/{id}/my_bookmarks','\App\Http\Controllers\BookmarksController@my_bookmarks');
        Route::post('/add_bookmark','\App\Http\Controllers\BookmarksController@add_bookmark')->middleware('Limitations:add bookmarks');
        Route::put('/edit_bookmark','\App\Http\Controllers\BookmarksController@edit_bookmark')->middleware('AllowedToUpdateOrDelete:id,bookmarks');
        Route::delete('/delete_bookmark/{type}/{id}','\App\Http\Controllers\BookmarksController@delete_bookmark')->middleware('AllowedToUpdateOrDelete:null,bookmarks');

        // Vote
        Route::put('/vote_text_question','\App\Http\Controllers\VoteController@vote_text_question')->middleware('Limitations:vote questions');
        Route::put('/vote_video_question','\App\Http\Controllers\VoteController@vote_video_question')->middleware('Limitations:vote questions');
        Route::put('/vote_text_answer','\App\Http\Controllers\VoteController@vote_text_answer')->middleware('Limitations:vote answers');
        Route::put('/vote_video_answer','\App\Http\Controllers\VoteController@vote_video_answer')->middleware('Limitations:vote answers');
        Route::put('/vote_text_summary','\App\Http\Controllers\VoteController@vote_text_summary')->middleware('Limitations:vote summaries');
        Route::put('/vote_video_summary','\App\Http\Controllers\VoteController@vote_video_summary')->middleware('Limitations:vote summaries');
        Route::put('/vote_text_bookmark','\App\Http\Controllers\VoteController@vote_text_bookmark')->middleware('Limitations:vote bookmarks');
        Route::put('/vote_video_bookmark','\App\Http\Controllers\VoteController@vote_video_bookmark')->middleware('Limitations:vote bookmarks');
        Route::get('/users_who_voted/{event_id}/{event_type}','\App\Http\Controllers\VoteController@users_who_voted');

        // Reports
        Route::post('/report','\App\Http\Controllers\ReportController@report');
        Route::get('/get_report_msg/{type}','\App\Http\Controllers\ReportController@get_report_msg');

        // Offers
        Route::get('/show_offers','\App\Http\Controllers\OfferController@show_offers');
        Route::post('/subscribe_offer','\App\Http\Controllers\OfferController@subscribe_offer');
        Route::post('/un_subscribe_offer','\App\Http\Controllers\OfferController@un_subscribe_offer');

        // Profile
        Route::get('/profile/{id}','\App\Http\Controllers\ProfileController@index');
        Route::get('/statistics/{user_id}','\App\Http\Controllers\ProfileController@statistics');
        Route::get('/badges/{user_id}','\App\Http\Controllers\ProfileController@badges');
        Route::put('/edit_profile','\App\Http\Controllers\ProfileController@edit_profile')->middleware('EditProfile');
        Route::put('/change_password','\App\Http\Controllers\ProfileController@change_password')->middleware('ChangePassword');
        // progress
        Route::post('/add_progress_lecture','\App\Http\Controllers\UserProgressController@add_progress_lecture');
        Route::get('/user_course_progress/{course_id}','\App\Http\Controllers\UserProgressController@user_course_progress');
        Route::get('/user_progress/{user_id}','\App\Http\Controllers\UserProgressController@user_progress');

        // Notifications
        Route::get('/GetUserNotifications','\App\Http\Controllers\NotificationsController@GetUserNotifications');
        Route::post('/seen','\App\Http\Controllers\NotificationsController@seen');
//        Route::post('/QuestionVote/{a}/{b}/{c}','\App\Http\Controllers\NotificationsController@AnswerVote');
//        Route::post('/QuestionAnswered/{a}/{b}/{c}','\App\Http\Controllers\NotificationsController@QuestionAnswered');
//        Route::post('/AnswerMarked/{a}/{b}','\App\Http\Controllers\NotificationsController@AnswerMarked');
//        Route::post('/Badge/{a}/{b}','\App\Http\Controllers\NotificationsController@Badge');
//        Route::post('/UsersOfCourse/{a}','\App\Http\Controllers\NotificationsController@UsersOfCourse');
//        Route::post('/NewQuestion/{a}/{b}','\App\Http\Controllers\NotificationsController@QuestionBounty');


    });
    //Upload
    Route::post('/upload_pic/{type}','\App\Http\Controllers\UploadController@upload_pic');

    Route::get('/profile_countries/{country_name}','\App\Http\Controllers\ProfileController@countries');
    Route::get('/profile_cities/{country_id}/{city_name}','\App\Http\Controllers\ProfileController@cities');
    Route::get('/profile_universities/{university_id}','\App\Http\Controllers\ProfileController@universities');
    Route::get('/profile_colleges/{university_id}/{college_name}','\App\Http\Controllers\ProfileController@colleges');
    Route::get('/profile_majors/{user_year}/{university_id}/{college_id}/{major_name}','\App\Http\Controllers\ProfileController@majors');


    //////////////////////////// Subscribed Users Routes End //////////////////////////////////////

    //////////////////////////// Auth Routes Start //////////////////////////////////////

    // Auth
    Route::post('/register', '\App\Http\Controllers\Auth\AuthController@register')->middleware('Register');
    Route::post('/login', '\App\Http\Controllers\Auth\AuthController@login')->middleware('Login');
    Route::get('signup/activate/{token}', '\App\Http\Controllers\Auth\AuthController@signupActivate');
    Route::group([
        'namespace' => 'Auth',
        'prefix' => 'password'
    ], function () {
        Route::post('create', 'PasswordResetController@create');
        Route::get('find/{token}', 'PasswordResetController@find');
        Route::post('reset', 'PasswordResetController@reset');
    });
    //////////////////////////// Auth Routes End //////////////////////////////////////


//    Route::post('/upload_image', function() {
//        $CKEditor = Input::get('CKEditor');
//        $funcNum = Input::get('CKEditorFuncNum');
//        $message = $url = '';
//        if (Input::hasFile('upload')) {
//            $file = Input::file('upload');
//            if ($file->isValid()) {
//                $filename = $file->getClientOriginalName();
//                $file->move(public_path().'/uploads/images/', $filename);
//                $url = public_path() .'/uploads/images/' . $filename;
//            } else {
//                $message = 'An error occured while uploading the file.';
//            }
//        } else {
//            $message = 'No file uploaded.';
//        }
//        return '<script>window.parent.CKEDITOR.tools.callFunction('.$funcNum.', "'.$url.'", "'.$message.'")</script>';
//    });


});

//////////////////////////// API End //////////////////////////////////////





//
//Route::get('fire', function () {
//
//    $data = [
//        "id" => 54,
//        "user_id" => 1,
//        "title" => "Question answered",
//        "text" => " admin has added a new answer on question you follow.",
//        "picture" => "images/Users/b6e05557fdf7ae1fe1134f9462eb52c0.jpg",
//        "type" => "Question answered",
//        "event_id" => 1,
//        "created_at" => "2018-10-13 23:32:11",
//        "updated_at" => "2018-10-13 23:32:11",
//        "is_seen" => 0,
//    ];
//
//    event(new App\Events\QuestionAndAnswers('text', '1',$data));
//
////    $user = App\Models\User::find(1);
////    event(new App\Events\Notifications($user, $data));
////        $user = App\Models\User::find(2);
////        event(new App\Events\Notifications($user, $data));
//    return "soso event fired";
//});